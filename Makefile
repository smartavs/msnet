#####custom
prefix=/usr/local/msnet
export PKG_CONFIG_PATH=${prefix}/lib/pkgconfig:$PKG_CONFIG_PATH
libdir=${prefix}/lib
includedir=${prefix}/include

D_BASEAPI_FLAGS= \
	-DOS_LINUX_SOC \
	-D__STDC_FORMAT_MACROS

CFLAGS_SHARE=-shared
C_SRC=src/msnet.c \
	src/mspcap.c

PATH_CUR=`pwd`
D_MSLOG_LIBS 		:=-L${libdir} -lmslog
D_MSLOG_INCLUDES 	:=-I${includedir} -I${includedir}/libmslog
D_MSCOMMON_LIBS 	:=-L${libdir} -lmscommon
D_MSCOMMON_INCLUDES 	:=-I${includedir} -I${includedir}/libmscommon
D_MSPROTOCOL_LIBS 	:=-L${libdir} -lmsprotocol
D_MSPROTOCOL_INCLUDES	:=-I${includedir} -I${includedir}/libmsprotocol
D_MSTOOL_LIBS 		:=-L${libdir} -lmstool
D_MSTOOL_INCLUDES	:=-I${includedir} -I${includedir}/libmstool
D_MSPCAP_LIBS 		:=-L${libdir} -lpcap
D_MSPCAP_INCLUDES	:=-I${includedir} -I${includedir}/libpcap


C_INCLUDE:=${D_MSLOG_INCLUDES} ${D_MSCOMMON_INCLUDES}  ${D_MSPROTOCOL_INCLUDES} ${D_MSTOOL_INCLUDES} 	${D_MSPCAP_INCLUDES} -Isrc ${D_BASEAPI_FLAGS} -I/usr/local/msnet/include/
C_LIBS	 :=${D_MSLOG_LIBS}     ${D_MSCOMMON_LIBS}      ${D_MSPROTOCOL_LIBS}     ${D_MSTOOL_LIBS} 	${D_MSPCAP_LIBS}     -pthread -lm 

msnet_CC=${CC}
msnet_SRC=${C_SRC}
msnet_CFLAGS=${C_INCLUDE} ${C_LIBS}

msnet_gdb_CC=${msnet_CC}
msnet_gdb_SRC=${msnet_SRC}
msnet_gdb_CFLAGS=${msnet_CFLAGS} -g

PATH_EXTERNSRC=../
PATH_EXTERNSRC_MSCOMMON=${PATH_EXTERNSRC}/mscommon
PATH_EXTERNSRC_MSLOG=${PATH_EXTERNSRC}/mslog
PATH_EXTERNSRC_MSPROTOCOL=${PATH_EXTERNSRC}/msprotocol
PATH_EXTERNSRC_MSTOOL=${PATH_EXTERNSRC}/mstool

all: init libms msnet
init:
	#rm -fr /usr/local/msnet
	#cp -fr msnet_pkg/target/msnet_target  /usr/local/msnet
	mkdir -p /usr/local/msnet/lib
	mkdir -p /usr/local/msnet/include
	mkdir -p /usr/local/msnet/bin
	cp -fr msnet_pkg/target/libmsnet.conf /etc/ld.so.conf.d/libmsnet.conf
	ldconfig
libms:
	cd ${PATH_EXTERNSRC_MSLOG}      && make x8664 && make clean
	cd ${PATH_EXTERNSRC_MSCOMMON}   && make x8664 && make clean
	cd ${PATH_EXTERNSRC_MSPROTOCOL} && make simple && make clean
	cd ${PATH_EXTERNSRC_MSTOOL}     && make x8664 && make clean
	cp -fr /usr/local/mscore/lib/libmslog.so        /usr/local/msnet/lib/libmslog.so
	cp -fr /usr/local/mscore/lib/libmscommon.so     /usr/local/msnet/lib/libmscommon.so
	cp -fr /usr/local/mscore/lib/libmsprotocol.so   /usr/local/msnet/lib/libmsprotocol.so
	cp -fr /usr/local/mscore/lib/libmstool.so       /usr/local/msnet/lib/libmstool.so
	rm -fr /usr/local/msnet/include/libmslog
	rm -fr /usr/local/msnet/include/libmscommon
	rm -fr /usr/local/msnet/include/libmsprotocol
	rm -fr /usr/local/msnet/include/libmstool
	cp -fr /usr/local/mscore/include/libmslog       /usr/local/msnet/include/libmslog
	cp -fr /usr/local/mscore/include/libmscommon    /usr/local/msnet/include/libmscommon
	cp -fr /usr/local/mscore/include/libmsprotocol  /usr/local/msnet/include/libmsprotocol
	cp -fr /usr/local/mscore/include/libmstool      /usr/local/msnet/include/libmstool
msnet: 
	$($@_CC)  $($@_SRC)  $($@_CFLAGS)  -o out_$@ 
	cp -fr out_$@ /usr/local/msnet/bin/$@_bin 
msnet_gdb: 
	$($@_CC)  $($@_SRC)  $($@_CFLAGS)  -o out_$@ -DCONFIG_DEBUG
	cp -fr out_$@ /usr/local/msnet/bin/$@_bin 
pkg:
	rm -fr msnet_pkg/target/msnet_target
	cp -fr /usr/local/msnet  msnet_pkg/target/msnet_target
	cd msnet_pkg && bash mkinst.sh
clean:
	rm  out -fr
help:
	@echo "USAGE:make target"
	@echo "    libmstcpacc      	Produce the libmstcpacc.so"
	@echo "    mstcpacc             Program with libmstcpacc api"
	@echo "    install      	Install libmstcpacc."
	@echo "    uninstall     	Uninstall libmstcpacc"
	@echo "    clean                Clear out"

