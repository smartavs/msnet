#!/bin/sh
#########################
# === global config === #
#########################
PATH_CUR=`pwd`

###################BEGAIN：设置目录###################
DIR_OUT_PATH=${PATH_CUR}/pkg_out

##IPTV Server软件目录
DIR_INSTALL_SOFT=msnet
FILE_SH_HEADER=${PATH_CUR}/inst.sh

TARGE_3PKG=/home/sugao/msavskit/extern/msavskit_3party/pkg/
##安装帮助说明文件
FILE_INSTALL_HELP=
#########################
#   === function ===    #
#########################

DOMK_API_DATE()
{    
    echo "**************************************************"
    echo  "DATE:"    `date`
    echo "**************************************************"
}

STATICDO_INIT_PreSetTargetName()
{
    echo "no" 
}

STATICDO_INIT_PreSetVerNum()
{
    FILE_VER=${PATH_CUR}/version
    ##设置目前使用系统名
    PRE_VER_NUM=`cat ${FILE_VER}`
    echo "${PRE_VER_NUM}"
    
    echo -n "Input version num:"
    read VERSION_NUM
    echo -n "Input modify info:"
    read VERSION_INFO
    touch  ${FILE_VER}
}


##使用名字的设置
STATICDO_INIT_PreSetName()
{
    STATICDO_INIT_PreSetTargetName
    
    STATICDO_INIT_PreSetVerNum

    DIR_OUTPUT=${DIR_OUT_PATH}/out${DIR_INSTALL_SOFT}_${VERSION_NUM}

    echo "out dir:${DIR_OUTPUT}"
}


##环境的初始化，该函数依赖STATICDO_INIT_PreSetName的DIR_OUTPUT的设置
DOMK_API_INITEnv()
{
    mkdir -p ${DIR_OUT_PATH}
    STATICDO_INIT_PreSetName
    if [ $? -ne 0 ]; then
        exit 1
    fi
    rm -fr     ${DIR_OUTPUT}

}

##生成安装${DIR_OUTPUT}
DOMK_API_ACTDone()
{
    mkdir -p ${DIR_OUTPUT}  
  
    PKT_NAME=${DIR_INSTALL_SOFT}.pkg
    
    cp -fr target ${DIR_INSTALL_SOFT}
    #生成安装目录tar.gz压缩包
    echo "压缩${DIR_INSTALL_SOFT}中，请稍等..."
    tar -zcf ${DIR_INSTALL_SOFT}.tar.gz ${DIR_INSTALL_SOFT}
    echo "压缩${DIR_INSTALL_SOFT}完成"
    echo "生成可执行安装包${PKT_NAME}中，请稍等..."
    cat ${FILE_SH_HEADER} ${DIR_INSTALL_SOFT}.tar.gz > ${PKT_NAME}
    chmod +x ${PKT_NAME}
    cp ${PKT_NAME} ${DIR_OUTPUT}/ -fr
    echo "生成${PKT_NAME}完成"

    echo "拷贝${PKT_NAME}文件到${TARGE_3PKG}"
    cp -fr ${PKT_NAME} 	${TARGE_3PKG}	
    rm -fr ${PKT_NAME}
}

##做一些清尾以及结果提示操作
DOMK_API_ACTDoneCleanAndPrint()
{
    #删除临时压缩包
    rm -fr ${DIR_INSTALL_SOFT}.tar.gz ${PKT_NAME} ${PATH_CUR}/${DIR_INSTALL_SOFT}
    echo "`date` : ${VERSION_NUM}  ${VERSION_INFO}"  >> ${FILE_VER}
    cp ${FILE_VER} ${DIR_OUTPUT}/version -fr  
    echo "Make ${DIR_OUTPUT} success!"
}


###############################
#   === function active===    #
###############################
DOMK_API_DATE

DOMK_API_INITEnv
if [ $? -ne 0 ]; then
    exit 1
fi

DOMK_API_ACTDone
if [ $? -ne 0 ]; then
    exit 1
fi

DOMK_API_ACTDoneCleanAndPrint
if [ $? -ne 0 ]; then
    exit 1
fi
