	PATH_UNPACK=/tmp
	NAME_PKG=msnet.tar.gz
	PATH_PKG=/tmp/msnet	
        ## 从安装文件尾部取出tar包并在当前路径解开
	echo "Unpacking..."
	##计算行数，不能自动计算行数
	##line=`wc -l $0 | awk '{print $1}'`
	line=39
	let line=$line+1
	##line=`expr $line + 1`
	##从尾部提取除了脚本外的压缩包数据
        tail -n +$line $0 > ${PATH_UNPACK}/${NAME_PKG}
	##解压缩
        tar -zvxf ${PATH_UNPACK}/${NAME_PKG}  -C ${PATH_UNPACK}/ >/dev/null

        if [ $? != 0 ]

        then
                echo "There is error when unpacking files."
	## 删除临时文件
		#rm -f ./IPTVServer.setup
		rm -f ${PATH_UNPACK}/${NAME_PKG}
                    exit 1

        fi

        echo "Done."
	## 删除临时文件
	#rm -f ./IPTVServer.setup
	rm -f ${PATH_UNPACK}/${NAME_PKG}

        ## 执行具体安装操作
    
	cd ${PATH_PKG} &&bash install.sh
        ## 删除临时目录
	rm -fr ${PATH_PKG}


	exit 0
