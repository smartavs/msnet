#! /bin/bash
CURPATH=`pwd`
SERVICE_NAME=msnet

doprint()
{
    case "${SYS_LANGUAGE}" in
        "CHINESE" | "CH" )
            echo "$1"
            ;;

        *)
            echo "$2"
            ;;
    esac
}


do_pre_checkos()
{
    echo " "
    echo "=====================${SERVICE_NAME}:Check os==============================="
    echo " "
    
    TEST_SYSTEM="ubuntu 16.04,ubuntu 18.04 or centos 8.1"
    if  ( lsb_release -a >/dev/null);then
        if  ( lsb_release -a | grep Ubuntu >/dev/null);then
            if   ( lsb_release -r | grep 16.04 >/dev/null);then
                SYSTEM="ubuntu1604"
            elif   ( lsb_release -r | grep 18.04 >/dev/null);then
                SYSTEM="ubuntu1804"
            else
                echo "Unteset ubuntu, you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to ubuntu1804 for test-running"
	        SYSTEM="ubuntu1804"
            fi
        #elif  ( lsb_release -a | grep Uos >/dev/null);then
        #    if   ( lsb_release -r | grep "20 Home" >/dev/null);then
        #        SYSTEM="uos20Home"
        #   else
        #       echo "Unteset uos,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to uos20Home for test-running"
	#       SYSTEM="uos20Home"
        #   fi
        else
            echo "Unteset lsb_release,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to ubuntu1804 for test-running"
            SYSTEM="ubuntu1804"
        fi         
    elif  ( cat /etc/redhat-release | grep "CentOS Linux release" >/dev/null);then
        if  ( cat /etc/redhat-release | grep "CentOS Linux release" >/dev/null);then
            if   ( cat /etc/redhat-release | grep "CentOS Linux release 8.1" >/dev/null);then
                SYSTEM="centos8.1"
            else
                echo "Unteset redhat,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to centos8.1 for test-running"
                SYSTEM="centos8.1"
            fi
        else
            echo "Unteset redhat,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to centos8.1  for test-running"
            SYSTEM="centos8.1"
        fi   
    else
        echo "Unteset os,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to ubuntu1804 for test-running"
        SYSTEM="ubuntu1804"
    fi
    
    case "${SYSTEM}" in
    "ubuntu1604" | "ubuntu1804"| "centos8.1")
        echo "OK，your OS is $SYSTEM,you can use this script..."
        ;;
    *)
        echo "Unteset os $SYSTEM you should change your os to "${TEST_SYSTEM}""
        ;;
    esac

    OS_MACHINE=`uname -m`
    if [ "$OS_MACHINE" = "x86_64" ]; then
        echo "OK，your OS is ${SYSTEM}_${OS_MACHINE},keep going...."
    else
        echo "Sorry, We only support${TEST_SYSTEM}, This script doesn't support your OS platform(${SYSTEM}${OS_MACHINE})，will exit"
        exit 1
    fi
}

do_pre_serverstop()
{
    service ${SERVICE_NAME} stop
}

do_uninstall_bin()
{
    rm -fr /usr/local/msnet -fr
    rm -fr /etc/msnet/
    rm -fr /etc/ld.so.conf.d/libmsnet.conf
    ldconfig
    doprint "卸载可执行文件完成！" "Uninstall bin file success!"
}

do_uninstall_startup()
{
    case "${SYSTEM}" in

    "ubuntu1604" | "ubuntu1804")
        if (update-rc.d -f ${SERVICE_NAME} remove && rm /etc/init.d/${SERVICE_NAME} -fr);then 
            doprint "${SYSTEM}:成功移除 ${SERVICE_NAME} 服务器为开机启动!" "${SYSTEM} :Remove ${SERVICE_NAME} startup success!"
        else
            doprint "${SYSTEM}:移除 ${SERVICE_NAME} 服务器开机启动失败!"   "${SYSTEM} :Remove ${SERVICE_NAME} startup failed!"
            exit 1
        fi
        ;;
    "centos8.1")
        if (chkconfig --del ${SERVICE_NAME} && rm /etc/init.d/${SERVICE_NAME} -fr);then 
            doprint "${SYSTEM}:成功设置 ${SERVICE_NAME} 服务器为开机启动!" "${SYSTEM} :Remove ${SERVICE_NAME} startup success!"
        else
            doprint "${SYSTEM}:设置 ${SERVICE_NAME} 服务器开机启动失败!"   "${SYSTEM} :Remove ${SERVICE_NAME} startup failed!"
            exit 1
        fi
        ;;
    esac
}

do_pre_checkos

do_pre_serverstop

do_uninstall_bin

do_uninstall_startup

rm /usr/local/bin/uninstall_${SERVICE_NAME}
