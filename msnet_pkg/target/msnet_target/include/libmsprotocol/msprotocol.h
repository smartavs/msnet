#ifndef MSPROTOCOL_H
#define MSPROTOCOL_H
/******************************************************************************
 ******************************************************************************
  File Name     : url.h
  Version       : Initial Draft
  Author        : su<sugao_cn@163.com>
  Created       : 2015/4/14
  Last Modified :
  Description   : url file
  Function List :
  History       :
  1.Date        : 2015/4/14
    Author      : su<sugao_cn@163.com>
    Modification: Created file

******************************************************************************/
#if defined(OS_ANDROID)||defined(OS_LINUX_SOC)
#include <netinet/in.h>
#elif defined(OS_WIN32)
#include <winsock2.h>
#endif
#include <libmscommon/msthread.h>
#include <libmscommon/mstypes.h>
#include <libmscommon/msfifo.h>
#include <libmslog/mstype.h>
 #define HAVE_IPV6  ms_true

//net flag
#define FLAG_WRITE			(1<<0)		//0:read , 1:write;	
#define FLAG_READ_WRITE		(1<<1)		//read and write;

#define FLAG_FILE_APPEND		(1<<2)
#define FLAG_FILE_EMPTY		(1<<3)
#define FLAG_FILE_NOCREATE	(1<<4)

#define FLAG_USE_THREAD_FIFO	(1<<5)
#define FLAG_DATA_NOCOPY		(1<<6)
#define FLAG_BROADCAST			(1<<7)
#define FLAG_NOCHECKEXSIT		(1<<8)	

//check mask
#define MS_FLAG_EXSIT		(1<<0)	
#define MS_FLAG_READ		(1<<1)
#define MS_FLAG_WRITE		(1<<2)

//port
#define ms_unvalidport			(-1)
#define ms_defaultport_rtsp		554
#define ms_defaultport_http		80
#define ms_defaultport_rtmp	1935

#define  mscfg_maxlen_url			1024
#define  mscfg_maxlen_send			32768
#define  mscfg_maxlen_recv			32768	//65536

#define  mscfg_max_whitelist		4
#define  mscfg_max_blacklist		4

//msptc_file must be first
typedef enum ENUPROTOCOLIndex{
	msptc_file=0x00,
	msptc_udp,
	msptc_rtp,
	msptc_rtmp,
	msptc_hls,
	msptc_msoshls,
	msptc_http,
	msptc_http_vod,
	msptc_rtsp,
	msptc_tcp,
	msptc_srt,
	msptc_none,
	msptc_raw,
	msptc_unknow,
}ENUPROTOCOLIndex;

typedef enum IPPINGState{	
	ipping_state_unreachable=0x00,
	ipping_state_reachable,
	ipping_state_nocheck,
}IPPINGState;

#define msstring_protocol \
	"file", \
	"udp", \
	"rtp", \
	"rtmp", \
	"hls", \
	"msoshls", \
	"http", \
	"http_vod", \
	"rtsp", \
	"tcp", \
	"srt",\
	"none", \
	"raw", \
	"unkonw" 

typedef enum RTSPLowerTransport {
    RTSP_LOWER_TRANSPORT_UNKNOW = 0,  			/**< UDP/unicast */
    RTSP_LOWER_TRANSPORT_UDP ,  			/**< UDP/unicast */
    RTSP_LOWER_TRANSPORT_TCP ,   			/**< TCP; interleaved in RTSP */
    RTSP_LOWER_TRANSPORT_UDP_MULTICAST, 	/**< UDP/multicast */
    RTSP_LOWER_TRANSPORT_NB,
    RTSP_LOWER_TRANSPORT_HTTP ,  			/**< HTTP tunneled - not a proper transport mode as such,only for use via AVOptions */ 
    RTSP_LOWER_TRANSPORT_CUSTOM ,       		/**< Custom IO - not a public option for lower_transport_mask,but set in the SDP demuxer based on a flag.*/
}RTSPLowerTransport;

typedef enum RTSPDATAType {
	RTSP_DATATYPE_ERR_OK				=0xffff,
	RTSP_DATATYPE_ERR_UNKNOW		=(-0xffff),
	RTSP_DATATYPE_ERR_CMD		=-6,
	RTSP_DATATYPE_ERR_NOMAGICNUM=-5,
	RTSP_DATATYPE_ERR_CHANNLE	=-4,
	RTSP_DATATYPE_ERR_DATALEN	=-3,
	RTSP_DATATYPE_ERR_FIFOLEN_TOOSMALL=-2,
	RTSP_DATATYPE_ERR_FIFOLEN		=-1,
	RTSP_DATATYPE_RTP				=0x00,
	RTSP_DATATYPE_RTCP			=0x01,
}RTSPDATAType;
typedef enum{
	INTERFACE_INTYPE_UNKNOW	=0x00,
	INTERFACE_INTYPE_GLOBAL,
	INTERFACE_INTYPE_USER,	
	INTERFACE_INTYPE_USER_PPPOE,	
	INTERFACE_INTYPE_USER_LINKDOWN,	
	INTERFACE_INTYPE_USER_NOPPPOE
}INTERFACEINType;

typedef enum RTSPPAYLOADType {
	RTP_PAYLOADTYPE_PCMU=0x00, 
	RTP_PAYLOADTYPE_PCMA=8, 
	RTP_PAYLOADTYPE_MPA=14, 
	RTP_PAYLOADTYPE_MP2T=33, 
	RTP_PAYLOADTYPE_CUSTOM=96,
	RTSP_PAYLOADTYPE_UNKNOW=0xff,
}RTPPAYLOADType;

typedef struct{
	ms_bool init;
	ms_u08 localhostipin[128];		//ip or interface
	ms_u08 localhostipout[128];		//ip or interface
	ms_u08 *ppp_localhostipin;		//ip or interface
	ms_u08 interface_in[32];			//ip or interface
	ms_u08 interface_out[32];		//ip or interface	
	ms_u08 *ppp_interfacein;			//ip or interface
	INTERFACEINType interfaceintype_in;			
	INTERFACEINType interfaceintype_out;	
	ms_bool ischange_defaultgw;
	ms_bool is_pppoe;
}IFInfo;

 typedef struct URLInfo{
 	ms_s08 protocol_name[32];
	ms_s08 hostname[256];
	ms_s08 authorization[64];
	ms_s08 path[mscfg_maxlen_url];  
	ms_s32   port;
	ms_bool flag_ipv6;

	ms_bool flag_useipv6;
} URLInfo;

 typedef struct FILEOpt{
	ms_s08 newname[128];	
}FILEOpt;

typedef struct RTSPINTERLEAVEDFRAMEContext{
	ms_u08 magic;
	ms_u08 channel;
	ms_u32 len;
}RTSPINTERLEAVEDFRAMEContext;

typedef struct RTPHEADERContext{
	ms_bool isrtp;
	ms_bool  init;
	ms_u08 cc;
	ms_u08 x;
	ms_u08 p;
	ms_u08 v;
	ms_u08 pt;
	ms_u08 m;
	ms_u16 sequence_num;
	ms_u32 timestamp_base;
	ms_u32 timestamp;
	ms_u32 ssrc;
	ms_u32 csrc[512];
//extern		
	ms_u16 profile_data;
	ms_u16 len_header_extension;
	ms_u32 header_extension[512];

	ms_u32 header_total_len;
}RTPHEADERContext;

typedef struct RTPOVERTCPHEADERContext{
	RTSPINTERLEAVEDFRAMEContext rtspif_ctt;
	RTPHEADERContext rh_ctt;
	ms_s32 payload_len;
}RTPOVERTCPHEADERContext;

typedef struct   RTPPKTFIFOContext{
	ms_bool flag_use;
	ms_u16 sequence_num;	
	ms_u32 data_len;
	ms_byte data[1500];	
}RTPPKTFIFOContext;
#define MAXNUM_FIFO_RTPPKT	20
typedef struct   RTPContext{
	RTSPINTERLEAVEDFRAMEContext  rtspif_ctt;
	RTPHEADERContext    rtpheader_ctt;
	//sequencenum check uses	
	ms_u16 sequence_num_pre;
	ms_u16 sequence_num_need;
	ms_bool flag_sequencenum_check;
	RTPPKTFIFOContext *prtppktfifo_ctt;
	ms_u16 rtptfifo_ptknum;
}RTPContext;

 typedef struct UDPOpt{
	ms_string localhostipin;
	ms_string interfacein;		//ip or interface
	ms_string interfaceout;		//ip or interface
	ms_string localhostipout;
	ms_string client_hostname;
	
	ms_s32 client_prot;
	ms_s32 block;
	ms_s32 rw_timeout; 	
	ms_s32 reuse_socket;		//must be int type
	ms_s32 fifo_size;
	ms_bool enable_rpfilter;
	ms_u08 num_include_sources;
	ms_u08 num_exclude_sources;
	ms_u08 include_sources[mscfg_max_whitelist][32];
	ms_u08 exclude_sources[mscfg_max_whitelist][32];
} UDPOpt;

typedef struct TCPOpt{
	ms_s32 flag_scanport;
	ms_s32 block;				
	ms_s32 listen;	
	ms_s32 open_timeout; 
	ms_s32 rw_timeout; 		
	ms_s32 recv_buffer_size;	
	ms_s32 send_buffer_size;	
	ms_s32 nodelay;			
	//listen ==1 ,uses
	ms_s32 reuse;
	ms_s32 backlog;
} TCPOpt;
typedef struct TSSLOpt{
	ms_s32 block;				
	ms_s32 listen;	
	ms_s32 open_timeout; 
	ms_s32 rw_timeout; 		
	ms_s32 recv_buffer_size;	
	ms_s32 send_buffer_size;	
	ms_s32 nodelay;			
	//listen ==1 ,uses
	ms_s32 reuse;
	ms_s32 backlog;
	ms_s32 fd_tcp;	
} TSSLOpt;
typedef struct SRTOpt{	
	ms_s32 block;	
	ms_s32 listen;		
	ms_s32 rw_timeout; 		
	ms_s32 recv_buffer_size;	
	ms_s32 send_buffer_size;		
	//listen ==1 ,uses
	ms_s32 rendezvous;
	ms_s32 peer_port;
	ms_s32 reuse;
	ms_s32 backlog;
	// res
	ms_s32 open_timeout;	
	
	//read
	ms_s08 clientip[128];
} SRTOpt;

typedef struct HTTPOpt{
	ms_s32 listen;
	ms_u08 range_type;
	ms_u32 range_byte;
	ms_u08 accept_type;
	ms_u08 connect_type;
	ms_s32 rw_timeout; 	
	ms_byte user_agent[128];
	ms_byte context_type[128];	//application/json
	ms_byte context[1024];

	ms_bool flag_302return;
} HTTPOpt;

typedef struct {
  #if defined(OS_ANDROID)||defined(OS_WIN32) ||defined(OS_PLATFORM_ARM64)
  	ms_s32 len;
  #else
	socklen_t len;
 #endif
	struct sockaddr_in sockaddrs;
 #if HAVE_IPV6
 	struct sockaddr_in6 sockaddrs6;
 #endif
	ms_u08 has_info;
}CLIENTInfo;

  typedef struct MSAVOpt{
	ms_u32 fifo_size;
} MSAVOpt;

typedef struct RTSPTRANSPORTInfo{
	ms_s08 source[80]; 
	ms_s32 ssrc;
	ms_s32 serverport_rtp;
	ms_s32 serverport_rtcp;
}RTSPTRANSPORTInfo;

  typedef struct RTSPOpt{
	ms_u08 lower_transport;	
	ms_u08 transport;
	ms_pvoid url_rtspctt;
	ms_pvoid url_rtpctt;
	ms_pvoid url_rtcpctt;	
	ms_pvoid tcp_func;	
	ms_pvoid udp_func;	
	ms_pvoid rtp_func;
	ms_u32 rtp_port;
	ms_bool is_connected;
	THREADstate thread_started;	//0-unknow;1-start;2-stop
	MSLOCKContext mslock_ctt;
	RTSPTRANSPORTInfo transport_info;
	ms_bool is_rtpconnected;	
	ms_bool is_mpegts;		//
	ms_bool flag_authorization;		//

//user_io	
	ms_u08 timesec_beatheart;
/* BEGIN: Added by sugao<sugao_cn@163.com>, 2019/11/11   PN:001,Server   ANNOUNCE used */
	ms_s08 announce_url[mscfg_maxlen_url];
/* END:   Added by sugao<sugao_cn@163.com>, 2019/11/11 */
}RTSPOpt;
    typedef struct RTPOpt{
	ms_u08 payload_ype;	
	ms_bool isrtp;
	ms_bool is_offset;
	//test use,20210802
	ms_bool flag_rtpovertcp;
}RTPOpt;
    typedef struct HLSOpt{
	ms_s08 vodpath[128];	
	ms_u32 list_size;
	ms_float time;
}HLSOpt;

 typedef struct URLContext{
	ms_s32 fd;
	ms_s32 flags;
	ms_bool is_connected;
	ms_bool is_cmdstop;
	ms_bool is_rtspstream;		//use to flag this is a rtsp stream,otherwise it is a rtp stream 
	ms_pvoid priv_data;
	ms_pvoid priv_data2;		//now, for rtp uses
	ms_string interfacein;
	ms_string interfaceout;
	
	union{
		FILEOpt file_opt;
		UDPOpt  udp_opt;
		TCPOpt  tcp_opt;
		TSSLOpt tssl_opt;
		SRTOpt  srt_opt;
		HTTPOpt http_opt;		
		MSAVOpt msav_opt;
		RTSPOpt rtsp_opt;
		RTPOpt rtp_opt;
		HLSOpt hls_opt;
	}urlctt_opt;
	CLIENTInfo peer_info;
	ms_bool is_location;	
	ms_s08 location[mscfg_maxlen_url];	
	URLInfo url_info;//set by call msstr_api_urlsplit	
	ms_s08 url[mscfg_maxlen_url];
	ms_s08 url_bak[mscfg_maxlen_url];

	ms_bool flag_urlchange;	
} URLContext;

typedef enum {
	STREAM_UNKNOW	=0x00,
	STREAM_LIVE	,
	STREAM_FILE_VOD	,
	STREAM_LIVE_VOD	,
}STREAMType;

typedef struct {
	ms_s64 (*url_seek)( URLContext *, ms_s64, ms_s32);	
	ms_s32 (*url_check)(URLContext *, ms_s32);
	ms_s32 (*url_remove)(URLContext *);
	ms_s32 (*url_rename)(URLContext *);	
	ms_s32 (*url_empty)(URLContext *);
}PRIVEFile;
typedef struct {
	ms_s32 (*accept)(URLContext *);
	ms_s32 (*get_listen_handle)(URLContext *);
	ms_s32 (*shutdown)(URLContext *);
	ms_s32 (*get_window_size)(URLContext *);
}PRIVETcp;
typedef struct {
	ms_s32 (*accept)(URLContext *);
	ms_s32 (*get_listen_handle)(URLContext *);
}PRIVETssl;
typedef struct {
	ms_s32 (*accept)(URLContext *);
	ms_s32 (*get_listen_handle)(URLContext *);
}PRIVESrt;
typedef struct {
	ms_s32 (*accept)(URLContext *s);
	ms_s32 (*get_listen_handle)(URLContext *);
}PRIVEHttp;
typedef struct {
	ms_s32 (*play)(URLContext *);
	ms_s32 (*pause)(URLContext *);
	ms_s32 (*seek)(URLContext *,int64_t );
}PRIVERtsp;

typedef struct {
	ms_u32 (*playlist_totalnum)(URLContext *s);
	ms_u32 (*list_size)(URLContext *s);
	ms_u32 (*m3u8_parase)(URLContext *s);
}PRIVEHls;

typedef struct URLProtocol{
	ms_cstring name;
	ms_cstring longname;
	ms_s32 (*url_open)(URLContext *);
	ms_s32 (*url_read)(URLContext *, ms_pu08 , ms_s32 );
	ms_s32 (*url_write)(URLContext *,ms_pu08 , ms_s32 );
	ms_s32 (*url_close)(URLContext *);
	ms_string (*url_info)(URLContext *);
	union{
		PRIVEFile 	file;
		PRIVETcp 	tcp;
		PRIVETssl tssl;
		PRIVESrt 	srt;
		PRIVEHttp 	http;
		PRIVERtsp 	rtsp;
		PRIVEHls 	hls;
	}priv_func;
	ms_s32 flags;
	struct URLProtocol *next;
}URLProtocol;

typedef struct M3U8Context {
	ms_u08 resolution[64];	
	ms_u32 bandwidth;	
	ms_u08 url[256];	
} M3U8Context;

typedef enum{
	content_type_unknow=0x00,
	content_type_mpegurl,
	content_type_mp2t,
}ENUM_CONTENTType;
typedef struct HTTPContext {
	URLContext tcpctt;
	ms_pbyte replyfiledata;
	ms_s32 replyfiledata_len;
	URLProtocol *tcp_ptl;
	
	ms_u08 method[32];
	ms_s32 status_code;
	ms_u08 des[128];	
	ms_u32 content_len;
	ENUM_CONTENTType content_type;	// 0-unknow,1-x-mpegurl,2-mp2t
	ms_bool flag_chunked;
} HTTPContext;

typedef struct URLVal{
	URLContext	url_ctt;
	URLProtocol	 *purl_ptl;
	ENUPROTOCOLIndex ptl_index;
	ms_s64 base_sys;
	//mpegts use
	ms_s64 Etimep_pat_pmt;
}URLVal;

typedef struct  WEBHOSTInfo{
	unsigned char server_host[256];
	unsigned char server_ip[256];
	unsigned char server_port[256];
	ms_bool flag_ipv6;
	ms_byte url_param[512];
}WEBHOSTInfo;
typedef struct  RTSPREQUESTData{
	unsigned char CSeq[128];
	unsigned char Transport[128];
	unsigned char Range[128];	
	unsigned char Session[128];
	unsigned char pscAccept[128];
}RTSPREQUESTData;
typedef struct  HTTPREQUESTData{
	WEBHOSTInfo webhost_info;
}HTTPREQUESTData;

typedef struct  WEBREQUESTContext{
	ENUPROTOCOLIndex ptl_index;
	ms_byte method[32];
	ms_byte url[512];
	ms_byte protocol[32];
	ms_byte user_agent[512];
	ms_bool falg_hascontent;
	ms_byte content_type[256];
	ms_byte content[5120];
	ms_byte connection[mscfg_maxlen_recv+1];
	ms_u32  content_len;
	union{
		HTTPREQUESTData http_rd;
		RTSPREQUESTData rtsp_rd;
	}extern_data;
	ms_bool flag_lineend;
}WEBREQUESTContext;


typedef enum RTSP_CODE {
//1xx:Notification - Request received, proceed	
	RTSP_STATUS_CONTINUE             				=100,
//2XX: Success - the operation was successfully received, understood, and accepted		
	RTSP_STATUS_OK                  	 				=200,
	RTSP_STATUS_CREATED              				=201,
	RTSP_STATUS_LOW_ON_STORAGE_SPACE 	=250,
//3XX: Redirection - Further action must be taken to complete the request.	
	RTSP_STATUS_MULTIPLE_CHOICES     			=300,
	RTSP_STATUS_MOVED_PERMANENTLY    		=301,
	RTSP_STATUS_MOVED_TEMPORARILY    		=302,
	RTSP_STATUS_SEE_OTHER            				=303,
	RTSP_STATUS_NOT_MODIFIED         			=304,
	RTSP_STATUS_USE_PROXY            				=305,
//4XX: Client error - request has syntax error or cannot be implemented
	RTSP_STATUS_BAD_REQUEST          			=400,
	RTSP_STATUS_UNAUTHORIZED         			=401,
	RTSP_STATUS_PAYMENT_REQUIRED   		=402,
	RTSP_STATUS_FORBIDDEN            				=403,
	RTSP_STATUS_NOT_FOUND            				=404,
	RTSP_STATUS_METHOD               				=405,
	RTSP_STATUS_NOT_ACCEPTABLE       			=406,
	RTSP_STATUS_PROXY_AUTH_REQUIRED  		=407,
	RTSP_STATUS_REQ_TIME_OUT         			=408,
	RTSP_STATUS_GONE                 					=410,
	RTSP_STATUS_LENGTH_REQUIRED      			=411,
	RTSP_STATUS_PRECONDITION_FAILED  		=412,
	RTSP_STATUS_REQ_ENTITY_2LARGE    		=413,
	RTSP_STATUS_REQ_URI_2LARGE       			=414,
	RTSP_STATUS_UNSUPPORTED_MTYPE    		=415,
	RTSP_STATUS_PARAM_NOT_UNDERSTOOD 	=451,
	RTSP_STATUS_CONFERENCE_NOT_FOUND 	=452,
	RTSP_STATUS_BANDWIDTH            			=453,
	RTSP_STATUS_SESSION              				=454,
	RTSP_STATUS_STATE                				=455,
	RTSP_STATUS_INVALID_HEADER_FIELD 		=456,
	RTSP_STATUS_INVALID_RANGE        			=457,
	RTSP_STATUS_RONLY_PARAMETER      		=458,
	RTSP_STATUS_AGGREGATE            				=459,
	RTSP_STATUS_ONLY_AGGREGATE       			=460,
	RTSP_STATUS_TRANSPORT            				=461,
	RTSP_STATUS_UNREACHABLE          			=462,
//5XX: Server side error - the server was unable to fulfill a valid request	
	RTSP_STATUS_INTERNAL             				=500,
	RTSP_STATUS_NOT_IMPLEMENTED     	 		=501,
	RTSP_STATUS_BAD_GATEWAY          			=502,
	RTSP_STATUS_SERVICE             	 			=503,
	RTSP_STATUS_GATEWAY_TIME_OUT			=504,
	RTSP_STATUS_VERSION              				=505,
	RTSP_STATUS_UNSUPPORTED_OPTION   		=551,
}ENUM_RTSP_CODE;

#define	UNVALID_FD		-1 

#define PTLNAME_FILE		"file"
#define PTLNAME_MSOSHLS		"msoshls"
#define PTLNAME_HLS		"hls"
#define PTLNAME_HTTP	"http"
#define PTLNAME_RTMP	"rtmp"
#define PTLNAME_RTMPT	"rtmpt"
#define PTLNAME_RTMPE	"rtmpe"
#define PTLNAME_RTMPTE	"rtmpte"
#define PTLNAME_RTMPS	"rtmps"
#define PTLNAME_MSAV	"msav"
#define PTLNAME_NONE	"none"
#define PTLNAME_PIPE		"pipe"
#define PTLNAME_FIFO		"fifo"
#define PTLNAME_RTP		"rtp"
#define PTLNAME_RTSP	"rtsp"
#define PTLNAME_TCP		"tcp"
#define PTLNAME_TSSL	"tssl"
#define PTLNAME_SRT		"srt"
#define PTLNAME_RAW	"raw"
#define PTLNAME_UDP		"udp"
#define PTLNAME_UDPLITE	"udplite"


#define PTLDES_FILE		"Use to control(read,write,remove,rename and so on) file and context"
#define PTLDES_HLS		"HTTP Live Streaming,include a m3u8 file and many segment"
#define PTLDES_HTTP		"HyperText Transfer Protocol,used in web and vod"
#define PTLDES_RTMP		"Real Time Messaging Protocol"
#define PTLDES_RTMPT	"Real Time Messaging Protocol over http"
#define PTLDES_RTMPE	"Real Time Messaging Protocol with Encryption"
#define PTLDES_RTMPTE	"Real Time Messaging Protocol over http with Encryption"
#define PTLDES_RTMPS	"Real Time Messaging Protocol over https"
#define PTLDES_MSAV		"Use protocol done by ffmpeg"
#define PTLDES_NONE		"Do nothing"
#define PTLDES_PIPE		"Use to control(read,write,remove,rename and so on) pipe"
#define PTLDES_FIFO		"Use to control(read,write,remove,rename and so on) fifo"
#define PTLDES_RTP		"Real-time Transport Protocol"
#define PTLDES_RTSP		"Real Time Streaming Protocol"
#define PTLDES_TCP		"Transmission Control Protocol"
#define PTLDES_TSSL		"Transmission Control Protocol  over ssl"
#define PTLDES_SRT		"Secure Reliable Transport Protocol"
#define PTLDES_UDP		"User Datagram Protocol"
#define PTLDES_RAW		"OP raw data"
#define PTLDES_UDPLITE	"Use to recv or send udp data,but checksum ChecksumCoverage byte"


#define msenv_ptl_rtsptransport				"msenv_ptl_rtsptransport"
#define msenv_ptl_rtpsequencenum			"msenv_ptl_rtpsequencenum"
#define msenv_ptl_useragent				"msenv_ptl_useragent"
#define msenv_ptl_hasipv6				"msenv_ptl_hasipv6"


#define msinfo_sdp \
	"v=0\r\n" \
	"o=- 1464329709034587 1 IN IP4 127.0.0.1\r\n" \
	"s=MPEG Transport Stream, streamed by msavskit(Email:msavskit@163.com)\r\n" \
	"m=video 0 RTP/AVP 33\r\n" \
	"c=IN IP4 0.0.0.0\r\n" \
	"a=control:trackid1\r\n"

#define OPTVAL_INIT(optname,optval,unvalid_val,default_val) \
	if(unvalid_val==optval){ \
		ms_verbose("%s:unset value,set to default %d",optname,default_val); \
		optval=default_val; \
	}else{ \
		ms_verbose("%s:set value to %d",optname,optval); \
	}
#define OPTVAL_INIT_VAILD(optname,optval,valid_val,default_val) \
	if(valid_val!=optval){ \
		ms_verbose("%s:unvalue,set to default %d",optname,default_val); \
		optval=default_val; \
	}else{ \
		ms_verbose("%s:set value to %d",optname,optval); \
	}
#define CONFIG_RTSPBASE_RTPPORT		51000
#define CONFIG_MAXPORT				65535
#define RTP_MAGIC_NUM       0x24
#define RTP_INTERLEAVERFRAME_LEN       4
#define RTP_HEASER_LEN	12
#ifndef MSPROTOCOL_C
//rtp(tcp)  interleaverframe
extern ms_void 
	msprotocol_api_rtpiframe_debug(RTSPINTERLEAVEDFRAMEContext *prtspif_ctt,ms_bool enable_debug);
extern RTSPDATAType 
	msprotocol_api_rtpiframe_drop(MSFIFOSIZEContext  *plist_stream_fifoctt,ms_pbyte recvbuf,int *plen);
extern ms_s32 
	msprotocol_api_rtpiframe_set(ms_u08 *buf,ms_s32 len,ms_bool is_offset);
extern RTSPDATAType  
	msprotocol_api_rtpiframe_get(RTSPINTERLEAVEDFRAMEContext *prtspif_ctt,ms_u08 *buf,ms_s32 len);
extern ms_void 
	msprotocol_api_rtpiframe_bufdebug(ms_u08 *buf,ms_s32 len,ms_bool enable_debug);
extern ms_void 
	msprotocol_api_rtpheader_debug(RTPHEADERContext *prh_ctt,ms_bool enable_debug);
extern ms_void 
	msprotocol_api_rtpheader_get(RTPHEADERContext *prh_ctt,ms_u08 *buf,ms_s32 len,ms_bool debug);
extern ms_s32  
	msprotocol_api_rtpenc(RTPHEADERContext ms_in *prh_ctt,ms_u08 payload,ms_s08 ms_out *buf ,ms_s32 ms_out size,ms_bool is_offset);
extern ms_void 
	msprotocol_api_rtpheader_bufdebug(RTPContext * prtp_ctt,ms_string desinfo,ms_u08 *buf,ms_s32 len,ms_bool enable_debug);
extern ms_bool 
	msprotocol_api_rtpheader_checksequence(RTPContext * prtp_ctt,ms_string desinfo,ms_u08 *buf,ms_s32 len);
extern ms_void 
	msprotocol_api_rtpovertcpheader_bufdebug(ms_u08 *buf,ms_s32 len,ms_bool enable_debug);
extern ms_bool 
	msprotocol_api_rtpcheck(RTPHEADERContext *prh_ctt,ms_u08 *buf, int len ,ms_bool flag_checkiframe);
extern ms_bool 
	msprotocol_api_rtpcheck_mp2t_mpa(RTPHEADERContext *prh_ctt,ms_u08 *buf, int len ,
	ms_bool flag_mpa2rtp,ms_byte *pdatatype_times,ms_bool *pflag_reset,ms_string name_des);
extern ms_s32 
	msprotocol_api_rtpfifoout(RTPContext * prtp_ctt,ms_u08 **ppbuf, int *len );
extern void 
	msprotocol_api_rtpdec(RTPContext * prtp_ctt,ms_u08 *buf, int *len ,ms_bool flag_checkiframe);
extern ms_u32 
	msprotocol_api_contextlen(char *inbuf,ms_u32 len);
extern ms_s08 
	msprotocol_api_init(ms_void);
extern ms_void 
	msprotocol_api_deinit(ms_void);
extern ms_void 
	msprotocol_api_infofunc(ms_string ms_out pbuf);
extern ms_string 
	msprotocol_api_version(ms_void);
extern ms_void 
	register_protocol(ms_void);
extern void * 
	msprotocol_api_match(const ms_s08 *name);
extern URLProtocol * 
	msprotocol_api_matchurl(const ms_s08 *url);
extern URLProtocol * 
	msprotocol_api_matchurl2(const ms_u08 *url,URLInfo *purl_info);
extern ENUPROTOCOLIndex 
	msprotocol_api_getprotocol(char *url,char *format,ms_bool isvod);
extern ms_s32 
	msprotocol_api_io(URLContext *pcilent_ctt, ms_pu08 buf, ms_s32 len,ms_s32 (*url_io)(URLContext *, ms_pu08 , ms_s32 ));
#endif

#define mssrtepollin	0x1		//SRT_EPOLL_IN
#define mssrtepollout	0x4		//SRT_EPOLL_OUT
#define mssrtepollerr	0x8		//SRT_EPOLL_ERR
typedef enum MSSRT_SOCKSTATUS {
	MSSRTS_INIT = 1,
	MSSRTS_OPENED,
	MSSRTS_LISTENING,
	MSSRTS_CONNECTING,
	MSSRTS_CONNECTED,
	MSSRTS_BROKEN,
	MSSRTS_CLOSING,
	MSSRTS_CLOSED,
	MSSRTS_NONEXIST
} MSSRT_SOCKSTATUS;
#ifndef SRT_C
extern MSSRT_SOCKSTATUS mssrt_sockstate(int srt_fd );
extern int mssrt_send(int srt_fd, const  ms_u08 *buf, int size);
extern int mssrtepoll_open(ms_void );
extern int mssrtepoll_add(int epfd, int ev_fd, const int* opt);
extern int mssrtepoll_mod(int epfd, int ev_fd, const int* opt);
extern int mssrtepoll_del(int epfd, int ev_fd);
extern int mssrtepoll_wait(int epfd, int *pfds,char is_out);
extern int mssrtepoll_wait_timeout(int epfd, int *pfds,char is_out,int timeout_ms);
extern int mssrtepoll_wait_block(int epfd, int *pfds,char is_out);
extern int mssrtepoll_close(int epfd);
extern ms_void msprotocol_api_parserequest(char *inbuf,WEBREQUESTContext *prequest_ctt);
#endif
#endif

