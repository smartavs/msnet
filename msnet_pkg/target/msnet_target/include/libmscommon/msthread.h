#ifndef MSTHREAD_H
#define MSTHREAD_H

#include <libmslog/mstype.h>

typedef enum{
	THREAD_STATE_UNKNOW		=0,
	THREAD_STATE_INIT,
	THREAD_STATE_STARTING,
	THREAD_STATE_START,
	THREAD_STATE_STOPPING,	
	THREAD_STATE_STOP,	
}THREADstate;
typedef struct MSTHREADContext{
#if defined(OS_ANDROID)||defined(OS_LINUX_SOC)
	pthread_t thread_id;		
#elif defined(OS_WIN32)
	unsigned long thread_id;
#endif
	ms_u32  use_cpuindex;
	pthread_mutex_t mutex;
	ms_bool init;
	ms_bool debug;
	ms_u08 name[128];
	THREADstate state;
}MSTHREADContext;

//typedef void* (*MSTHREADFunc)(void*);

#define MSTHREADFunc ms_pvoid

typedef struct MSLOCKContext{
	ms_u08 name[128];
	pthread_mutex_t mutex;
	ms_bool init;
	ms_bool debug;
	ms_bool flag_used;
	ms_byte owner_info[128];
}MSLOCKContext;

typedef struct MSLOCKLISTContext{	
	MSLOCKContext *pmslock_cct;
	ms_bool flag_used;
}MSLOCKLISTContext;
#define maxnum_locklist 5120

#ifndef MSTHREAD_C
extern ms_void 
	msthread_api_envinit(ms_void);
extern MSLOCKLISTContext *
	mslocklist_api_init(ms_void);
extern ms_void 
	mslocklist_api_show(MSLOCKLISTContext *pmslocklist_ctt);
extern ms_void 
	mslocklist_api_deinit(MSLOCKLISTContext *ppmslocklist_ctt);

extern ms_void 
	msthread_api_setcurname(ms_string ms_in name);
extern ms_void 
	msthread_api_setcurname(ms_string ms_in name);
extern ms_s08 
	msthread_api_create(MSTHREADContext * ms_io pmsthread_ctt,ms_string ms_io name,MSTHREADFunc ms_in msthrad_func,ms_pvoid ms_in arg);
extern ms_s08 
	msthread_api_destroy(MSTHREADContext * ms_io pmsthread_ctt);
extern ms_s08 
	msthread_api_lock(MSTHREADContext * ms_in pmsthread_ctt);
extern ms_s08 
	msthread_api_unlock(MSTHREADContext * ms_in pmsthread_ctt);
extern ms_bool 
	msthread_api_waitstate_timeout(MSTHREADContext * ms_in pmsthread_ctt,THREADstate tstte_init,THREADstate tstte_wait,ms_u32 timeout_ms,ms_u32 freq_ms);	//100ms
extern ms_bool 
	msthread_api_waitstartingcmd(MSTHREADContext * ms_in pmsthread_ctt,ms_u32 timeout_ms,ms_u32 freq_ms);
extern ms_bool  
	msthread_api_isstop(MSTHREADContext * ms_in pmsthread_ctt);
extern ms_s08 
	mslock_api_init(MSLOCKContext * ms_io pmslock_ctt,ms_string ms_in des);
extern ms_s08 
	mslock_api_dodes(MSLOCKContext * ms_in pmslock_ctt,ms_cstring ms_in func, ms_u32 ms_in line);
extern ms_s08 
	mslock_api_undodes(MSLOCKContext * ms_in pmslock_ctt,ms_cstring ms_in func, ms_u32 ms_in line);
extern ms_s08 
	mslock_api_deinit(MSLOCKContext * ms_io pmslock_ctt);
extern ms_bool 
	mslock_api_waitstate_timeout(MSLOCKContext * ms_io pmslock_ctt,THREADstate * ms_in pthreadstate_g,THREADstate tstte_init,THREADstate tstte_wait,ms_u32 timeout_ms,ms_u32 freq_ms);	//100ms
extern ms_bool  
	mslock_api_waitstartingcmd(MSLOCKContext * ms_io pmslock_ctt,THREADstate * ms_in pthreadstate_g,ms_u32 timeout_ms,ms_u32 freq_ms);
extern MSLOCKLISTContext mslocklist_ctt[maxnum_locklist];
#endif

#define mslock_api_do(pmslock_ctt)		mslock_api_dodes(pmslock_ctt,__FUNCTION__,__LINE__);
#define mslock_api_undo(pmslock_ctt)	mslock_api_undodes(pmslock_ctt,__FUNCTION__,__LINE__);
#endif