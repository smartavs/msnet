#ifndef MSCOMMON_H
#define MSCOMMON_H

#include "mscommon_inner.h"
/*****************************************************************************
 Description  : symbol like dll ,so,i/o
*****************************************************************************/
#define ms_dllsymbol	DLL_SYMBOL
#define ms_i		
#define ms_o		
#define ms_io		

/*****************************************************************************
 Description  : return out
*****************************************************************************/
#define ms_averror(e) 		(-(e))
#define ms_neterrno 		ms_averror(errno)
#define ms_strerr 			(strerror(errno))

/*****************************************************************************
 Description  : system function
*****************************************************************************/
#define ms_assert(expression) 		assert(expression)
#define ms_sprintf					sprintf
#define ms_snprintf				snprintf
#define ms_strcat					strcat
#define ms_remove 				remove
#define ms_rename				rename
#define ms_popen					popen
#define ms_pclose					pclose
#define ms_fopen					fopen
#define ms_fread					fread
#define ms_fwrite					fwrite
#define ms_fclose					fclose
#define ms_mkdir(dir)				mst_mkdir(dir)
#define ms_open					mst_open
#define ms_read					mst_read
#define ms_write					mst_write
#define ms_lseek					mst_lseek
#define ms_close					mst_close
#define ms_access					mst_access
#define ms_rmdir(rm_cmd,dirname) 	ms_sprintfs(rm_cmd,"rm   -fr   %s",dirname); system(rm_cmd);
#define ms_sleep(sencod)			mst_sleep(sencod)
#define ms_msleep(ms)				mst_usleep(ms*1000)
#define ms_usleep(us)				mst_usleep(us)

#define ms_atoi(buf) 		atoi(buf)
#define ms_atol(buf)		atol(buf)
#define ms_exit(ret)		exit(ret)
#define ms_exiterr			ms_exit(1)
#define ms_exitok			ms_exit(0)
/*****************************************************************************
 Description  : min and max
*****************************************************************************/
#define ms_min(a,b) 				((a) > (b) ? (b) : (a))
#define ms_max(a,b) 				((a) > (b) ? (a) : (b))
#define ms_min3(a,b,c) 			ms_min(ms_min(a,b),c)
#define ms_max3(a,b,c) 			ms_max(ms_max(a,b),c)
#define ms_min4(a,b,c,d) 			ms_min(ms_min3(a,b,c) ,d)
#define ms_min5(a,b,c,d,e) 			ms_min(ms_min4(a,b,c,d),e)
#define ms_min6(a,b,c,d,e,f) 		ms_min(ms_min5(a,b,c,d,e),f)
#define ms_min7(a,b,c,d,e,f,g) 		ms_min(ms_min6(a,b,c,d,e,f),g)
#define ms_min8(a,b,c,d,e,f,g,h) 		ms_min(ms_min7(a,b,c,d,e,f,g),h)

/*****************************************************************************
 Description  : network and host data
*****************************************************************************/
#define ms_network_8b(buf,val)		NETWORK_8BYTE(buf,val)
#define ms_network_4b(buf,val)		NETWORK_4BYTE(buf,val)
#define ms_network_3b(buf,val)		NETWORK_3BYTE(buf,val)
#define ms_network_2b(buf,val)		NETWORK_2BYTE(buf,val)
#define ms_network_4b_lf(buf,val)	NETWORK_4BYTE_LF(buf,val)
#define ms_network_2b_lf(buf,val)	NETWORK_2BYTE_LF(buf,val)
#define ms_host_8b(buf)	 	HOST_8BYTE(buf)
#define ms_host_4b(buf)	 	HOST_4BYTE(buf)
#define ms_host_3b(buf)	 	HOST_3BYTE(buf)
#define ms_host_2b(buf)	 	HOST_2BYTE(buf)

#define ms_10t16_08bit(num)	((num/10<<4)|(num%10))
#define ms_10t16_12bit(num)	((num/100<<8)|((num%100)/10<<4)|((num%100)%10))
		
/*****************************************************************************
 Description  : system,support ms_api_cmdline
*****************************************************************************/
#define ms_system(cmdline) 						system(cmdline)
#define ms_cmdline(cmdbuf,fmt,arg...) 				SYS_CMDLINE(cmdbuf,fmt,##arg)
#define ms_cmdline_sync(cmdbuf,fmt,arg...) 		SYS_CMDLINE_SYNC(cmdbuf,fmt,##arg)
#define ms_api_cmdline(cmdline)					mst_innerapi_cmdline(cmdline)
#define ms_read_cmdinfo(cmdline,info ,maxlen)		mst_innerapi_read_cmdinfo(cmdline,info,maxlen)
#define ms_read_cmdinfo2(cmdline,info ,maxlen)	mst_innerapi_read_cmdinfo2(cmdline,info,maxlen)
#define ms_isgrep_cmdinfo(cmdline)				mst_innerapi_isgrep_cmdinfo (cmdline)
#define ms_cmdkill(cmdline)						mst_innerapi_kill(cmdline);
#define ms_cmdcp(cmdbuf,fm,to)  				SYS_CMDCP(cmdbuf,fm,to)
#define ms_cmdbin(cmdbuf,fm,to) 				SYS_CMDCP_BIN(cmdbuf,fm,to)

/*****************************************************************************
 Description  : string and buf 
*****************************************************************************/
#define ms_strlen(buf)					strlen(buf)
#define ms_buflen(buf)					ms_strlen(buf)
#define ms_bufsize(buf)					sizeof(buf)
#define ms_bufunvalid(buf)				((ms_null==buf)||(0==ms_buflen(buf)))
#define ms_arrayelems(a)				(sizeof(a) / sizeof((a)[0]))	
#define ms_strcats(tbuf,obuf,fmt,arg...)	CMD_STRCAT(tbuf,obuf,fmt,##arg)
#define ms_strcat2(obuf, msstring);		if(ms_false==msstr_api_iscasestr(obuf, msstring)){ms_strcat(obuf,msstring);}
#define ms_slprintf						slprintf
#define ms_snprint						mst_snprint
#define ms_snprintfs(outbuf,len,fmt,arg...)	CMD_SNPRINTF(outbuf,len,fmt,##arg)
#define ms_snprints(outbuf,fmt,arg...)		ms_snprintfs(outbuf, ms_bufsize(outbuf), fmt,##arg);
#define ms_sprintfs(outbuf,fmt,arg...)		CMD_SPRINTF(outbuf,fmt,##arg)
#define ms_strcpy(str1,str2)				CMD_STRCPY(str1,str2)
#define ms_memset(addr,val,len)			memset(addr,val,len);
#define ms_memsetbuf(buf)				memset(buf,0,sizeof(buf));
#define ms_memset_stru(buf,type)		memset(buf,0,sizeof(type));
#define ms_buf0(buf)					memset(buf,0,sizeof(buf));
#define ms_memset0(addr,len)			memset(addr,0,len);
#define ms_memcpy(addr1,addr2,len)		memcpy(addr1,addr2,len);
#define ms_dataswitch(addr1,addr2,len)	mst_innerapi_dataswitch(addr1,addr2,len);
#define ms_selfcp(buf,offset_byte,len)		mst_innerapi_selfcp(buf,offset_byte,len) ; 
#define ms_encrypt( data, len, key, keylen, outbuf,  is_enc)	 		mst_innerapi_encrypt( data, len, key, keylen, outbuf,  is_enc);
#define ms_encrypt_outstring(outstr,pbasekey, prefmt, aftfmt)		mst_innerapi_encrypt_outstring(outstr,pbasekey, prefmt, aftfmt);
#define ms_decrypt(instr,pbasekey,flag_mustdecrypt)			mst_innerapi_decrypt(instr,pbasekey,flag_mustdecrypt);
#define ms_gethlsprogramname(hls_url,vodpath, programname)	mst_innerapi_gethlsprogramname(hls_url,vodpath, programname);
#define ms_gethls_outpath(hls_url,vodpath, hls_path)				mst_innerapi_gethlspath(hls_url,vodpath, hls_path);
/*****************************************************************************
 Description  : string cmp 
*****************************************************************************/
#define ms_strncmp_eq(str1,str2,len)		(!strncmp(str1,str2,len))
#define ms_strncmp_neq(str1,str2,len)		(strncmp(str1,str2,len))
#define ms_strncmps_eq(str1,str2)		(ms_strncmp_eq(str1,str2,strlen(str2)))
#define ms_strncmps_neq(str1,str2)		(ms_strncmp_neq(str1,str2,strlen(str2)))
#define ms_strncmps_aeq(str1,str2)		(ms_strncmp_eq(str1,str2,strlen(str2))  &&  ms_strncmp_eq(str2,str1,strlen(str1)))
#define ms_strncmps_naeq(str1,str2)		(!ms_strncmps_aeq(str1,str2))

/*****************************************************************************
 Description  : buf len,malloc and free
*****************************************************************************/
#define ms_malloc(size,name)						MS_MALLOC(size,name)
#define ms_malloc_ret_nodbg(item,size,name,ret)		MALLOC_VAL_RET_NODBG(item,size,name,ret)
#define ms_malloc_ret(item,size,name,ret)				MALLOC_VAL_RET(item,size,name,ret)
#define ms_malloc_ret1(ret,item,size,name)				MALLOC_VAL_RET(item,size,name,ret)
#define ms_malloc_noret(item,size,name)				MALLOC_VAL_NORET(item,size,name)
#define ms_malloc_goto(goval,item,size,name)			MALLOC_VAL_GOTO(goval,item,size,name)
#define ms_malloc_retgoto(goval,ret,item,size,name)		MALLOC_VAL_RETGOTO(goval,ret,item,size,name)

#define ms_calloc_ret_nodbg(item,size,name,ret)		CALLOC_VAL_RET_NODBG(item,size,name,ret)
#define ms_calloc_ret(ret,item,size,name)				CALLOC_VAL_RET(item,size,name,ret)
#define ms_calloc_noret(item,size,name)				CALLOC_VAL_NORET(item,size,name)
#define ms_calloc_goto(goval,item,size,name)			CALLOC_VAL_GOTO(goval,item,size,name)
#define ms_calloc_retgoto(goval,ret,item,size,name)		CALLOC_VAL_RETGOTO(goval,ret,item,size,name)

#define ms_free(addr)						MS_FREE(addr)
#define ms_freep(addr)						MS_FREEP(addr)
#define ms_demalloc(item)  					DEMALLOC_VAL(item)
#define ms_showblock()  	 				MS_SHOWBLOCK()
/*****************************************************************************
 Description  : range
*****************************************************************************/
#define  ms_equal(var1,var2)			(var1==var2) 
#define  ms_noequal(var1,var2)		(var1!=var2) 
#define  ms_lessthan(var,min)		(var<min) 
#define  ms_morethan(var,max)		(var>max)   
#define  ms_noless(var,min)			( !ms_lessthan(var,min)  )
#define  ms_nomore(var,max)		( !ms_morethan(var,max)   )
#define  ms_range(var,min,max)		( ms_noless(var,min) &&ms_nomore(var,max) )			//min =<var <= max
#define  ms_range2(var,min,max)		( ms_morethan(var,min) && ms_lessthan(var,max) )		//min <var <max
#define  ms_rangeout(var,min,max)	( !ms_range(var, min, max))
/*****************************************************************************
 Description  : ++,--
*****************************************************************************/
#define ms_sdd(num)	MS_SDD(num)	
#define ms_udd(num)	MS_UDD(num)	

#define ms_macTostr(  buf, mac_buf )  \
	ms_sprintf( mac_buf, "%02x:%02x:%02x:%02x:%02x:%02x",(unsigned char) *buf, (unsigned char)(*(buf+1)), \
	(unsigned char)(*(buf+2)), (unsigned char)(*(buf+3)), (unsigned char)(*(buf+4)), (unsigned char)(*(buf+5))); \
	mac_buf[17] = 0;
/*****************************************************
* :msfile_filter
*****************************************************/
#define msfile_filter_flag(url,filter_str)  	ms_strncmps_eq((&url[ms_buflen(url)-ms_buflen(filter_str)]), filter_str)
/*****************************************************
* :Byte
*****************************************************/
#define ms_perbit 		(1024)
#define ms_perbyte 	(1024)
#define ms_b			1
#define ms_kb 		(ms_perbit*ms_b)
#define ms_mb		(ms_perbit*ms_kb)
#define ms_gb			(ms_perbit*ms_mb)
#define ms_B			1
#define ms_KB 		(ms_perbyte*ms_B)
#define ms_MB		(ms_perbyte*ms_KB)
#define ms_GB		(ms_perbyte*ms_MB)

#define ms_Bb(byte)		(8*byte)
#define ms_bB(bit)			(bit/8)

#define ms_bkmg_cmd32(bit,wh,wl)			ms_bitbyte32((bit/wh)	, ((bit%wh)/wl)	)
#define ms_bkmg_cmd64(bit,wh,wl)			ms_bitbyte64((bit/wh)	, ((bit%wh)/wl)	)
#define ms_bkmg_cmd32_unit(bit,wh,wl,str)		ms_bitbyte32_unit((bit/wh)	, ((bit%wh)/wl),str	)
#define ms_bkmg_cmd64_unit(bit,wh,wl,str)		ms_bitbyte64_unit((bit/wh)	, ((bit%wh)/wl),str	)
//b
#define ms_b32_unit(bit)				ms_bkmg_cmd32_unit(bit,ms_b,1,"b")
#define ms_b64_unit(bit)				ms_bkmg_cmd64_unit(bit,ms_b,1,"b")
//kb
#define ms_btkb32(bit)					ms_bkmg_cmd32(bit,ms_kb,ms_b)
#define ms_btkb64(bit)					ms_bkmg_cmd64(bit,ms_kb,ms_b)
#define ms_btkb32_unit(bit)				ms_bkmg_cmd32_unit(bit,ms_kb,ms_b,"Kb")
#define ms_btkb64_unit(bit)				ms_bkmg_cmd64_unit(bit,ms_kb,ms_b,"Kb")
//mb
#define ms_btm32(bit)					ms_bkmg_cmd32(bit,ms_mb,ms_kb)
#define ms_btm64(bit)					ms_bkmg_cmd64(bit,ms_mb,ms_kb)
#define ms_btm32_unit(bit)				ms_bkmg_cmd32_unit(bit,ms_mb,ms_kb,"Mb")
#define ms_btm64_unit(bit)				ms_bkmg_cmd64_unit(bit,ms_kb,ms_kb,"Mb")
//gb
#define ms_btg32(bit)					ms_bkmg_cmd32(bit,ms_gb,ms_mb)
#define ms_btg64(bit)					ms_bkmg_cmd64(bit,ms_gb,ms_mb)
#define ms_btg32_unit(bit)				ms_bkmg_cmd32_unit(bit,ms_gb,ms_mb,"Gb")
#define ms_btg64_unit(bit)				ms_bkmg_cmd64_unit(bit,ms_gb,ms_mb,"Gb")
//B
#define ms_B32_unit(byte)				ms_bkmg_cmd32_unit(byte,ms_B,1,"B")
#define ms_B64_unit(byte)				ms_bkmg_cmd64_unit(byte,ms_B,1,"B")
//KB
#define ms_BTKB32(byte)				ms_bkmg_cmd32(byte,ms_KB,ms_B)
#define ms_BTKB64(byte)				ms_bkmg_cmd64(byte,ms_KB,ms_B)
#define ms_BTKB32_unit(byte)			ms_bkmg_cmd32_unit(byte,ms_KB,ms_B,"KB")
#define ms_BTKB64_unit(byte)			ms_bkmg_cmd64_unit(byte,ms_KB,ms_B,"KB")
//MB
#define ms_BTM32(byte)				ms_bkmg_cmd32(byte,ms_MB,ms_KB)
#define ms_BTM64(byte)				ms_bkmg_cmd64(byte,ms_MB,ms_KB)
#define ms_BTM32_unit(byte)			ms_bkmg_cmd32_unit(byte,ms_MB,ms_KB,"MB")
#define ms_BTM64_unit(byte)			ms_bkmg_cmd64_unit(byte,ms_MB,ms_KB,"MB")
//GB
#define ms_BTG32(byte)				ms_bkmg_cmd32(byte,ms_GB,ms_MB)	
#define ms_BTG64(byte)				ms_bkmg_cmd64(byte,ms_GB,ms_MB)
#define ms_BTG32_unit(byte)			ms_bkmg_cmd32_unit(byte,ms_GB,ms_MB,"GB")
#define ms_BTG64_unit(byte)			ms_bkmg_cmd64_unit(byte,ms_GB,ms_MB,"GB")

#define ms_KBTG32(byte)				ms_bkmg_cmd32(byte,(ms_perbyte*ms_perbyte),ms_perbyte)	
#define ms_KBTG64(byte)				ms_bkmg_cmd64(byte,(ms_perbyte*ms_perbyte),ms_perbyte)	
#define ms_KBTG32_unit(byte)			ms_bkmg_cmd32_unit(byte,(ms_perbyte*ms_perbyte),ms_perbyte,"GB")
#define ms_KBTG64_unit(byte)			ms_bkmg_cmd64_unit(byte,(ms_perbyte*ms_perbyte),ms_perbyte,"GB")	

//Byte/Bit to string
#define ms_Bb32AUTO_unit(byte,falg_bit) 		mst_innerapi_bit2str_64((char[32]){0},byte, falg_bit)	
#define ms_Bb64AUTO_unit(byte,falg_bit)		mst_innerapi_bit2str_32((char[32]){0},byte, falg_bit)

#define ms_BYTE32AUTO_unit(byte) 			ms_Bb32AUTO_unit(byte, ms_false)	
#define ms_BYTE64AUTO_unit(byte)			ms_Bb64AUTO_unit(byte, ms_false)
#define ms_BIT32AUTO_unit(bit) 				ms_Bb32AUTO_unit(bit, ms_true)	
#define ms_BIT64AUTO_unit(bit)				ms_Bb64AUTO_unit(bit, ms_true)

#define PATH_HLS    			"/dev/shm"
#define PATH_FASTDB   		"/dev/shm"
#define PATH_PID				"/var/run"

#define PATH_LOG_MSCORE	"/var/log/mscore"
#define PATH_LOG_MSNGINX	"/usr/local/nginx"
#define PATH_LOG_MSSRS		"/usr/local/mssrs/objs/"
#define PATH_LOG_MSTOMCAT	"/usr/local/mstomcat/logs/"
#define FILE_LOG_WARING		"/var/log/mscore/msmsg_waring"
#define FILE_LOG_TASKMSG		"/var/log/mscore/msmsg_task"
#define FILE_LOG_MSNETSAFE	"/var/log/msnetsafe.log"
#define FILE_LOG_CLAMAV		"/var/log/clamav/logs/scan.log"

#define msfile_endline		"\r\n"
#ifndef MSCOMMON_INNER_C
extern ms_void 
	mscommon_api_infofunc(ms_string ms_out pbuf);
extern ms_string 
	mscommon_api_version(ms_void);
#endif

#endif
