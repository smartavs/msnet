#ifndef MSTIME_H
#define MSTIME_H
#include <libmslog/mstype.h>

typedef struct RSTime{
	ms_u32 tmsec;		/* Seconds: 0-59 (K&R says 0-61?) */
	ms_u32 tmmin;		/* Minutes: 0-59 */
	ms_u32 tmhour;		/* Hours since midnight: 0-23 */
	ms_u32 tmday;		/* Day of the month: 1-31 */
	ms_u32 tmmon;		/* Months *since* january: 0-11 */
	ms_u32 tmyear;		/* Years since 1900 */
	ms_u32 tmwday;
}RSTime,* PRSTime;


/*****************************************************
* :Time
*****************************************************/

#define ms_seconds(x) 			(x)
#define ms_minutes(x)			(x*60*ms_seconds(1))
#define ms_hours(x)			(x*60*ms_minutes(1))
#define ms_days(x)				(x*24*ms_hours(1))


#define ms_mseconds(x)		(x)
#define ms_msseconds(x) 		(x*1000*ms_mseconds(1))
#define ms_msminutes(x)		(x*60*ms_msseconds(1))
#define ms_mshours(x)			(x*60*ms_msminutes(1))
#define ms_msdays(x)			(x*24*ms_mshours(1))

#define ms_useconds(x)			(x)
#define ms_usmseconds(x)		(x*1000*ms_useconds(1))
#define ms_usseconds(x) 		(x*1000*ms_usmseconds(1))
#define ms_usminutes(x)		(x*60*ms_usseconds(1))
#define ms_ushours(x)			(x*60*ms_usminutes(1))
#define ms_usdays(x)			(x*24*ms_ushours(1))

#ifndef MSTIME_C
extern ms_void 
	mstime_api_print(PRSTime ms_io prstime);
extern ms_s64 
	mstime_api_prs2sec(PRSTime ms_in pTm);	//s
extern ms_u64 
	mstime_api_us(ms_void);
extern ms_u64 
	mstime_api_ms(ms_void);
extern ms_void 
	mstime_api_usdebug1(ms_u64 ms_in basetime);
extern ms_void 
	mstime_api_usdebug2(ms_void);
extern ms_u64 
	mstime_api_sec(ms_void);
extern ms_bool 
	mstime_api_setsec(char *datetime_string);
extern ms_u32 
	mstime_api_minute(ms_void);
extern ms_u32 
	mstime_api_hour(ms_void);

/**
 Description  : get current time(inclde date)
 Return Value : 
 	the string of current time,format(year_mon_day_hour_min_sec)
**/
extern ms_string mstime_api_string_curdate(ms_string ms_in frm);
extern ms_string mstime_api_string_curym(ms_string ms_in frm);
extern ms_s08 	    mstime_api_curmouth(ms_void);
/**
 Description  : get current time(inclde date)
 Return Value : 
 	the string of current time,format(year_mon_day_hour_min_sec)
**/
extern ms_string mstime_api_string_curtime(ms_string ms_in frm);

/**
 Description  : get current time(inclde date)
 Return Value : 
 	the string of current time,format(year_mon_day_hour_min_sec)
**/
extern ms_string mstime_api_string_curdatetime(ms_string ms_in frm);
extern ms_void    mstime_api_string_ustime(ms_u64 * ptimeus,ms_string ms_in frm,char * strtime);
extern ms_void    mstime_api_string_time(ms_u64 * ptimesec,ms_string ms_in frm,char * strtime);
extern ms_string mstime_api_string_rstime(ms_string ms_in fmt,RSTime ms_in rstime,ms_string outstr);
extern ms_string mstime_api_string_timesec_out(ms_u32 ms_in timesec,ms_string outbuf);
extern ms_string mstime_api_string_timeus_out(ms_s64 ms_in timesec,ms_string outbuf);
extern ms_bool mstime_api_isnewday(time_t * ms_io timep_pre)	;
extern ms_bool mstime_api_isnewloop(ms_string ms_in pstr,ms_s32 timeout,ms_bool *flag_cur,ms_bool *flag_pre);	//s
extern ms_bool mstime_api_iswholeclock(time_t *ms_io timep_pre);
extern ms_bool  mstime_api_isvailddate( RSTime ms_in rstime_now,ms_string ms_in start_date,ms_string ms_in end_date);
extern ms_bool mstime_api_isvaildtime( RSTime ms_in rstime_now,ms_string ms_in start_time,ms_string ms_in end_time);
extern ms_bool mstime_api_isvaildwday(RSTime ms_in rstime_now, ms_s32 ms_in vaildwday);
extern ms_bool mstime_api_ispass(PRSTime ms_in pTm);


extern ms_void mstime_api_curdatetime(RSTime * ms_out prstime);
extern ms_u32 mstime_api_timenodate(RSTime * ms_out prstime);
/**
 Description  : get current time(inclde date)
 Input
 	unsigned int sec 	time,use to delay
 Return Value : 
 	none
**/
extern ms_void mstime_api_counter_sync(ms_u32 ms_in sec);
extern ms_bool mstime_api_counter_async(time_t * ms_in Etimep,ms_u32 ms_in sec);	//s
extern ms_bool mstime_api_counter_async_us(ms_u64 * ms_in Etimep,ms_u64 ms_in time_us);	//us

extern ms_s08 mstime_api_compare(PRSTime ms_in pTm1,PRSTime ms_in pTm2);
extern ms_bool mstime_api_timeout(PRSTime ms_in ptm_start,PRSTime ms_in ptm_stop,ms_u64 timeou_sec);	//s
/**
 Description  : Compare current time with the input PRSTime
 Input
	unsigned char *pucDate		the string to be parsed,format(B20150603123025E20150603123025)
	PRSTime pStartTm 			the time of starting dumping
	PRSTime pStopTm				the time of stopping dumping
	char * outStr				format trans 20150603123025-20150603123025
 Return Value : 
 	none
**/
extern ms_void mstime_api_parsetimestr1(ms_string ms_in pstr, PRSTime ms_out ptm_start, 
	PRSTime ms_out ptm_stop,ms_ps08 ms_out outbuf);
//2015-06-03 12:30:25
extern ms_void mstime_api_parsetimestr2(ms_string ms_in pstr, PRSTime ms_out prstime);	
#endif
#endif
