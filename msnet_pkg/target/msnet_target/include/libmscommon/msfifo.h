#ifndef MSFIFO_H
#define MSFIFO_H
#include <stdint.h>
#include <libmslog/mstype.h>
#include "msthread.h"
typedef struct MSFIFOBLOCKContext {
	ms_pu08 ptr_begain;
	ms_pu08 ptr_end;
	ms_pu08 ptr_in;
	ms_pu08 ptr_out;
	ms_u32 num_in;
	ms_u32 num_out;
/* BEGAIN: Added by su<sugao_cn@163.com>, 2021/01/03 Memory optimization   */
	ms_u08 name[128];
/* END: Added by su<sugao_cn@163.com>, 2021/01/03 Memory optimization   */
	ms_u32 size_total;
	MSLOCKContext mslock;		//protect  fifolist data(pmsfifo_ctt),in func hserver_dorecv and hserver_dorecvfifo
	//fifo_state:user set
	ms_u32 size_used;
	ms_u32 size_left;
	ms_u32 num_flowoverreset;
} MSFIFOBLOCKContext;

typedef struct MSFIFOLISTContext {
	ms_u32 total_num;
	ms_u32 len;
	ms_pvoid ptr;
	struct MSFIFOLISTContext *next;
} MSFIFOLISTContext;

typedef struct MSFIFOSIZEContext{
	ms_u32 fifosize_left;
	ms_u32 fifosize_total;
	ms_u32 num_reset;
	ms_u32 num_flowoverreset;
	MSFIFOBLOCKContext * pmsfifo_ctt;
}MSFIFOSIZEContext;

#ifndef MSFIFO_C
#define INTERFACE_MSFIFO extern
#else
#define INTERFACE_MSFIFO
#endif
//FIFO BLOCK
INTERFACE_MSFIFO MSFIFOBLOCKContext *
	msfifoblock_api_init(ms_u32 ms_in size,ms_string ms_in name);
INTERFACE_MSFIFO ms_void 
	msfifoblock_api_deinit(MSFIFOBLOCKContext **  ms_io ppmsfifoblock_ctt);
/*|--data(len byte)--|*/
INTERFACE_MSFIFO ms_s32 
	msfifoblock_api_in(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt, ms_pvoid ms_in pbuf, ms_u32 ms_in size,ms_s32  ms_in (*func)(ms_pvoid, ms_pvoid, ms_u32));
INTERFACE_MSFIFO ms_s32 
	msfifoblock_api_in_sizeleft(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt, ms_pvoid ms_in pbuf, 
		ms_u32 ms_in size,ms_s32  ms_in (*func)(ms_pvoid, ms_pvoid, ms_u32),ms_u32 size_left);
INTERFACE_MSFIFO ms_s32 
	msfifoblock_api_out(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt, ms_pvoid ms_in pbuf, ms_u32 ms_in size,ms_s32  ms_in (*func)(ms_pvoid, ms_pvoid, ms_u32));
/*|--len(4 byte)--|--data(len byte)--|*/
INTERFACE_MSFIFO ms_s32 
	msfifoblock_api_lenin(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt, ms_pvoid ms_in pbuf, ms_s32 ms_in size);
INTERFACE_MSFIFO ms_s32 
	msfifoblock_api_lenin_sizeleft(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt, ms_pvoid ms_in pbuf, ms_s32 ms_in size,ms_u32 sizeleft);
INTERFACE_MSFIFO ms_s32 
	msfifoblock_api_lenout_funcline(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt, ms_pvoid ms_in pbuf, ms_u32 ms_in size,ms_cstring ms_in func, ms_u32 ms_in line);
INTERFACE_MSFIFO ms_u32 
	msfifoblock_api_sizeused(MSFIFOBLOCKContext * ms_in pmsfifoblock_ctt);
INTERFACE_MSFIFO  ms_u32 
	msfifoblock_api_sizeleft(MSFIFOBLOCKContext * ms_in pmsfifoblock_ctt);
INTERFACE_MSFIFO  ms_u32 
	msfifoblock_api_numreset(MSFIFOBLOCKContext * ms_in pmsfifoblock_ctt);
INTERFACE_MSFIFO ms_void 
	msfifoblock_api_flowoverreset(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt);
INTERFACE_MSFIFO ms_u32 
	msfifoblock_api_flowoverreset_num(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt);
INTERFACE_MSFIFO ms_void 
	msfifoblock_api_reset(MSFIFOBLOCKContext * ms_io pmsfifoblock_ctt);

//FIFO LIST
INTERFACE_MSFIFO MSFIFOLISTContext *msfifolist_api_malloc(ms_u32 ms_in size,ms_string name);
INTERFACE_MSFIFO ms_void msfifolist_api_demalloc(MSFIFOLISTContext ** ms_io ppmsfifolist_ctt);
INTERFACE_MSFIFO ms_void msfifolist_api_in(MSFIFOLISTContext ** ms_io ppheader_fifolist_ctt,MSFIFOLISTContext * ms_in pmsfifolist_ctt);
INTERFACE_MSFIFO MSFIFOLISTContext *msfifolist_api_out(MSFIFOLISTContext ** ms_in ppheader_fifolist_ctt);
INTERFACE_MSFIFO ms_u32 msfifolist_api_total(MSFIFOLISTContext ** ms_in ppheader_fifolist_ctt);
INTERFACE_MSFIFO ms_void msfifolist_api_reset(MSFIFOLISTContext ** ms_in ppheader_fifolist_ctt);

#define msfifoblock_api_lenout(pmsfifoblock_ctt, pbuf,size)	msfifoblock_api_lenout_funcline(pmsfifoblock_ctt, pbuf, size,__FUNCTION__,__LINE__);
#define msfifoblock_api_lenout_2048(pmsfifoblock_ctt, pbuf)	msfifoblock_api_lenout(pmsfifoblock_ctt, pbuf, 2048);



#endif /* FIFO_H */

