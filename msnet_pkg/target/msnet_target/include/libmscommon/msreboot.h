#ifndef MSREBOOT_H
#define MSREBOOT_H
#include <libmslog/mstype.h>
#ifndef MSREBOOT_C
extern ms_void ms_api_reboot(ms_void);
extern ms_void ms_api_forcereboot(ms_void);
extern ms_void ms_api_poweroff(ms_void);
#endif
#endif
