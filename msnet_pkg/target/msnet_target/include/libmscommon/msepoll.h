#ifndef MSEPOLL_H
#define MSEPOLL_H


#include <sys/types.h>
#include <sys/epoll.h> 

typedef  struct epoll_event	msepoll_event;
typedef  struct epoll_event*	pmsepoll_event;
//event
#define msepollin			EPOLLIN
#define msepollpri			EPOLLPRI
#define msepollout			EPOLLOUT
#define msepollrdband		EPOLLRDBAND
#define msepollwband		EPOLLWRNORM
#define msepollmsg		EPOLLMSG
#define msepollerr			EPOLLERR
#define msepollhup		EPOLLHUP
#define msepollrdhup		EPOLLRDHUP
#define msepollexclusive	EPOLLEXCLUSIVE
#define msepollwakeup		EPOLLWAKEUP
#define msepolloneshot		EPOLLONESHOT
#define msepollet			EPOLLET
#define msepollio			(EPOLLIN|EPOLLOUT)

#ifndef MSEPOLL_C
extern ms_s32 msepoll_open(ms_u32 ms_in ev_size);
extern ms_s32 msepoll_add(ms_s32 ms_in epfd,ms_s32 ms_in ev_fd,ms_u32 ms_in opt);
extern ms_s32 msepoll_mod(ms_s32 ms_in epfd,ms_s32 ms_in ev_fd,ms_u32 ms_in opt);
extern ms_s32 msepoll_del(ms_s32 ms_in epfd,ms_s32 ms_in ev_fd,ms_u32 ms_in opt);
extern ms_s32 msepoll_wait(ms_s32 ms_in epfd, pmsepoll_event ms_io events, ms_u32 ms_in maxevents);
extern ms_s32 msepoll_wait_timeout(ms_s32 ms_in epfd, pmsepoll_event ms_io  events, ms_u32 ms_in maxevents,ms_u32 ms_in timeout_ms);
extern ms_s32 msepoll_close(ms_s32 ms_in *epfd);
extern ms_s32 msepoll_waitfd_timeout(ms_s32 ms_in epfd,ms_s32 ms_in ev_fd, ms_bool ms_in is_out,ms_u32 ms_in timeout_ms);
extern ms_s32 msepoll_waitfd_timeout_noopt(ms_s32 ms_in epfd,ms_s32 ms_in ev_fd, ms_bool ms_in is_out,ms_u32 ms_in timeout_ms);
extern ms_s32  msepoll_appapi_init( ms_u32 num,void *callback_args ,ms_u32 ms_in opt,
	ms_s32 (*callback_func)(void *callback_args,ms_u32 index));
extern ms_void msepoll_appapi_deinit( ms_s32 epfd,ms_u32 num,void *callback_args,
	ms_void (*callback_func)(void *callback_args,ms_u32 index));
ms_void msepoll_appapi_dataread( ms_s32 epfd,ms_u32 num ,void *callback_args,
	ms_void (*incallback_func)(void *callback_args,ms_s32 fd),
	ms_void (*outcallback_func)(void *callback_args,ms_s32 fd));
#endif

	
#endif

