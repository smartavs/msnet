#ifndef MSENV_H
#define MSENV_H
#include <libmslog/mstype.h>
#ifndef MSENV_C
extern ms_string msenv_api_get(ms_cstring ms_in name,ms_cstring ms_in default_strenv);
extern ms_bool msenv_api_set(ms_cstring ms_in name,ms_cstring ms_in value,ms_bool ms_in overwrite);
extern ms_bool msenv_api_unset(ms_cstring ms_in name);
extern ms_bool msenv_api_put(ms_cstring ms_in name_val);
#endif

#endif
