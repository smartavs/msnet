#ifndef MSSIGNAL_H
#define MSSIGNAL_H
#include <signal.h>
#include <libmslog/mstype.h>

#define mswait_ifexited(s) 	WIFEXITED(s)
#define mswait_exitstatus(s)	WEXITSTATUS(s)
#define mswait_ifsignaled(s) WIFSIGNALED(s)
#define mswait_termsig(s) 	WTERMSIG(s)
#define mswait_ifstopped(s) 	WIFSTOPPED(s)
#define mswait_stopsig(s) 	WSTOPSIG(s)

#ifndef MSSIGNAL_C
extern ms_void mssignal_api_catch(ms_void);
extern ms_void mssignal_api_watchdog(ms_s32 (*childfunc)(ms_pvoid ),ms_pvoid ms_in arg);
extern ms_void mssignal_api_startdaemon(ms_string ms_in logdir);
#endif
#endif
