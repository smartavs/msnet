#ifndef MSSYSCTL_H
#define MSSYSCTL_H
#include <libmslog/mstype.h>

/*****************************************************
* :System file
*****************************************************/
#ifndef MSSYSCTL_C
extern ms_void mssysctl_api_disable_rp_filter(ms_void);
extern ms_void mssysctl_api_enable_rp_filter(ms_void);
extern ms_s32  mssysctl_api_igmp_getversion(ms_void);
extern ms_void mssysctl_api_igmp_setversion(ms_u08 igmp_version);
#endif
#endif
