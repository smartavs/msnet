#ifndef MSSTRING_H
#define MSSTRING_H
#include <stddef.h>
#include <libmslog/mstype.h>
#ifndef MSSTRING_C
extern size_t msstr_api_lcpy(ms_string ms_in dst, ms_string ms_in src, size_t ms_in size);
extern size_t msstr_api_lcat(ms_string ms_in dst, ms_string ms_in src, size_t ms_in size);
extern size_t msstr_api_lcatf(ms_string ms_in dst, size_t ms_in size, ms_cstring ms_in fmt, ...);
extern ms_bool msstr_api_isspace(ms_s08 ms_in c);
extern ms_void msstr_api_getline(ms_ps08 ms_in buf, ms_s32 ms_in buf_size, ms_pcstring ms_io pp);
extern ms_void msstr_api_getline_withend(ms_ps08 ms_in buf, ms_s32 ms_in buf_size, ms_pcstring ms_io pp);
extern ms_void msstr_api_getword(ms_ps08 ms_in buf, ms_s32 ms_in buf_size, ms_pcstring ms_io pp);
extern ms_string msstr_api_casestr(ms_cstring ms_in strsrc, ms_cstring ms_in strtarget);
extern ms_bool msstr_api_iscasestr(ms_cstring ms_in strsrc, ms_cstring ms_in strtarget);
extern ms_u32 msstr_api_split(ms_string ms_in str_token,ms_cstring ms_in str_delimit,ms_string ms_out str_out[]);
extern ms_u32 msstr_api_split2(ms_string ms_in str_token,ms_cstring ms_in str_delimit,ms_string ms_out str_out[]);
extern ms_void msstr_api_urlsplit(ms_string ms_out proto, ms_s32 ms_in proto_size,
                  ms_string ms_out authorization, ms_s32 ms_in authorization_size,
                  ms_string ms_out hostname, ms_s32 ms_in hostname_size,
                  ms_ps32 port_ptr,
                  ms_string ms_out path, ms_s32 ms_in path_size,
                  ms_bool *pflag_ipv6,
                  ms_cstring  ms_in url);
extern ms_string msstr_api_listurlsbuild(ms_string ms_in proto,ms_string ms_in authorization,
	ms_string ms_in hostname,ms_s32 ms_in port,ms_string ms_in path,ms_s32 listnum,ms_s32 arrsize,ms_byte *plisturl);
extern ms_void  msstr_api_listurlsget(ms_string ms_in proto,ms_string ms_in authorization,
	ms_string ms_in hostname,ms_s32 ms_in port,ms_string ms_in path,ms_s32 listnum,ms_byte *plisturl);
extern ms_bool msstr_api_findinfotag(char *arg, int arg_size, const char *tag1, const char *info);
extern ms_s08 msstr_api_urlcheck(ms_cstring  ms_in url);
extern ms_bool msstr_api_isurl(ms_cstring  ms_in url);
extern ms_string msstr_api_findread(ms_string  inbuf,ms_string target,ms_string readfmt,ms_u32 *preadval);
extern ms_string 
	msstr_api_replace(ms_cstring instr, ms_cstring src_substr, ms_cstring dst_substr, ms_string outstr);	
#endif

#define  msstr_api_urlsplit_autosize(proto,authorization,hostname,port_ptr,path,pflag_ipv6,url); \
                    msstr_api_urlsplit(proto, (ms_bufsize(proto)), \
			authorization, (ms_bufsize(authorization)), \
                    	hostname, (ms_bufsize(hostname)), \
			port_ptr,  \
			path, (ms_bufsize(path)),  \
			pflag_ipv6,  \
			url);
#endif

