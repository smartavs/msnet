#ifndef MSMEM_H
#define MSMEM_H
#include <libmslog/mstype.h>
#ifndef MSMEM_C
extern ms_pvoid msmem_api_malloc(ms_u32 ms_in size,ms_string name, ms_string ms_in filename, ms_u32 ms_in line)  ;  
extern ms_pvoid msmem_api_calloc(ms_u32 ms_in size,ms_string name, ms_string ms_in filename, ms_u32 ms_in line);  
extern ms_void msmem_api_free(ms_pvoid ms_io *pptr)  ;  
extern ms_void msmem_api_showblock(ms_void);  
extern ms_void msmem_api_info(char *outbuf,ms_string disable_des);
 extern ms_void msmem_api_init(ms_bool enable_debug);  
 extern ms_void msmem_api_deinit(ms_void);
  extern ms_void msmem_api_test(ms_void);
#endif

#endif

