#ifndef MSSI_H
#define MSSI_H

#include <libmslog/mslog.h>
#include "mscommon.h"
/****************************************************************************
 * Macro definitions
 ***************************************************************************/
/*PID*/
#define SI_PID_PAT		(0x0000)
#define SI_PID_CAT		(0x0001)
#define SI_PID_NIT			(0x0010)

#define SI_PIDUSE_MIN		(0x0010)
#define SI_PID_SDT		(0x0011)
#define SI_PID_BAT		(0x0011)
#define SI_PID_EIT			(0x0012)
#define SI_PID_TDT		(0x0014)
#define SI_PID_TOT		(0x0014)
#define SI_PIDUSE_MAX	(0x1FFE)

#define SI_PID_NULL		(0x1FFF)
#define LEN_PAT_HEADER		8
#define LEN_PAT_PROGRAMDES	4		//bit16-prgramindex   bit3-res   bit13-prgramid  

#define LEN_SDT_HEADER		11
#define LEN_SDT_HEADER_SERVICE_DES_LOOP		5

#define LEN_PMT_HEADER		12

#define LEN_CRC				4
/*Table ID*/
#define SI_TID_PAT			(0x00)
#define SI_TID_CAT			(0x01)
#define SI_TID_PMT			(0x02)
#define SI_TID_NIT_ACT		(0x40)
#define SI_TID_NIT_OTH		(0X41)
#define SI_TID_SDT_ACT		(0x42)
#define SI_TID_SDT_OTH		(0x46)
#define SI_TID_BAT			(0x4A)
#define SI_TID_EIT_PF_ACT		(0x4E)
#define SI_TID_EIT_PF_OTH		(0x4F)
#define SI_TID_EIT_SCHE_ACT	(0x50) /* 0x50 - 0x5f*/
#define SI_TID_EIT_SCHE_OTH	(0x60) /* 0x60 - 0x6f */
#define SI_TID_TDT			(0x70)
#define SI_TID_TOT			(0x73)

/*Sync Byte*/
#define TS_VALUE_SYNC	0x47

#define TS_PACKET_SIZE_FEC 	204
#define TS_PACKET_SIZE_DVHS 	192
#define TS_PACKET_SIZE 		188
#define TS_PACKET_SIZE_MAX 	204

#define PES_PACKET_SIZE 	184

#define NB_PID_MAX 		8192
#define MAX_SECTION_SIZE 4096

#define DEFAULT_PID_PMT 						0x0142
#define DEFAULT_SERVICEID 					0X0001
#define DEFAULT_TRANSPORT_STREAM_ID		0X0001
#define DEFAULT_ORIGINAL_NETWORK_ID 		0XFF01

/*****************************************************
*	Ts header value
******************************************************/
/*Byte[0]: sync*/
#define TS_GET_SYNC(buf) 						(buf[0])
/*Byte[1:2]: 
	bit[15]:transport error indicator
	bit[14]:payload unit start indicator
	bit[13]:transport priority
	bit[0:12]:PID
*/
#define TS_GET_TRAN_ERR_IND(buf) 				((buf[1]&0x80)>>7)
#define TS_GET_PAYLOAD_UNIT_START_IND(buf) 	((buf[1]&0x40)>>6)
#define TS_TRAN_PRIORITY(buf) 					((buf[1]&0x20)>>5)
#define TS_GET_PID(buf) 						(((((buf)[1]&0x1f)<<8)|(((buf)[2]&0xff)))&0xffff)	
/*Byte[3]: 
	bit[6:7]:transport scrambling control
	bit[4:5]:adaption field control
	bit[0:3]:continuity counter,0~15
*/
#define TS_GET_TRAN_SCRAMBLING_CONTROL(buf) 	((buf[3]&0xc0)>>6)	
#define TS_GET_ADAPTION_FIELD_CONTROL(buf) 		((buf[3]&0x30)>>4)	
#define TS_GET_CONTINUITY_COUNTER(buf) 			(buf[3]&0x0f)	
/*****************************************************
* :System clock
*****************************************************/
#define MS_SYSTIME_BASE			(1000*1000)		// 1Mhz		us
#define MS_DEVICETIME_BASE		(27*1000*1000)	// 27Mhz
#define MS_DEVICETIME_EXA		(90*1000)			// 27Mhz


enum PMT_STREAMType{
	streamtype_res				=0x00,
	streamtype_video_mpeg1		=0x01,
	streamtype_video_mpeg2		=0x02,
	streamtype_audio_mpeg1		=0x03,	
	streamtype_audio_mpeg2		=0x04,	
	streamtype_private_sections	=0x05,
	streamtype_private_data		=0x06,	
	streamtype_audio_aac			=0x0f,
	streamtype_video_mpeg4		=0x10,
	streamtype_audio_aac_latm		=0x11,	
	streamtype_metadata			=0x15,
	streamtype_video_h264			=0x1b,	
	streamtype_video_hevc			=0x24,	
	streamtype_video_cavs			=0x42,
	streamtype_video_vc1			=0xea,
	streamtype_video_dirac		=0xd1,
	streamtype_audio_ac3			=0x81,
	streamtype_audio_dts			=0x82,
	streamtype_audio_truehd		=0x83,
	streamtype_audio_eac3			=0x87,
	streamtype_max,
};

typedef struct TSHEADInfo{
	ms_u08   syncbyte;
	ms_u08   transporterror_indicator;
	ms_u08   payload_unit_start_ind;
	ms_u08   transport_priority;
	ms_u16   pid;
	ms_u08   transport_scrambling_control;
	ms_u08   adaptation_field_ctr;
	ms_u08   continuity_counter;
}TSHEADInfo;
typedef struct CTUCOUNTERInfo{
	ms_u16 pid;
	ms_u08 last_cc;
	ms_u32 errnum;
	ms_u32 lostnum;

	ms_bool flag_nofirstpkg;
}CTUCOUNTERInfo;	
static inline ms_void mssi_api_print_tsheader(TSHEADInfo * ms_in ptshead_info,ms_byte ms_in dugmode)
{
	if(1==dugmode){
		ms_ldebug("mssi","Sync byte:%#x",ptshead_info->syncbyte);
		ms_ldebug("mssi","transport error indicator:%#x",ptshead_info->transporterror_indicator);
		ms_ldebug("mssi","payload unit start indicator:%#x", ptshead_info->payload_unit_start_ind);
		ms_ldebug("mssi","transport priority:%#x",ptshead_info->transport_priority);
		ms_ldebug("mssi","PID:%#x",ptshead_info->pid);
		ms_ldebug("mssi","transport scrambling control:%#x",ptshead_info->transport_scrambling_control);
		ms_ldebug("mssi","adaption field control:%#x",ptshead_info->adaptation_field_ctr);// DBG_PCR
		ms_ldebug("mssi","continuity counter:%#x",ptshead_info->continuity_counter);	
		ms_ldebug("mssi","\r\n");
	}else if(2==dugmode){
		ms_ldebug("mssi","PID,continuity counter:%#x,%#x",ptshead_info->pid,ptshead_info->continuity_counter);	
	}
}
static inline ms_s08 mssi_api_checkcc(ms_u16 pid,CTUCOUNTERInfo *pstream_ctucounter_info,unsigned char cur_cc)
{
	if(pid!=pstream_ctucounter_info->pid){
		ms_lwaring("mssi","-------------%#x,%#x",pid,pstream_ctucounter_info->pid);
		return -2;
	}

	ms_ldebug("mssi","[%#x]:Continuity  got %d",pid,cur_cc);
	ms_s08 ret=0;
	if(!(pstream_ctucounter_info->last_cc<0)	){
		ms_u08 expected_cc = (pstream_ctucounter_info->last_cc + 1) & 0x0f;
		if(cur_cc!=expected_cc){
			pstream_ctucounter_info->errnum++;
			pstream_ctucounter_info->lostnum+=(cur_cc > expected_cc) ? (cur_cc-expected_cc) : (15+cur_cc-expected_cc);
			ms_lerror("mssi","[%#x]:Continuity check failed expected %d got %d,errnum,lostnum:%d %d",pid,
				expected_cc,cur_cc,
				pstream_ctucounter_info->errnum,
				pstream_ctucounter_info->lostnum);
			ret=-1;
		}
	}
	pstream_ctucounter_info->last_cc=cur_cc;
	return ret;
}
static inline ms_s08 mssi_api_get_tsheader(TSHEADInfo * ms_out ptshead_info,ms_pu08  ms_in ptspkt,ms_u32 len,ms_bool dbg_header)
{
	if(ms_null==ptshead_info
		&&ms_null==ptspkt){
		return -1;
	}
	if (TS_VALUE_SYNC!=TS_GET_SYNC(ptspkt )	){
		ms_lwaring("mssi","It is not a ts buf,because the syn_byte is %d ,not %d",TS_GET_SYNC(ptspkt ),TS_VALUE_SYNC);
		return -1;
	}	
	if (1==TS_GET_TRAN_ERR_IND(ptspkt )	){
		ms_lwaring("mssi","There is maybe an error in the pkt,but i ignor it");
		ms_buf("ptspkt-error-ind", ptspkt, 16);
		dbg_header=ms_true;
	}	
	
	ptshead_info->pid=TS_GET_PID(ptspkt );
	if (0x00!=TS_GET_TRAN_SCRAMBLING_CONTROL(ptspkt )	){
		ms_lwaring("mssi","The sceambling control is  %d, maybe the pes need to do,but I donot suport it",
			TS_GET_TRAN_SCRAMBLING_CONTROL(ptspkt ));
		ms_buf("ptspkt- sceambling control ", ptspkt, 16);
		dbg_header=ms_true;
	}	
	ptshead_info->syncbyte=TS_GET_SYNC(ptspkt );
	ptshead_info->transporterror_indicator=TS_GET_TRAN_ERR_IND(ptspkt );
	ptshead_info->payload_unit_start_ind=TS_GET_PAYLOAD_UNIT_START_IND(ptspkt);	
	ptshead_info->transport_priority=TS_TRAN_PRIORITY(ptspkt);	
	ptshead_info->pid=TS_GET_PID(ptspkt );
	ptshead_info->transport_scrambling_control=TS_GET_TRAN_SCRAMBLING_CONTROL(ptspkt );
	ptshead_info->adaptation_field_ctr=TS_GET_ADAPTION_FIELD_CONTROL(ptspkt);
	ptshead_info->continuity_counter=TS_GET_CONTINUITY_COUNTER(ptspkt );
	mssi_api_print_tsheader(ptshead_info,dbg_header);
	return 0;
}

static inline ms_s08 mssi_api_build_tsheader(ms_u16 pid,ms_byte adaption_field_control,ms_byte*ptspkt)
{
	ptspkt[0]=TS_VALUE_SYNC;
/*Byte[1:2]: 
	bit[15]:transport error indicator
	bit[14]:payload unit start indicator
	bit[13]:transport priority
	bit[0:12]:PID
*/
	ptspkt[1]=(0<<7)&0x80;
	ptspkt[1]|=(0x01<<6)&0x40;
	ptspkt[1]|=(0<<5)&0x20;
	ptspkt[1]|=(pid>>8)&0x1f;
	ptspkt[2]=pid&0xff;
/*Byte[3]: 
	bit[6:7]:transport scrambling control
	bit[4:5]:adaption field control
	bit[0:3]:continuity counter,0~15
*/
	ptspkt[3]=(0	<<6)&0xc0;
	ptspkt[3]|=(adaption_field_control	<<4)&0x30;
	ptspkt[3]|=0x00;
}

static inline ms_s08 mssi_api_build_cc(ms_byte cc,ms_byte*ptspkt)
	
{
/*Byte[3]: 
	bit[6:7]:transport scrambling control
	bit[4:5]:adaption field control
	bit[0:3]:continuity counter,0~15
*/
	ptspkt[3]=(0	<<6)&0xc0;
	ptspkt[3]|=(0x01<<4)&0x30;
	ptspkt[3]|=cc&0x0F;
}


static inline ms_bool mssi_api_istspkt(unsigned char *buf, unsigned int len)
{
	ms_u32 index=0;
	while( len> (index*TS_PACKET_SIZE)){
		if(TS_VALUE_SYNC!=TS_GET_SYNC((&buf[index*TS_PACKET_SIZE]))	){
			return ms_false;
		}
		index++;
	}
	return ms_true;
}


static inline ms_u08 mssi_api_getadaptation_field_len(ms_u08   adaptation_field_ctr,ms_byte * pes_buf)
{
	ms_u08 adaptation_field_len=0;
	switch(adaptation_field_ctr){
		case 0x0:		//res
			adaptation_field_len=PES_PACKET_SIZE;
			break;
		case 0x1:		//no adaptation_field_len,payload is 184
			adaptation_field_len=0;
			break;	
		case 0x2:		//no payload,adaptation_field_len must be 183;
			adaptation_field_len=(PES_PACKET_SIZE-1);
			break;
		case 0x3:		//the adaptation_field_len is rang of(0~182);
			adaptation_field_len=(ms_u08 )(pes_buf[0]&0xFF);
			if(ms_rangeout(adaptation_field_len, 0, 182)){
				adaptation_field_len=(PES_PACKET_SIZE-1);
			}else{
				adaptation_field_len+=1;
			}
			break;
	}
	return adaptation_field_len;
}
static inline ms_byte * mssi_api_setadaptation_field_len(ms_u08   adaption_field_control,ms_u16 section_len,ms_byte **ppes_buf)
{
	switch(adaption_field_control){
		case 0x0:		//res
			return ms_null;
		case 0x1:		//no adaptation_field_len,payload is 184
			return &(*ppes_buf)[4];
			break;	
		case 0x2:		//no payload,adaptation_field_len must be 183;
			(*ppes_buf)[4]=0x00;
			return ms_null;
		case 0x3:		//the adaptation_field_len is rang of(0~182);
			(*ppes_buf)[4]=PES_PACKET_SIZE-1-section_len-LEN_CRC;
			(*ppes_buf)[5]=0x00;
			return &(*ppes_buf)[4+PES_PACKET_SIZE-section_len-LEN_CRC];
			break;
	}
}
static inline ms_bool  mssi_api_isvalid_pid(ms_u32 pid)
{
	if(ms_range(pid, SI_PIDUSE_MIN, SI_PIDUSE_MAX)){
		return ms_true;
	}else{
		return ms_false;
	}
}
static inline ms_bool  mssi_api_isffmpeg_validpid(ms_u32 pid)
{
	if(SI_PID_SDT==pid){
			ms_lwaring("mssi","FFMPEG will send sdt,so %#x res for sdt",SI_PID_SDT);
	}
	if((ms_true==mssi_api_isvalid_pid( pid))&&(SI_PID_SDT!=pid)){
		return ms_true;
	}else{
		return ms_false;
	}
}
#endif

