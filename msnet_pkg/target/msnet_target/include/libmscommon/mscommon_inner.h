#ifndef MSCOMMON_INNER_H
#define MSCOMMON_INNER_H
#include <libmslog/mslog.h>
#include <libmslog/mstype.h>
#define DLL_SYMBOL	__declspec(dllexport)
#define NETWORK_8BYTE(buf,val) \
	(buf)[0]=(val&0xff00000000000000)>>56;\
	(buf)[1]=(val&0x00ff000000000000)>>48;\
	(buf)[2]=(val&0x0000ff0000000000)>>40;\
	(buf)[3]=(val&0x000000ff00000000)>>32; \
	(buf)[4]=(val&0xff000000)>>24;\
	(buf)[5]=(val&0x00ff0000)>>16;\
	(buf)[6]=(val&0x0000ff00)>>8;\
	(buf)[7]=(val&0x000000ff);	
#define NETWORK_4BYTE(buf,val) \
	(buf)[0]=(val&0xff000000)>>24;\
	(buf)[1]=(val&0x00ff0000)>>16;\
	(buf)[2]=(val&0x0000ff00)>>8;\
	(buf)[3]=(val&0x000000ff);	
#define NETWORK_3BYTE(buf,val) \
	(buf)[0]=(val&0x00ff0000)>>16;\
	(buf)[1]=(val&0x0000ff00)>>8;\
	(buf)[2]=(val&0x000000ff);	
#define NETWORK_2BYTE(buf,val) \
	(buf)[0]=(val&0xff00)>>8; \
	(buf)[1]=(val&0xff);

#define NETWORK_4BYTE_LF(buf,val) \
	(buf)[3]=(val&0xff000000)>>24;\
	(buf)[2]=(val&0x00ff0000)>>16;\
	(buf)[1]=(val&0x0000ff00)>>8;\
	(buf)[0]=(val&0x000000ff);	
#define NETWORK_2BYTE_LF(buf,val) \
	(buf)[1]=(val&0xff00)>>8; \
	(buf)[0]=(val&0xff);


#define HOST_8BYTE(buf)	(\
	((((uint64_t)((buf)[0]))&0xff)<<56) \
	|((((uint64_t)((buf)[1]))&0xff)<<48) \
	|((((uint64_t)((buf)[2]))&0xff)<<40) \
	|((((uint64_t)((buf)[3]))&0xff)<<32) \
	|((((uint64_t)((buf)[4]))&0xff)<<24) \
	|((((uint64_t)((buf)[5]))&0xff)<<16) \
	|((((uint64_t)((buf)[6]))&0xff)<<8) \
	|(((uint64_t)((buf)[7]))&0xff)	)
#define HOST_4BYTE(buf)	((((buf)[0]&0xff)<<24)|(((buf)[1]&0xff)<<16)|(((buf)[2]&0xff)<<8)|((buf)[3]&0xff))
#define HOST_3BYTE(buf)	((((buf)[0]&0xff)<<16)|(((buf)[1]&0xff)<<8)|((buf)[2]&0xff))
#define HOST_2BYTE(buf)	(((((buf)[0]&0xff)<<8)|(((buf)[1]&0xff)))&0xffff)

#define SYS_CMDLINE(cmdbuf,fmt,arg...)	\
	memset(cmdbuf,0,sizeof(cmdbuf)); \
	snprintf(cmdbuf,sizeof(cmdbuf),fmt,##arg); \
	system(cmdbuf);

#define SYS_CMDLINE_SYNC(cmdbuf,fmt,arg...)	\
	memset(cmdbuf,0,sizeof(cmdbuf)); \
	snprintf(cmdbuf,sizeof(cmdbuf),fmt,##arg); \
	system(cmdbuf);\
	system("sync");

#define SYS_CMDCP(cmdbuf,fm,to) \
	SYS_CMDLINE(cmdbuf,"cp %s %s  -fr",fm,to);

#define SYS_CMDCP_BIN(cmdbuf,fm,to) \
	SYS_CMDLINE(cmdbuf,"cp %s %s -fr && chmod 777 %s",fm,to,to); 

#define CMD_STRCAT(tempbuf,outbuf,fmt,arg...)	\
	memset(tempbuf,0,strlen(tempbuf)); \
	sprintf(tempbuf,fmt,##arg); \
	strcat(outbuf,tempbuf);

#define CMD_SPRINTF(outbuf,fmt,arg...)	\
	memset(outbuf,0,strlen(outbuf)); \
	sprintf(outbuf,fmt,##arg); 

#define CMD_SNPRINTF(outbuf,len,fmt,arg...)	\
	memset(outbuf,0,len); \
	snprintf(outbuf,len,fmt,##arg); 

#define  CMD_STRCPY(str1,str2) \
	memset(str1, 0,sizeof(str1)); \
	if((8!=sizeof(str1))&&(4!=sizeof(str1))&& (sizeof(str1) < (strlen(str2)+1)) ){  \
		ms_waring("There is a bug that buf1 (%lu < %lu)(%s),please fix!!",sizeof(str1) , (strlen(str2)+1),str2); \
		ms_memcpy(str1, str2, sizeof(str1)); \
	} else{strcpy(str1,str2);}
	
#ifndef AV_WL32
#   define AV_WL32(p, darg) do { \
        unsigned int d = (darg); \
        ((unsigned char*)(p))[0] = (d); \
        ((unsigned char*)(p))[1] = (d)>>8; \
        ((unsigned char*)(p))[2] = (d)>>16; \
        ((unsigned char*)(p))[3] = (d)>>24; \
    } while(0)
#endif

#ifndef AV_RL32
#   define AV_RL32(x)	\
    (((unsigned int)((const unsigned char*)(x))[3] << 24) |	\
               (((const unsigned char*)(x))[2] << 16) |	\
               (((const unsigned char*)(x))[1] <<  8) |	\
                ((const unsigned char*)(x))[0])
#endif

#include "msmem.h"
#define MS_MALLOC(size,name)	msmem_api_malloc (size,name, __FILE__,__LINE__);	//
#define MS_CALLOC(size,name)	msmem_api_calloc (size,name, __FILE__,__LINE__);	//malloc(size)
#define MS_FREE(item)			msmem_api_free((void **)&item);	
#define MS_FREEP(item)		msmem_api_free((void **)&item);item=NULL;
#define MS_SHOWBLOCK()		msmem_api_showblock();  

#define MALLOC_VAL_RET_NODBG(item,size,name,ret) \
	item	=MS_MALLOC(size,name); \
	if(NULL==item){ \
		return ret; \
	}
	
#define MALLOC_VAL_RET(item,size,name,ret) \
	item	=MS_MALLOC(size,name); \
	if(NULL==item){ \
		ms_errret(ret,"Malloc %s failed,please check the memory",name); \
	}
	
#define MALLOC_VAL_NORET(item,size,name) \
	item	=MS_MALLOC(size,name); \
	if(NULL==item){ \
		ms_errnoret("Malloc %s failed,please check the memory",name); \
	}
#define MALLOC_VAL_GOTO(goval,item,size,name) \
	item	=MS_MALLOC(size,name); \
	if(NULL==item){ \
		ms_errgoto(goval,"Malloc %s failed,please check the memory",name); \
	}
#define MALLOC_VAL_RETGOTO(goval,ret,item,size,name) \
	item	=MS_MALLOC(size,name); \
	if(NULL==item){ \
		ret=-1; \
		ms_errgoto(goval,"Malloc %s failed,please check the memory",name); \
	}


#define CALLOC_VAL_RET_NODBG(item,size,name,ret) \
	item	=MS_CALLOC(size,name); \
	if(NULL==item){ \
		return ret; \
	}
	
#define CALLOC_VAL_RET(item,size,name,ret) \
	item	=MS_CALLOC(size,name); \
	if(NULL==item){ \
		ms_errret(ret,"Malloc %s failed,please check the memory",name); \
	}
	
#define CALLOC_VAL_NORET(item,size,name) \
	item	=MS_CALLOC(size,name); \
	if(NULL==item){ \
		ms_errnoret("Malloc %s failed,please check the memory",name); \
	}
#define CALLOC_VAL_GOTO(goval,item,size,name) \
	item	=MS_CALLOC(size,name); \
	if(NULL==item){ \
		ms_errgoto(goval,"Malloc %s failed,please check the memory",name); \
	}
#define CALLOC_VAL_RETGOTO(goval,ret,item,size,name) \
	item	=MS_CALLOC(size,name); \
	if(NULL==item){ \
		ret=-1; \
		ms_errgoto(goval,"Malloc %s failed,please check the memory",name); \
	}
	
#define DEMALLOC_VAL(item)  if(NULL!=item){MS_FREE(item);item=NULL;}

#define SET_REG_BIT(REG, BitMask, EnOrDis)\
	if ( EnOrDis ){\
		(REG) |= (BitMask) ;\
	}else{\
		(REG) &= ~( BitMask) ;\
	}
#define GET_REG_BIT(REG, BitMask)		(  (REG) & (BitMask)  )

#define SET_BIT(REG, BIT, EnOrDis)\
	if ( EnOrDis ){\
		(REG) |= (1<<BIT) ;\
	}else{\
		(REG) &= ~(1<<BIT) ;\
	}
#define GET_BIT(VALUE, BIT)	((VALUE&(1<<BIT))>>BIT)

#define SET_DATA_VALUE(Data,Value, ValueMask)		( (Data) = ( (Value)&(ValueMask) ) )
#define GET_DATA_VALUE(Data,ValueMask)			( (Data)&(ValueMask) )

#define GET_REG_STATUS(REG, VALUE)  ( REG&VALUE	)
#define SET_REG_STATUS(REG, VALUE, REGBitopr) \
	if ( REGBitopr == REG_BIT_SET ){\
		REG |= VALUE ;\
	}else if ( REGBitopr == REG_BIT_CLEAR ){	\
		REG &= ~(VALUE);	\
	}else{\
		LOG_DBG( mslog_level_error, FLAG , "SET_REG_STATUS Param error!");\
	}
	
#define MS_SDD(num) 	(num)--
#define MS_UDD(num) 	if((num)>0){(num)--;}else{(num)=0;}

//common infos
#define EMAIL_LIST(prefix) \
	prefix"- MSAVSKIT(Email:msavskit@163.com)"
#define SERVER_AUTHOR			"MSAVSKIT"
#define SERVER_AUTHOR_CONTACT	"MSAVSKIT(Email:msavskit@163.com)"
#define SERVER_AUTHOR_CONTACT_EMAIL	"msavskit@163.com"
#define REVISION_FFMPEG				"4.2.2"
#define REVISION_LAME				"3.100.1"
#define REVISION_LIBXML2				"2.9.9"
#define REVISION_NVCODECHEADER		"9.1.23"
#define REVISION_RTMPDUMP			"2.4"
#define REVISION_X264				"0.157.2969"
#define REVISION_OPENSSL				"3.0.0"

#if defined(OS_ANDROID)||defined(OS_LINUX_SOC)
#include <unistd.h>			//mkdir()		access()
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/prctl.h>
#include <sys/types.h>  
#include <sys/wait.h> 
#include <stdlib.h>
#include <stdio.h>

#define mst_mkdir(dir)	mkdir(dir,0777);
#define mst_open		open
#define mst_read		read
#define mst_write		write
#define mst_lseek		lseek
#define mst_close		close
#define mst_access		access
#define mst_snprint		snprintf
#define mst_rmdir(rm_cmd,dirname) 		snprintf(rm_cmd,sizeof(rm_cmd),"rm   -fr   %s",dirname); system(rm_cmd);
#define mst_sleep(sencod)	sleep(sencod)
#define mst_msleep(us)		usleep(us*1000)
#define mst_usleep(us)		usleep(us)
#elif defined(OS_WIN32)	
#define mst_open			_open
#define mst_read			_read
#define mst_write			_write
#define mst_lseek			_lseek
#define mst_close			_close
#define mst_access			_access
#define mst_snprint			_snprintf
#define mst_mkdir(dir)		_mkdir(dir);
#define mst_rmdir(rm_cmd,dirname) 		snprintf(rm_cmd,sizeof(rm_cmd),dirname); system(rm_cmd);
#define mst_sleep(ms)		Sleep(ms*1000)
#define mst_msleep(ms)		Sleep(ms)
#endif
extern ms_s32   mst_innerapi_read_cmdinfo( ms_string ms_in cmd,ms_string ms_in info ,ms_u32 ms_in maxlen);    
extern ms_s32   mst_innerapi_read_cmdinfo2( ms_string ms_in cmd,ms_string ms_in info ,ms_u32 ms_in maxlen);
extern ms_bool mst_innerapi_isgrep_cmdinfo( ms_string ms_in cmd ) ; 
extern ms_void mst_innerapi_kill( ms_string ms_in cmd ) ; 
extern ms_bool mst_innerapi_cmdline(ms_string ms_in cmd) ; 
extern ms_s08 mst_innerapi_dataswitch(ms_pbyte ms_in buf1,ms_pbyte ms_in buf2 ,ms_u32 ms_in len)   ;
extern ms_s08 mst_innerapi_selfcp(ms_pbyte ms_io buf,ms_s32 ms_in offset_byte,ms_u32 ms_in len) ; 
extern ms_u32 mst_innerapi_encrypt(char* data, ms_u32 len,char *key,char keylen,char *outbuf,char is_enc) ;
extern ms_void mst_innerapi_encrypt_outstring(ms_string  ms_out outstr,ms_string ms_in pbasekey,ms_string prefmt,ms_string aftfmt);
extern ms_bool mst_innerapi_decrypt(char *instr,ms_string ms_in pbasekey,ms_bool flag_mustdecrypt);
extern ms_void mst_innerapi_gethlsprogramname(ms_string ms_in hls_url,ms_string ms_in vodpath,ms_string ms_out programname);
extern ms_bool mst_innerapi_gethlspath(ms_string hls_url,ms_string pvodpath,ms_string hls_path);
extern ms_string mst_innerapi_bit2str_64( ms_string ms_out outstr, ms_u64 byte,ms_bool falg_bit);
extern ms_string mst_innerapi_bit2str_32( ms_string ms_out outstr, ms_u32 byte,ms_bool falg_bit);
extern ms_bool mst_innerapi_datatype( ms_s08 *ptypebf, ms_s08 *ptypeaf,ms_byte * pdatatype_times,
	ms_string  name_des,ms_string name_typebf,ms_string name_typeaf);
#endif

