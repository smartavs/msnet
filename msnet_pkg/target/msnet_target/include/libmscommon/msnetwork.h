#ifndef MSNETWORK_H
#define MSNETWORK_H
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#if defined(OS_ANDROID)||defined(OS_LINUX_SOC)
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/sockios.h>
#endif
#include <libmslog/mstype.h>
#include "mscommon.h"


typedef unsigned long nfds_t;
struct pollfd {
    ms_s32 fd;
    ms_s16 events;  /* events to look for */
    ms_s16 revents; /* events that occurred */
};

/* events & revents */
#define POLLIN     0x0001  /* any readable data available */
#define POLLOUT    0x0002  /* file descriptor is writeable */
#define POLLRDNORM POLLIN
#define POLLWRNORM POLLOUT
#define POLLRDBAND 0x0008  /* priority readable data */
#define POLLWRBAND 0x0010  /* priority data can be written */
#define POLLPRI    0x0020  /* high priority readable data */

/* revents only */
#define POLLERR    0x0004  /* errors pending */
#define POLLHUP    0x0080  /* disconnected */
#define POLLNVAL   0x1000  /* invalid file descriptor */

#define poll msnet_api_poll

#define MIN_PORT	1024   /* minimum port allowed */
#define MAX_PORT 65535  /* maximum port allowed */

#define msnet_maxlen_ifname	64
#define msnet_maxlen_mac		18	//2c:53:4a:01:11:2a
#define msnet_maxlen_mtu		16
#define msnet_maxlen_ip		16
#define msnet_maxlen_mask		16
#define msnet_maxlen_gw		16

#define msnet_checktime_ipping		30


typedef enum {
	msnet_tcp_unkonw = 0,
	msnet_tcp_established = 1,
	msnet_tcp_synsend,
	msnet_tcp_synrecv,
	msnet_tcp_finwait1,
	msnet_tcp_finwait2,
	msnet_tcp_timewait,
	msnet_tcp_close,
	msnet_tcp_closewait,
	msnet_tcp_lastack,
	msnet_tcp_listen,
	msnet_tcp_closing,	/* Now a valid state */
	msnet_tcp_maxstates	/* Leave at the end! */
}MSNETTCPState;
typedef enum NETCFG_MODE{
	netcfg_mode_unknow=0x00,
	netcfg_mode_static,
	netcfg_mode_dhcp,
	netcfg_mode_bondslaver,
	netcfg_mode_bondmaster,
	netcfg_mode_max,
}NETCFG_MODE;
#define ethnmode_string \
	 "unkonw", \
	 "static", \
	 "dhcp", \
	 "bond-slaver", \
	 "bond-master", \
	 "max"
	 
typedef struct NETCFGInfo{
	ms_u08   name[msnet_maxlen_ifname];
	ms_u08   address[msnet_maxlen_mac];
	ms_u08   ipaddress[msnet_maxlen_ip];
	ms_u08   netmask[msnet_maxlen_mask];
	ms_u08   netmaskbit[64];
	ms_u08   gateway[msnet_maxlen_gw];
	ms_u08   usrgateway[msnet_maxlen_gw];
	ms_u08   mtu[msnet_maxlen_mtu];
	ms_u08   bondmaster_name[msnet_maxlen_mtu];
	ms_u08   dns1[128];
	ms_u08   dns2[128];
	ms_u08   mode[128];
	ms_bool flag_defaultgw;
	
	ms_bool use_pppoe;
	ms_bool flag_restart;
	ms_bool flag_wifi;
}NETCFGInfo;

typedef struct MSIPlist{
	ms_u08   totalnum;
	ms_u08   ipaddr[128][32];	
	ms_bool  flag_ipv6[32];	
}MSIPlist;

typedef struct MSIPPINGContext{
	ms_u08 num;
	ms_u08 list[64][32];
	ms_bool flag_reachable[64];
	ms_bool flag_multicast[64];
	ms_bool info[64][32];
	ms_bool cmd_stop;
}MSIPPINGContext;

#ifndef MSNETWORK_C
	#define MSNET_INTERFACE extern
#else
	#define MSNET_INTERFACE
#endif
MSNET_INTERFACE ms_s08 msnet_api_init(ms_void);
MSNET_INTERFACE ms_void msnet_api_deinit(ms_void);
MSNET_INTERFACE ms_bool msnet_api_isethernetconnected(ms_string ms_in paddr,ms_s32 port);	//"www.baidu.com"  
MSNET_INTERFACE ms_bool msnet_api_islinkup( ms_string ms_in pifname ) ;  
MSNET_INTERFACE ms_s32 msnet_api_socketnonblock(ms_s32 ms_io socket, ms_bool ms_in enable);
MSNET_INTERFACE ms_s08 msnet_api_socketconnect(ms_s32 ms_io  socket, struct sockaddr * ms_in plisten_addr,ms_u32 ms_in len,ms_u64 ms_in timeout);
MSNET_INTERFACE ms_s32 msnet_api_poll(struct pollfd * ms_in fds, nfds_t ms_in numfds, ms_u32 ms_in timeout);
MSNET_INTERFACE ms_s32 msnet_api_waitfd(ms_s32 ms_in fd, ms_bool ms_in is_write);
MSNET_INTERFACE ms_s32 msnet_api_waitfd_timeout(ms_s32 ms_in fd, ms_s32 ms_in is_write,ms_s32 ms_in retrytime);
MSNET_INTERFACE ms_s08 msnet_api_netcfg_getinfo(NETCFGInfo * ms_out pnetcfg_info) ;  
MSNET_INTERFACE ms_s08 msifm_api_netcfg_setinfo(NETCFGInfo *  ms_in pnetcfg_info) ;   
MSNET_INTERFACE ms_bool 
	msnet_api_ismulticast( ms_string ms_in paddr);
MSNET_INTERFACE ms_string 
	msnet_api_getipaddr(struct sockaddr *addr);
MSNET_INTERFACE ms_bool 
	msnet_api_isresaddr( ms_string ms_in pipaddr);
MSNET_INTERFACE ms_s08 
	msnet_api_dns_getip(ms_string ms_in phostname, ms_string ms_in pservice,MSIPlist *pmsiplist);
MSNET_INTERFACE ms_bool 
	msnet_api_isIpv6(ms_string ms_in ipaddr);
MSNET_INTERFACE ms_void 
	msnet_api_ipout_Ipv6(ms_pbyte ms_in ipaddr);
MSNET_INTERFACE ms_s08 msnet_api_iface_getip(const ms_string ms_in pifname, ms_string ms_out pipaddr);
MSNET_INTERFACE ms_s08 msnet_api_iface_getipandmask(const ms_string ms_in pifname, ms_string ms_out pipaddr, ms_string ms_out pnetmask);
MSNET_INTERFACE ms_s08 msnet_api_iface_getmac(ms_string ms_in pifname,ms_string ms_out pmacstr,ms_u32 ms_in size)  ;
MSNET_INTERFACE ms_s08 msnet_api_iface_getmac_byte(ms_string ms_in pifname, ms_pbyte ms_out pmac);
MSNET_INTERFACE ms_s08 msifm_api_iface_getmtu(ms_string ms_in pifname,ms_string ms_in mtubuf,ms_u32 ms_in size)  ;
MSNET_INTERFACE ms_s08 msnet_api_iface_setmtu(ms_string ms_in pifname,ms_u32 ms_in mtuval)  ;
MSNET_INTERFACE ms_s08 msnet_api_iface_setflags( ms_s32 ms_in fd  ,ms_string ms_in pifname, ms_u32 ms_in flags) ;
MSNET_INTERFACE ms_void msnet_api_mac_num2str(ms_pbyte pbuf,ms_u32 len,ms_pbyte pstrout);
MSNET_INTERFACE ms_s08 msnet_api_mac_stringtobin( ms_string ms_in pmacstr, ms_string ms_out poutbuf);
MSNET_INTERFACE ms_bool msnet_api_mac_vaildcheck( ms_string ms_in pmacstr);
MSNET_INTERFACE ms_u32 msnet_api_ip_string2num( ms_string ms_in pipstr);
MSNET_INTERFACE ms_void msnet_api_ip_num2string( ms_u32 ms_in ipaddr_num, ms_string ms_out poutbuf);
MSNET_INTERFACE ms_u16 msnet_api_checksum(ms_pu16 ms_in paddr,ms_u32 ms_in bytes);
MSNET_INTERFACE ms_u16 msnet_api_checksum_ptc(ms_pbyte ms_in  pether_pkt,ms_u32  source_ip,ms_u32  dest_ip,ms_u08  protocol,ms_u08  header_len,ms_pbyte data,ms_u16 data_len);	
MSNET_INTERFACE ms_void msnet_api_tcpstate_getstring( MSNETTCPState ms_in msnet_tcpstate,ms_string ms_out outstr) ;
MSNET_INTERFACE ms_s08 msnet_api_binddevice(ms_s32 ms_in fd , ms_string ms_in pifname) ;
MSNET_INTERFACE ms_s32 msnet_api_epollrecv(ms_s32 ms_in fd ,  ms_pu08 ms_in  pbuf, ms_u32 ms_in  size,ms_u32 ms_in  timeout);
MSNET_INTERFACE ms_s32 msnet_api_epollsend(ms_s32 ms_in fd ,  ms_pu08 ms_in  pbuf, ms_u32 ms_in  size,ms_u32 ms_in  timeout);
MSNET_INTERFACE ms_s32 msnet_api_epollsend2(ms_s32 ms_in fd ,  ms_pu08 ms_in  pbuf, ms_u32 ms_in  size,ms_u32 ms_in  timeout) ;
MSNET_INTERFACE ms_bool msnet_api_netmaskbit(ms_string ms_in pipaddr,ms_string ms_in pnetmask,ms_string ms_out netmask_out,ms_string ms_out usr_gateway);
MSNET_INTERFACE ms_bool msnet_api_subnet(ms_string ms_in pipaddr,ms_string ms_in pnetmask,ms_string ms_out netmask_out);
MSNET_INTERFACE ms_s32 msnet_api_getipaddrs(ms_string ms_in pipaddr_start,ms_string ms_in pipaddr_end,ms_string ms_out pmipaddrs,ms_u08 arrsize);
MSNET_INTERFACE ms_bool msnet_api_isvaildfd(ms_s32 fd);
MSNET_INTERFACE NETCFG_MODE msnet_api_getmode(ms_u08* ms_in mode_string);
MSNET_INTERFACE ms_string msnet_api_nettostr(ms_pbyte pbuf);
MSNET_INTERFACE ms_void msnet_api_numtostr(ms_pbyte pbuf,ms_u32 len,ms_pbyte pstrout);
MSNET_INTERFACE ms_void msnet_api_strtonum(ms_pbyte pstrin,ms_u32 len,ms_pbyte pbuf);
MSNET_INTERFACE ms_void msnet_api_numto16str(ms_pbyte pbuf,ms_u32 len,ms_pbyte pstrout);
MSNET_INTERFACE ms_bool msnet_api_ping( ms_string ipaddr);

#define msnet_nettostr(pbyte)	msnet_api_nettostr(pbyte)
#define msnet_strtonet(str)		inet_addr(str)
#define msnet_ntohs(pbyte)		ntohs(pbyte) 
#define msnet_htons(pbyte)		htons(pbyte) 
#define msnet_ntohl(pbyte)		ntohl(pbyte) 
#define msnet_htonl(pbyte)		htonl(pbyte) 
#define msnet_numtostr(pbuf,len,pstrout)		msnet_api_numtostr(pbuf,len,pstrout)
#define msnet_numto16str(pbuf,len,pstrout)	msnet_api_numto16str(pbuf,len,pstrout)
#define msnet_numtoMacstr(pbuf,len,pstrout)	msnet_api_mac_num2str(pbuf,len,pstrout)
#define msnet_strtonum(pstrin,len,pbuf)		msnet_api_strtonum(pstrin,len,pbuf)
#endif

