#ifndef MSMD_H
#define MSMD_H
#include <libmslog/mstype.h>
typedef enum{
	memclear_none		=0x00,
	memclear_pagecache,
	memclear_dentries_inodes,
	memclear_all,
}MEMCLEARLevel;


typedef enum {
	decencmode_sw		=0x00,
	decencmode_nvidia	,
	decencmode_intel		
}DECENCMode;
#define msstring_decencmode	"sw","nvidia","intel"

#define mscfg_maxnum_cpu 		256
#define mscfg_maxphysicsnum_cpu 	4
#define mscfg_maxnum_partiton		64
#define msnet_maxnum_iface		32
#define msnet_maxnum_diskdev		8
#define maxlen_dmidecode_info		2048
#define maxlen_blkserial_info		512
#define maxlen_blkid_info			128
#define maxlen_systemuuid			128
#define maxlen_processorid			512

#ifndef MSMD_C
extern ms_bool 
	msmd_api_avfilter(ms_string ms_in filename);
extern ms_void 
	msmd_api_rangenum16(ms_u16 *pval,ms_u16 min,ms_u16 max );
extern ms_u32 
	msmd_api_random(ms_u32 min,ms_u32 max );
extern ms_s32 
	msmd_api_read(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size);
extern ms_s32 
	msmd_api_write(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size,ms_string stropt);
extern ms_s32 
	msmd_api_read2(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size);
extern ms_s32 
	msmd_api_read3(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size);
ms_void 
	msmd_api_rm_bysize(ms_string ms_in filename,ms_u32 ms_in size);
ms_s32 
	msmd_api_writea_and_rm_bysize(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size,ms_u32 ms_in maxsize);
extern ms_s32 
	msmd_api_write2(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size);
extern ms_s32 
	msmd_api_writea(ms_string ms_in filename,ms_pu08 ms_in buf,ms_u32 ms_in size);
extern ms_s32 
	msmd_api_getcpunum(ms_void);
extern ms_s08 
	msmd_api_changeuser(ms_string ms_in username);
extern ms_void 
	msmd_api_memclear( MEMCLEARLevel ms_in);
extern ms_void 
	msmd_api_syslogclear( ms_void);
extern ms_void 
	msmd_api_applogclear( ms_void);
extern ms_void 
	msmd_api_dumpclear( ms_void);
extern ms_void 
	msmd_api_kernelclear( ms_void);
extern ms_bool 
	msmd_api_checksoftware(ms_string name);
ms_bool 
	msmd_api_checksoftware_bybin(ms_string binname);
extern DECENCMode  
	msmd_api_decencmode(ms_void)  ;
extern ms_void 
	msmd_api_graphics(char *pinfo) ; 
extern ms_s08 
	msmd_api_setulimit(ms_void);
extern ms_void 
	msmd_api_setcpufreq(ms_void);
extern ms_void 
	msmd_api_pstree(ms_string pos_version,ms_u32 pid,ms_string strout);
extern ms_bool 
	msmd_api_filecheckexist(ms_string filename);
#endif
#endif

