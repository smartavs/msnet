#ifndef MSTYPES_H
#define MSTYPES_H
#include <inttypes.h>
#if defined(OS_LINUX_X64)
	/* bsd */
	typedef unsigned char		u_char;
	typedef unsigned short		u_short;
	typedef unsigned int		u_int;
	typedef unsigned long		u_long;

	/* sysv */
	typedef unsigned char		unchar;
	typedef unsigned short		ushort;
	typedef unsigned int		uint;
	typedef unsigned long		ulong;
	typedef unsigned char		u_int8_t;
	typedef unsigned short		u_int16_t;
	typedef unsigned int		u_int32_t;
	typedef signed char		__s8;
	typedef short				__s16;
	typedef int				__s32;
	typedef long long			__s64;
	typedef unsigned char		__u8;
	typedef unsigned short		__u16;
	typedef unsigned int		__u32;
	typedef unsigned long long	__u64;	

	typedef		__s8		int8_t;
	typedef		__s16		int16_t;
	typedef		__s32		int32_t;	
#endif

typedef		char			int08;
typedef		char			int8;
typedef		short		int16;
typedef		int			int32;
typedef		long long		int64;

typedef		unsigned char		uint08;
typedef		unsigned char		uint8;
typedef		unsigned short	uint16;
typedef		unsigned int		uint32;
typedef		unsigned long long	uint64;

typedef		uint8		uint8_t;
typedef		uint16		uint16_t;
typedef		uint32		uint32_t;

typedef		float  		float32_t;
#if defined(OS_WIN32)||defined(OS_LINUX_X32)
	typedef		unsigned long long		u_int64_t;
	typedef		long long				int64_t;
	typedef		unsigned long long		uint64_t;
#endif


typedef	uint8 *	string;
typedef	uint8	byte;
typedef	uint8 	array;
#define new_array8(nay,size)	uint8 nay[size]={0};
#define new_array16(nay,size)	uint16 nay[size]={0};
#define new_array32(nay,size)	uint32 nay[size]={0};
#define new_string(nay)			string nay=NULL;
#define new_byte(nay)			string nay=NULL;
#endif
