#ifndef MSLOG_H
#define MSLOG_H
#include "mslog_inner.h"

/*****************************************************************************
 Description  : 
*****************************************************************************/
INTERFACE_LOG ms_void 
	mslog_api_new(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_setopt(ms_u32 ms_in logopt,ms_string ms_in dir,ms_u32 maxlen);
INTERFACE_LOG ms_void 
	mslog_api_getopt_string(ms_u32 ms_in logopt,ms_string ms_in outstr);
INTERFACE_LOG ms_string 
	mslog_api_getlogfile(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_unsetopt(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_infofunc(ms_string ms_out pbuf);
INTERFACE_LOG ms_string 
	mslog_api_version(ms_void);
/*****************************************************************************
 Description  : mslog without use-flags api
*****************************************************************************/
#define ms_fatal(fmt, arg...) 				ms_lfatal(        FLAG , fmt, ##arg)
#define ms_error(fmt, arg...) 				ms_lerror(       FLAG , fmt, ##arg)
#define ms_waring(fmt, arg...) 			ms_lwaring(    FLAG , fmt, ##arg)
#define ms_info(fmt, arg...) 				ms_linfo(        FLAG , fmt, ##arg)
#define ms_debug(fmt, arg...) 			ms_ldebug(     FLAG , fmt, ##arg)
#define ms_verbose(fmt, arg...) 			ms_lverbose(  FLAG , fmt, ##arg)
#define ms_more(fmt, arg...) 				ms_lmore(  FLAG , fmt, ##arg)
#define ms_fatal1(fmt, arg...) 			ms_lfatal1(        FLAG , fmt, ##arg)
#define ms_error1(fmt, arg...) 			ms_lerror1(       FLAG , fmt, ##arg)
#define ms_waring1(fmt, arg...) 			ms_lwaring1(    FLAG , fmt, ##arg)
#define ms_info1(fmt, arg...) 				ms_linfo1(        FLAG , fmt, ##arg)
#define ms_debug1(fmt, arg...) 			ms_ldebug1(     FLAG , fmt, ##arg)
#define ms_verbose1(fmt, arg...) 			ms_lverbose1(  FLAG , fmt, ##arg)
#define ms_more1(fmt, arg...) 			ms_lmore1(  FLAG , fmt, ##arg)
#define ms_print(fmt, arg...)				PRINT(fmt, arg...)	

#define ms_fatal2(fmt, arg...) 		LOG_DBG2( mslog_level_assert 	, fmt, ##arg)
#define ms_error2( fmt, arg...) 		LOG_DBG2( mslog_level_error	 	, fmt, ##arg)
#define ms_waring2(fmt, arg...) 		LOG_DBG2( mslog_level_warn	 	, fmt, ##arg)
#define ms_info2(fmt, arg...) 			LOG_DBG2( mslog_level_info	 	, fmt, ##arg)
#define ms_debug2( fmt, arg...) 		LOG_DBG2( mslog_level_debug 	, fmt, ##arg)
#define ms_verbose2(fmt, arg...) 		LOG_DBG2( mslog_level_verbose 	, fmt, ##arg)
#define ms_more2(fmt, arg...) 		LOG_DBG2( mslog_level_more 	, fmt, ##arg)
/*****************************************************************************
 Description  : mslog with return
*****************************************************************************/
#define ms_warnoret(frm,arg...)		ms_waring(frm,##arg);  return;
#define ms_dbgret(ret,frm,arg...)		ms_debug(frm,##arg);  return ret;
#define ms_errret(ret,frm,arg...)		ms_error(frm,##arg);  return ret;
#define ms_errnoret(frm,arg...)		ms_error(frm,##arg);  return;

/*****************************************************************************
 Description  : mslog with goto
*****************************************************************************/
#define ms_dbggoto(gval,frm,arg...)	ms_debug(frm,##arg);  goto gval;
#define ms_infogoto(gval,frm,arg...)	ms_info(frm,##arg);  goto gval;
#define ms_wargoto(gval,frm,arg...)	ms_waring(frm,##arg);  goto gval;
#define ms_errgoto(gval,frm,arg...)	ms_error(frm,##arg);  goto gval;
#define ms_errretgoto(gval,ret,frm,arg...)	ms_error(frm,##arg);  ret=-1;goto gval;

/*****************************************************************************
 Description  : mslog with exit
*****************************************************************************/
#define ms_errexit(ret,frm,arg...)		ms_error( frm,##arg);  exit(ret);

/*****************************************************************************
 Description  : func enter,leave and line,use to debug 
*****************************************************************************/
#define ms_funcenter				ms_debug(">>>>>>>>>>Enter %s ",__FUNCTION__);
#define ms_funcleave				ms_debug("<<<<<<<<<<Leave %s ",__FUNCTION__);
#define ms_funcline				ms_debug("[%s]=========%d ",__FUNCTION__,__LINE__);

/*****************************************************************************
 Description  : func nosupport
*****************************************************************************/
#define ms_funcnosupport(arg)		ms_waring("No support %s",arg);
#define ms_funcnosupports		 	ms_waring("No support %s",__FUNCTION__);
/*****************************************************************************
 Description  : func unauthen
*****************************************************************************/
#define ms_funcunauthent(arg)		ms_waring("No authent %s",arg);
#define ms_funcunauthents		 	ms_waring("No authent %s",__FUNCTION__);
/*****************************************************************************
 Description  : func fix
*****************************************************************************/
#define ms_fix(frm,arg...)			ms_waring("[%s]" frm ",please fix me","su.gao",##arg);
/*****************************************************************************
 Description  : func deprecated
*****************************************************************************/
#define ms_deprecated(oldfunc_name,newfunc_name) 	ms_waring("%s is deprecated, uses %s to instead" ,oldfunc_name,newfunc_name);

/*****************************************************************************
 Description  : log 64 bit
*****************************************************************************/
#define ms_num2str(num) 						mslog_innerapi_num2str((char[32]){0}, num)
#define ms_bitbyte64(num_h,num_l) 				mslog_innerapi_bitbyte64((char[32]){0},num_h, num_l,ms_null)			
#define ms_bitbyte32(num_h,num_l) 				mslog_innerapi_bitbyte32((char[32]){0},num_h, num_l,ms_null)	
#define ms_bitbyte64_unit(num_h,num_l,unit) 		mslog_innerapi_bitbyte64((char[32]){0},num_h, num_l,unit)			
#define ms_bitbyte32_unit(num_h,num_l,unit) 		mslog_innerapi_bitbyte32((char[32]){0},num_h, num_l,unit)	
/*****************************************************************************
 Description  : buf 
*****************************************************************************/
/*---buf log---*/
#define ms_buf(description, buf, len)					mslog_innerapi_buf(description, buf, len,__FUNCTION__,__LINE__)
#define mslog_bufandascii(description, buf, len)			mslog_innerapi_bufandascii(description, buf, len,__FUNCTION__,__LINE__)
#define ms_errbuf(description, buf, len)				mslog_innerapi_errbuf(description, buf, len,__FUNCTION__,__LINE__)
#define ms_bufascii(description, buf, len)				mslog_innerapi_bufascii(description, buf, len,__FUNCTION__,__LINE__)
/*****************************************************************************
 Description  : log with en 
*****************************************************************************/
#define ms_enerr(enable,fmt, arg...)				if(enable)ms_error( fmt, ##arg);
#define ms_endbg(enable,fmt, arg...)				if(enable)ms_debug( fmt, ##arg);
#define ms_eninfo(enable,fmt, arg...)				if(enable)ms_info( fmt, ##arg);
#define ms_enverbose(enable,fmt, arg...)			if(enable)ms_verbose( fmt, ##arg);
#define ms_enhlight(enable,fmt, arg...)				if(enable)ms_info( fmt, ##arg);
#define ms_enbuf(enable,description, buf,len)		if(enable)ms_buf(description, buf,len);
#define ms_enbufascii(enable,description, buf,len)		if(enable)ms_bufascii(description, buf,len);
/*****************************************************************************
 Description  : buf  check 
*****************************************************************************/
#define ms_bufchecknoret(dbuf)				bufcheck_noret(dbuf)
#define ms_bufcheck(dbuf)					bufcheck(dbuf)
#define ms_bufcheck_des(buf,fmt,arg...) 			bufcheck_des(buf,fmt,##arg) 
#define ms_bufcheckret(ret,dbuf)				bufcheck_ret(ret,dbuf)
#define ms_bufcheckret_des(ret,buf,fmt,arg...) 		bufcheckret_des(ret,buf,fmt,##arg)
#define ms_bufcheckgoto(gval,dbuf)			bufcheck_goto(gval,dbuf)
#define ms_bufcheckgoto_des(gval,buf,fmt,arg...) 	bufcheck_goto_des(gval,buf,fmt,##arg) 
/*****************************************************************************
 Description  : param  check 
*****************************************************************************/
#define ms_pamnocheck(dbuf)	
#define ms_pamcheck(buf,strname) 			bufcheck_des(buf,"Param error####%s",strname)
#define ms_pamcheckret(ret,buf,strname)		bufcheckret_des(ret,buf,"Param error####%s",strname)
#define ms_pamcheckgoto(gval,buf,strname)	bufcheck_goto_des(gval,buf,"Param error####%s",strname)
#define ms_pamnocheck(dbuf)

/*****************************************************************************
 Description  : mslog  options 
*****************************************************************************/
#define mslog_enable_stdprint	(1<<4)
#define mslog_enable_linefunc	(1<<5)
#define mslog_enable_filelog	(1<<6)
#define mslog_enable_timeus	(1<<7)
#endif

