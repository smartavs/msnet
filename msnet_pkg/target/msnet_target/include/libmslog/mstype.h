#ifndef MSTYPE_H
#define MSTYPE_H

#include <inttypes.h>
typedef  long long  			int64;

//base data type
typedef 	int64_t			ms_s64;
typedef 	int				ms_s32;
typedef 	short			ms_s16;
typedef 	char				ms_s08;
typedef	void 				ms_void;
typedef 	float				ms_float;
typedef    double			ms_double;
typedef 	uint64_t 			ms_u64;
typedef 	unsigned int		ms_u32;
typedef 	unsigned short		ms_u16;
typedef 	unsigned char		ms_u08;
typedef 	long unsigned int 	ms_lu64;
typedef 	ms_s64 *			ms_ps64;
typedef 	ms_s32 *			ms_ps32;
typedef 	ms_s16 *			ms_ps16;
typedef	void 	* 			ms_pvoid;
typedef 	ms_s08 *			ms_ps08;
typedef 	ms_u64 *			ms_pu64;
typedef 	ms_u32 *			ms_pu32;
typedef 	ms_u16 *			ms_pu16;
typedef 	ms_u08 *			ms_pu08;


//data type
typedef	ms_s08			ms_byte;
typedef	ms_u08 			ms_array;
typedef	ms_pu08 			ms_pbyte;
typedef	ms_pu08 			ms_parray;

//bool
typedef    ms_s08			ms_bool;
typedef    ms_pu08			ms_pbool;
#define 	ms_true			1
#define 	ms_false			0
#define 	ms_restart		2

//string type
typedef	ms_s08 *			ms_string;
typedef	ms_string *		ms_pstring;
typedef	const char *		ms_cstring;
typedef	ms_cstring *		ms_pcstring;	

//function and typedef param or val uses
#define ms_in	
#define ms_out	
#define ms_io	
#define ms_inner	



//max num
#define ms_f64		0xffffffffffffffff
#define ms_f32		0xffffffff
#define ms_f16		0xffff
#define ms_f08		0xff
#define ms_sf(ms_fnum)	((ms_fnum>>1)-1)	//max num of  signed ms_fnum

/*common*/
#define ms_null	NULL

#endif

