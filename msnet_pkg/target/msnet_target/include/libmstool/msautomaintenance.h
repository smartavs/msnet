#ifndef MSAUTO_MAINTENANCE_H
#define MSAUTO_MAINTENANCE_H

#include <libmscommon/mstime.h>
#ifndef MSAUTO_MAINTENANCE_C
extern void msauto_api_maintenanceinit(RSTime *,char auto_maintenance[32]);
extern void msauto_api_maintenance(RSTime rstime_auto_maintenance,ms_void (* uesrtask)( ms_void));
#endif
#endif