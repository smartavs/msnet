#ifndef MSIPROUTE_H
#define MSIPROUTE_H
#ifndef MSIPROUTE_C
extern ms_void msiproute_api_webinfo(ms_string ms_out poutbuf,ms_string ms_in info,ms_string ms_in mlist_info,ms_string ms_in waitinit_info);
extern ms_bool msiproute_api_init(ms_bool enable);
extern ms_void msiproute_api_deinit(ms_void);
#endif
#endif

