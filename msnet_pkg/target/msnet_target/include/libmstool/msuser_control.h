#ifndef MSUSER_CONTROL_H
#define MSUSER_CONTROL_H
#ifndef MSUSER_CONTROL_C
	ms_s08   msmsusrctl_api_init(ms_void);
	ms_bool msmsusrctl_api_isblack(ms_string ipaddr);
	ms_bool msmsusrctl_api_iswhite(ms_string ipaddr);
	ms_void msmsusrctl_api_webinfo(ms_string ms_out outbuf);
#endif
#endif

