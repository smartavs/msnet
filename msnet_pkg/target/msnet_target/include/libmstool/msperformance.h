#ifndef MSPERFORMANCE_H
#define MSPERFORMANCE_H
#include <sys/resource.h>
#include <libmscommon/msnetwork.h>
#include <libmscommon/msmd.h>
#define MS_U64 unsigned long long
typedef struct{	//KByte
	ms_u32 total;
	ms_u32 free;
	ms_u32 shared;
	ms_u32 buff;
	ms_u32 cache;
	ms_u32 available;
	ms_u32 swaptotal;
	ms_u32 swapfree;
}MEMInfo;

#define MAX_NUM_CPU			128
typedef struct{
	ms_byte vgainfo[128];
	ms_byte num_sata;
	ms_byte satainfo[msnet_maxnum_diskdev][128];
	ms_byte satasizeinfo[msnet_maxnum_diskdev][128];
	ms_byte num_SCSIstorage;
	ms_byte SCSIstorageinfo[msnet_maxnum_diskdev][128];
	ms_byte meminfo[128];
	ms_byte audioinfo[128];
	ms_byte num_ethnet;
	ms_byte ethnetinfo[msnet_maxnum_iface][128];
	ms_byte sdhostinfo[128];
	ms_byte num_net;
	ms_byte netinfo[msnet_maxnum_iface][128];
	ms_byte serialbusinfo[128];
}PCIDEVInfo;
typedef struct{
	ms_bool  isinit;
	ms_u08   num;
	ms_u08   ppp_num;
	NETCFGInfo cfg_info[msnet_maxnum_iface];
	ms_u08  num_defaultroute;
}NETCARDInfo;
typedef struct{
	ms_u08 modelname[256];	//cpu model name
	ms_u08 cpumhz[32];		//
	ms_u32 cpucores;			//
	ms_u32 siblings;			//  threads
	ms_u32 clflushsize;			//  1    cache
	ms_u32 cachesize;			//  2    cache
#if defined OS_PLATFORM_ARM64 ||defined OS_PLATFORM_ARMV7L
	ms_u08 bogomips[32];
#endif
}CPUHWInfo;

typedef struct{
	MS_U64 rate;		//bps
	MS_U64 bytes;	
	MS_U64 packets;	
	MS_U64 errs;
	MS_U64 drop;	
}NETDATAInfo;

typedef struct{
	NETDATAInfo rx_netdata_info[msnet_maxnum_iface];
	NETDATAInfo tx_netdata_info[msnet_maxnum_iface];
	ms_u32 speed[msnet_maxnum_iface];
	ms_u08 duplex[msnet_maxnum_iface];
	ms_u08 autoneg[msnet_maxnum_iface];
	ms_bool is_linkup[msnet_maxnum_iface];
}NETInfo;


typedef struct{
	long num;
	float usage[mscfg_maxnum_cpu];			//cpu
	ms_byte temperature[mscfg_maxphysicsnum_cpu][128];	
	MS_U64 btime;
	MS_U64 procs_running;
	MS_U64 procs_blocked;
}CPUInfo;

typedef struct{
	char mountpoint_num;
	char dev[mscfg_maxnum_partiton][128];
	char mountpoint[mscfg_maxnum_partiton][128];
	char blkinfo[mscfg_maxnum_partiton][maxlen_blkid_info];
	ms_s64 dtotal[mscfg_maxnum_partiton];
	ms_s64 dused[mscfg_maxnum_partiton];
	ms_s64 dfree[mscfg_maxnum_partiton];
}DISKInfo;
typedef struct{
	ms_byte 	name[32];	
	ms_byte 	model[128];	
	ms_bool	flag_hotplug;
	ms_byte	sizeinfo[32];
	ms_byte 	serialno[64];	
}DISKHWInfo;

typedef struct{
	CPUHWInfo	cpuhw_info;
	NETCARDInfo	netcard_info;
	PCIDEVInfo	pcidevinfo;
	ms_byte		num_diskdev;
	DISKHWInfo	diskhwinfo[msnet_maxnum_diskdev];       
	ms_byte	dmidecode_info[maxlen_dmidecode_info];
	ms_byte 	blk_info_serialsda[maxlen_blkserial_info];	
	ms_byte	systemuuid[maxlen_systemuuid];	
	ms_byte	processorid[maxlen_processorid];	
	ms_u08 	graphicsinfo[256];	
}HARDWAREInfo;

typedef struct{
	ms_u08 is_connected_ethernet;
	CPUInfo	cpu_info;
	MEMInfo	mem_info;
	NETInfo	net_info;
	DISKInfo	disk_info;
	time_t timep_connected_ethernet;
}SYSPERFORMInfo;
typedef enum{
	ossys_unknow		=0x0000,	
//param check 		
	ossys_ubuntu,
	ossys_centos,
	ossys_deepin,
}OSSYSTEM;
typedef struct OSInfo{
	ms_u08 is_littleendian;
	ms_u08 is_supperuser;
	ms_u08 sysname[256];		//Linux	
	ms_u08 nodename[256];	//msos
	ms_u08 release[256];		//4.4.0-53-generic
	ms_u08 version[256];		//Ubuntu 18.04.4 LTS
	ms_u08 machine[256];		//x86_64 
	ms_u08 server_starttime[256];
	struct rlimit stack_size;
	struct rlimit max_open_files;
	struct rlimit max_user_processes;
	struct rlimit time_cpu;
	struct rlimit data_seg;
	struct rlimit file_size;
	ms_u08 has_safenetdriver;

	ms_u08   route_default[512];
	ms_u08   dns1[16];
	ms_u08   dns2[16];
	OSSYSTEM ossystem;
}OSInfo;

#ifndef MSPERFORMANCE_C
extern HARDWAREInfo hardware_info;
extern OSInfo  os_info;
extern SYSPERFORMInfo  sysperform_info;
extern char  msperformance_api_network(NETCARDInfo *);
extern char msperformance_api_done(SYSPERFORMInfo *sysperform_info,NETCARDInfo *pnetcard_info,void (*waring_func)(void),
	ms_bool flag_disable_checknet,ms_bool flag_disable_checkmem,ms_bool flag_disable_checkdisk,ms_bool flag_disable_checkcpu);
extern char msperformance_api_init(void );
extern void msperformance_api_deinit(void );
extern void msperformance_api_checkethernet(void);
extern void msperformance_api_buildosinfo(char * outstr);
extern FILE *msfp_netdev;	
#endif
#endif

