#ifndef MSEMAIL_H
#define MSEMAIL_H
typedef struct EMAILSENDContxt{
	char dns[128];
	char from[128];
	char to[128];
	char theme[512];
	char user[128];
	char passwd[128];
	char attach[512];
	char content[40960];
}EMAILSENDContxt;
#ifndef MSEMAIL_C
//Waring :Before modify this api, please concat su.gao
extern void msemail_api_send( EMAILSENDContxt * );
#endif
#endif
