#ifndef AUTODELETE_H
#define AUTODELETE_H
#define msde_autodelete_day_listnum(day)			(day+3)			//1005-3=1002
#define msde_autodelete_hour_listnum(hour)		(hour*2+2)		//(1005-2)/2=501	501/24=20
#define msde_maxnum_list	1005

typedef struct AUTODELETEContext{
	char list_item[msde_maxnum_list][256];	
	unsigned int tail_item;
	unsigned int head_item;
	//cannot clear
	ms_u32  auto_delete;
	ms_byte dumppath[128];
	ms_byte dbname[256];
}AUTODELETEContext;

#ifndef AUTODELETE_C
extern void msautodelete_api_day_init(AUTODELETEContext *pautodelete_ctt,ms_u32  auto_deleteday,ms_string dumppath,char *dbname);
extern void msautodelete_api_day_run(AUTODELETEContext *);
extern void msautodelete_api_hour_init(AUTODELETEContext *pautodelete_ctt,ms_u32  auto_deletehour,char *dbname);
extern void msautodelete_api_hour_run(AUTODELETEContext *,char *);
#endif
#endif