#ifndef SOUNDPASS_API_H
#define SOUNDPASS_API_H
#ifndef SOUNDPASS_API_C
/***********************************API change log:
1.20150713: test app
	void main(void)
	{
		AudioValInit();
		LOG_DBG(mslog_level_info, FLAG,"AudioInDeviceGetList:%s",AudioInDeviceGetList());
		LOG_DBG(mslog_level_info, FLAG,"AudioOutDeviceGetList:%s",AudioOutDeviceGetList());
		AudioSetInDevice(0);
		AudioSetOutDevice(0);
		AudioSoundStart();
		while(1){
			Sleep(100);
		}
		AudioSoundStop();
	}
*/

/***********************************Help Info
1.Return val:
	0	all is ok
	-1	start audio in err
	-2	start audio out err
	-3	stop audio in err
	-4	stop audio out err
	-5	restart audio in err
	-6	restart audio out err	
*/

/**
 Description  : 
 Input        : None
 Return Value : 
 	NULL
 	String
NoteFormat: 
	soundintoout_ok
**/
extern int AudioValInit();
/**
 Description  : Get audio input device string list.Error has happened,return NULL
 Input        : None
 Return Value : 
 	NULL
 	String
NoteFormat: 
	deviceid:description;
**/
extern char *AudioInDeviceGetList();

/**
 Description  : Get audio Speaker device string list.Error has happened,return NULL
 Input        : None
 Return Value : 
 	NULL
 	String
 NoteFormat: 
 	deviceid:description;
**/
extern char *AudioOutDeviceGetList();

/**
 Description  : Set a deviceid to select audio input device.You can get the deviceid by call AudioInDeviceGetList()
 Input        : deviceid
 Return Value : 
	soundintoout_ok
**/
extern int AudioSetInDevice(unsigned int);

/**
 Description  : Set a deviceid to select audio output device.You can get the deviceid by call AudioOutDeviceGetList()
 Input        : None
 Return Value : 
	soundintoout_ok
**/
extern int AudioSetOutDevice(unsigned int);
/**
 Description  : Get sound out volume
 Input        : None
 Return Value : 
 	volume(0~100)
**/
extern int AudioGetOutvolume(void);
/**
 Description  :Set sound out volume
 Input        : 
 	param1: volume(0~100)
 Return Value : 
	soundintoout_ok
**/
extern int AudioSetOutvolume(int i_volume);
/**
 Description  : Set sound out mute state
 Input        : 
 	param1: 1-mute,0-unmute
 Return Value : 
 	soundintoout_err_instart
	soundintoout_err_outstart
	soundintoout_ok
**/
extern int AudioSetMute(int mute);

/**
 Description  : start sound in to out server
 Input        : None
 Return Value : 
 	soundintoout_err_instart
	soundintoout_err_outstart
	soundintoout_ok
**/
extern int AudioSoundStart();

/**
 Description  : restart sound in to out server
 Input        : None
 Return Value : 
 	soundintoout_err_inrestart
	soundintoout_err_outrestart
	soundintoout_ok
**/
extern int AudioSoundRestart();

/**
 Description  : stop sound in to out server
 Input        : None
 Return Value : 
 	soundintoout_err_instop
	soundintoout_err_outstop
	soundintoout_ok
**/
extern int AudioSoundStop();
/**
 Description  : open file log func
 Input        : None
 Return Value : None
**/
extern void AudioSoundFileLogOpen();

/**
 Description  : Set log level
 Input        : level	(assert-0,error-1,waring-2,info-3,debug-4,verbose-5,all-6,The default level is all)
 Return Value : None
**/
extern void AudioSoundSetLoglevel(char level);
/**
 Description  : close file log func
 Input        : None
 Return Value : None
**/
extern void AudioSoundFileLogClose();
#endif
#endif