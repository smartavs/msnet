#ifndef MSWEB_H
#define MSWEB_H
#include <libmsprotocol/msprotocol.h>
typedef struct MSSERVERGWDATAContext{
	ms_byte name[64];
	ms_byte ipaddr_client[32];
	ms_s32 fd;
	ms_u64 intime;
	//ms_byte databuf_dep[mscfg_maxlen_recv+1];
}MSSERVERGWDATAContext;

typedef struct REQUESTCLIENTContext{
	ms_byte ipaddr[128];
	ms_byte des[128];
	ms_u64   num_total;
	ms_u64   num_intercept;
	ms_u64   lasttime_request;
	ms_u64   numper;
	ms_u64   basetime;
	ms_u64   basenum;
	struct REQUESTCLIENTContext *next;	
}REQUESTCLIENTContext;

typedef struct MSWEBEPOLLRECVContext{
	ms_bool cmd_stop;
	ms_byte name[64];
	MSFIFOLISTContext * *pphfifolist_ctt;
	MSFIFOLISTContext * *pphfifolist_epollrecv_ctt;
	MSLOCKContext *pmslock_ctt;
	MSLOCKContext *pmslock_ctt_epollrecv;
	int max_event;
	int msepoll_fd;
	int maxnum_recv;
}MSWEBEPOLLRECVContext;
typedef struct MSSERVERGWContext{
	ms_bool cmd_stop;
	ms_byte name[64];
	MSFIFOLISTContext * *pphfifolist_ctt;
	MSFIFOLISTContext * *pphfifolist_epollrecv_ctt;
	MSLOCKContext *pmslock_ctt;
	MSLOCKContext *pmslock_ctt_epollrecv;
	URLProtocol * purl_ptl;
	URLContext  * purl_ctt;
	void *callback_args;
	ms_s08 (*callback_func)(void *callback_args,MSSERVERGWDATAContext *,URLProtocol *,URLContext  *);
	ms_u32 index;
}MSSERVERGWContext;
typedef struct MSSERVERContext{
	ms_string name;
	ms_s32 port;
	ms_s32 gw_num;
	ms_s32 intercepttime;
	ms_void * callback_args;
	ms_s08 (*callback_func)(void *callback_args,MSSERVERGWDATAContext *,URLProtocol *,URLContext  *);
	void *control_cbfunc_arg;
	ms_bool (*control_cbfunc)(void *control_cbfunc_arg);
	ms_bool flag_enbale_intercept;
	ms_bool flag_interceptresaddr;
	ms_bool flag_disable_intercept_all;
	MSSERVERGWContext *pmsgw_ctt_list;
	ms_bool flag_epoll_recv;
	ms_u32 maxnum_recv;
	REQUESTCLIENTContext *preclient_ctt;
	ms_bool *pinited;
	ms_bool *pflag_disableout;
	URLVal * pserver_urlval;
}MSSERVERContext;

#define msnet_maxnum_servergw 			512
#define msnet_defaultnum_servergw 			8
#define msnet_autonum_servergw 			10240
#define msnet_timeout_droprequest			20
#define msnet_timeout_droprequ_nodata		8
#define mstool_timeout_requestrecv			1000	
#define mstool_timeout_requestsend			1000
typedef enum MATCHSTREAM_RET{
	machstream_ret_Forbidden			=(ms_f32-2),
	machstream_ret_nofind				=(ms_f32-1),
	machstream_ret_Unauthorizedaccess	=(ms_f32-1),
	machstream_ret_nameerror			=ms_f32,
}MATCHSTREAM_RET;


#ifndef AH_WEB
#define AH_WEB		"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
#endif
#ifndef SPACE_WEB
#define SPACE_WEB	"&nbsp;&nbsp;&nbsp;&nbsp;"
#endif

#ifndef MSWEB_C
extern ms_void msweb_api_headtitle(char *outbuf,char *title,char *page);
extern ms_void msweb_api_cmheadtitle(char *outbuf,char *title,char *page,WEBHOSTInfo webhost_info);
extern ms_void msweb_api_tailtitle(char *outbuf, WEBHOSTInfo webhost_infos,ms_string version);
extern ms_void msweb_api_ok(char *outbuf);
extern ms_bool msweb_api_ok_withdata(char *jsondata,char *outbuf);
extern ms_void msweb_api_frequentvisits(char *outbuf);
extern ms_void msweb_api_initfirst(char *outbuf);
extern ms_void msweb_api_disableout(char *outbuf);
extern ms_void msweb_api_failed(char *outbuf,ms_string desinfo);
extern ms_void msweb_api_busy(char *outbuf,int cur_connect,int max_connect);
extern ms_void msweb_api_nofound(char *outbuf);
extern ms_void msweb_api_forbidden(char *outbuf);
extern ms_bool msweb_api_badrequest_withclose(char *outbuf);
extern ms_void msweb_streamapi_ok(char *outbuf,ms_string servername);
extern ms_void msweb_streamapi_MovedTemporarily(char *outbuf,ms_string locationurl,ms_string servername);;
extern ms_void msweb_rtspapi_MovedTemporarily(WEBREQUESTContext * prequest_ctt,ms_string locationurl,char *outbuf);
extern ENUPROTOCOLIndex msweb_api_getprotocol(WEBREQUESTContext *prequest_ctt);
extern ms_void msweb_api_gateway(MSSERVERGWContext *pmsgw_ctt);
extern ms_s08 msweb_api_dataread(MSSERVERGWDATAContext * pmsdata,WEBREQUESTContext *prequest_ctt,ms_byte *pdatabuf,int len,ms_u32 recv_timeout);
extern ms_u32 msweb_api_getgatewaynum(ms_u32 gw_num);
extern ms_bool msweb_api_interceptenter(REQUESTCLIENTContext **ppreclient_ctt,ms_string ipaddr_client,ms_s32 fd,ms_s32 intercepttime,
	ms_bool flag_enable,ms_bool flag_interceptresaddr,ms_bool flag_disable_intercept_all);
extern ms_bool msweb_api_interceptexit(REQUESTCLIENTContext **ppreclient_ctt);
extern ms_u32 msweb_api_getotal_reqnum(MSSERVERGWContext *pmsgw_ctt_list);
extern ms_u32 msweb_api_getotal_reqnum_epollrecv(MSSERVERGWContext *pmsgw_ctt_list);
extern ms_void 
	msweb_api_enpoutstr(ms_string  ms_out outstr,ms_string exter_str);
extern ms_bool 
	msweb_api_enpmatch(ms_string  ms_out instr);
extern ms_void msweb_api_server(MSSERVERContext *pserver_ctt);
extern ms_s32 msweb_api_charconvert(ms_string ms_in instr,ms_string ms_out outstr,ms_u32 ms_in len);
extern ms_void msweb_api_formatconvert(char * inbuf,char *outbuf,int len);
extern ms_void msweb_api_formatconvert2(char * inbuf,char *outbuf,int len);
extern ms_s32 msweb_api_converttochar(ms_string ms_in instr,ms_string ms_out outstr,ms_u32 ms_in len);

#endif
#define msstr_intercept_black	"black"
#define msstr_intercept_white	"white"

#define MSWEB_FONT_STYLE 		"style=\"color:black;\""
#define MSWEB_H1_STYLE 			"style=\"color:red;\""
#define MSWEB_H2_STYLE 			"style=\"color:red;\""
#define MSWEB_HT1_STYLE 		"style=\"color:red;\""
#define MSWEB_HT2_STYLE 		"style=\"color:yellow;\""
#define MSWEBAPI_INTERCEPT_PERTIME		ms_usmseconds(500)
#endif

