#ifndef MSTOOL_H
#define MSTOOL_H

#include "msautomaintenance.h"
#include "msautomount.h"
#include "msemail.h"
#include "msethtool.h"
#include "msfeedback.h"
#include "msperformance.h"
#include "msphddns.h"
#include "mspppoe.h"
#include "msrouter.h"
#include "mssched.h"
#include "mssoundpass_api.h"

#ifndef MSTOOL_C
extern ms_void 
	mstool_api_infofunc(ms_string ms_out pbuf);
extern ms_string 
	mstool_api_version(ms_void);
#endif

#endif
