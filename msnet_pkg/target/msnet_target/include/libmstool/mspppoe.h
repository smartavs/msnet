#ifndef MSPPPOE_H
#define MSPPPOE_H
typedef enum{
	mctuerr_pppoe_noinit				=0x4000,
	mctuerr_pppoe_noethn				=0x4001,
	mctuerr_pppoe_cmdfail				=0x4002,
	mctuerr_pppoe_nosetup			=0x4003,
	mctuerr_pppoe_nolinkup			=0x4004,
	mctuerr_pppoe_paramerror			=0x4005,
}PPPOERet;

typedef struct{
	ms_byte usage[128];         
	ms_byte ethn_name[128];
	ms_byte username[128];
	ms_byte userpasswd[128];
	ms_byte debug[128];
	ms_byte demond[128];
	ms_byte firewall[128];
	ms_byte dns1[128];
	ms_byte dns2[128];
}PPPOEParam;
typedef struct{
	ms_byte name[64];
	ms_byte ipaddress[64];
	ms_byte netmask[64];
} PPPInfo;

typedef struct{
	//set by user  ,through call mspppoe_api_setup() function
	PPPOEParam pppoe_param;
	//set by system
	PPPInfo ppp_info;
	ms_bool  is_connect;	
	ms_bool is_setup;
	ms_byte ipaddr_testing[64];
	ms_byte time_string[64];
} ETHNInfo;

#define MAXNUM_ETH		16

typedef struct ETHPPPContext{
	ETHNInfo ethn_info[MAXNUM_ETH];
	ms_bool init;
}ETHPPPContext;

extern ETHPPPContext ethnppp_ctt;

#ifndef MSPPPOE_C
	#define MSPPPOE_C_INTERFACE extern
#else
	#define MSPPPOE_C_INTERFACE 
#endif
MSPPPOE_C_INTERFACE ETHNInfo *mspppoe_api_matchethn(ms_string ms_in ethn_name,ETHPPPContext * ms_in pethnppp_ctt);
MSPPPOE_C_INTERFACE ms_void mspppoe_api_writedb(ms_void);
MSPPPOE_C_INTERFACE ms_void mspppoe_api_init(ms_void);
MSPPPOE_C_INTERFACE ms_s32 mspppoe_api_setup(PPPOEParam ms_in pppoe_param);
MSPPPOE_C_INTERFACE ms_s32 mspppoe_api_start(ms_string ms_in ethn_name);
MSPPPOE_C_INTERFACE ms_s32 mspppoe_api_stop(ms_string ms_in ethn_name);
MSPPPOE_C_INTERFACE ms_void mspppoe_api_deinit(ms_void);
#endif

