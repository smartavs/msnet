#ifndef MSROUTER_H
#define MSROUTER_H


#define str_multicasthost		"multicasthost"
#define str_mh_sourceaddr		"sourceaddr"
#define str_mh_multicastaddr	"multicastaddr"
#define str_mh_interfacename	"interfacename"
typedef struct{
	char source_addr[256];
	char multicast_addr[256];
	char interface_name[256];
}MULTICASTHOSTCOntext;

typedef struct{
	MULTICASTHOSTCOntext multicasthost_ctt[120];
	int multicasthost_num;
}ROUTERCOntext;

#ifndef MSROUTER_C
extern ROUTERCOntext route_ctt;
extern char msroute_api_check(ROUTERCOntext *route_ctt);
extern char msroute_api_init(ROUTERCOntext *);
#endif
#endif

