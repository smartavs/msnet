#ifndef MSIFCONFIG_H
#define MSIFCONFIG_H
#ifndef MSIFCONFIG_C
#include "msperformance.h"
extern ms_bool msifconfig_api_init(ms_void);
extern ms_s08 msifconfig_api_set(NETCARDInfo* ms_in pnetcardinfo,ms_u08 * ms_out  poutstr);
extern ms_s08 msifconfig_api_restart(ms_u08 * ms_out  poutstr);
extern ms_void msifconfig_api_buildinfo_json(ms_void * jsnode_item);
extern ms_void msifconfig_api_parseinfo_json(ms_void *js_root,NETCARDInfo *pnetcardinfo);
 #endif
 #endif

