#ifndef MSETHTOOL_H
#define MSETHTOOL_H
#ifndef MSETHTOOL_C
extern void msethtool_api_checkport(char *ethname,char second);
extern ms_s08 msethtool_api_getinfo(ms_cstring devname, ms_pu32 speed, ms_pu08 duplex, ms_pu08 autoneg, ms_bool* link) ;
extern int msethtool_api_setinfo(const char* devname, int speed, int duplex, int autoneg) ;
 #endif
 #endif

