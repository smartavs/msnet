#! /bin/bash
CURPATH=`pwd`
SERVICE_NAME=msnet

doprint()
{
    case "${SYS_LANGUAGE}" in
        "CHINESE" | "CH" )
            echo "$1"
            ;;

        *)
            echo "$2"
            ;;
    esac
}

#   === BEGIN：安装前检测函数模块 ===    #
do_pre_checkroot()
{
    if [ "$(id -u)" != "0" ]; then
        doprint "本脚本必须在root权限下执行！！！" "!!!! This script must be run as root !!!"
        doprint "提示:请使用'su root'切换到root权限。" "USAGE: please use command: 'su root' change to account root if your account is sudo user"
        exit 1
    else
        doprint "你目前是root权限，脚本将继续执行..." "OK.... you are running by root.. system will keep going..."
    fi
}

do_pre_checkos()
{
    echo " "
    echo "=====================${SERVICE_NAME}:Check os==============================="
    echo " "
    
    TEST_SYSTEM="ubuntu 16.04,ubuntu 18.04 or centos 8.1"
    if  ( lsb_release -a >/dev/null);then
        if  ( lsb_release -a | grep Ubuntu >/dev/null);then
            if   ( lsb_release -r | grep 16.04 >/dev/null);then
                SYSTEM="ubuntu1604"
            elif   ( lsb_release -r | grep 18.04 >/dev/null);then
                SYSTEM="ubuntu1804"
            else
                echo "Unteset ubuntu, you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to ubuntu1804 for test-running"
	        SYSTEM="ubuntu1804"
            fi
        #elif  ( lsb_release -a | grep Uos >/dev/null);then
        #    if   ( lsb_release -r | grep "20 Home" >/dev/null);then
        #        SYSTEM="uos20Home"
        #   else
        #       echo "Unteset uos,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to uos20Home for test-running"
	#       SYSTEM="uos20Home"
        #   fi
        else
            echo "Unteset lsb_release,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to ubuntu1804 for test-running"
            SYSTEM="ubuntu1804"
        fi         
    elif  ( cat /etc/redhat-release | grep "CentOS Linux release" >/dev/null);then
        if  ( cat /etc/redhat-release | grep "CentOS Linux release" >/dev/null);then
            if   ( cat /etc/redhat-release | grep "CentOS Linux release 8.1" >/dev/null);then
                SYSTEM="centos8.1"
            else
                echo "Unteset redhat,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to centos8.1 for test-running"
                SYSTEM="centos8.1"
            fi
        else
            echo "Unteset redhat,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to centos8.1  for test-running"
            SYSTEM="centos8.1"
        fi   
    else
        echo "Unteset os,you should change your os to "${TEST_SYSTEM}",I will set SYSTEM to ubuntu1804 for test-running"
        SYSTEM="ubuntu1804"
    fi
    
    case "${SYSTEM}" in
    "ubuntu1604" | "ubuntu1804"| "centos8.1")
        echo "OK，your OS is $SYSTEM,you can use this script..."
        ;;
    *)
        echo "Unteset os $SYSTEM you should change your os to "${TEST_SYSTEM}""
        ;;
    esac

    OS_MACHINE=`uname -m`
    if [ "$OS_MACHINE" = "x86_64" ]; then
        echo "OK，your OS is ${SYSTEM}_${OS_MACHINE},keep going...."
    else
        echo "Sorry, We only support${TEST_SYSTEM}, This script doesn't support your OS platform(${SYSTEM}${OS_MACHINE})，will exit"
        exit 1
    fi
}

do_install_target()
{
    rm -fr /usr/local/msnet
    mkdir -p /etc/msnet/db/
    cp -fr ${CURPATH}/msdb_msnet.db   /etc/msnet/db/msdb_msnet.db
    cp -fr ${CURPATH}/msnet_target    /usr/local/msnet
    cp -fr uninstall.sh               /usr/local/bin/uninstall_${SERVICE_NAME} 
    cp -fr ${CURPATH}/libmsnet.conf   /etc/ld.so.conf.d/libmsnet.conf
    ldconfig

    chmod 777 /usr/local/bin/*
    doprint "安装可执行文件完成！" "Install binfile success!"
}

do_install_startup()
{
    case "${SYSTEM}" in
    "ubuntu1604" | "ubuntu1804")
        if (cp ${CURPATH}/${SERVICE_NAME} /etc/init.d && chmod +x /etc/init.d/${SERVICE_NAME}&& update-rc.d ${SERVICE_NAME} defaults 90);then 
            doprint "${SYSTEM}:成功设置 ${SERVICE_NAME} 服务器为开机启动!" "${SYSTEM} :Setup ${SERVICE_NAME} startup success!"
        else
            doprint "${SYSTEM}:设置 ${SERVICE_NAME} 服务器开机启动失败!" "${SYSTEM} :Setup ${SERVICE_NAME} startup failed!"
            exit 1
        fi 
        ;;
    "centos8.1")
        if (cp ${CURPATH}/${SERVICE_NAME} /etc/init.d && chmod +x /etc/init.d/${SERVICE_NAME} && chkconfig --add ${SERVICE_NAME});then 
            doprint "${SYSTEM}:成功设置 ${SERVICE_NAME} 服务器为开机启动!" "${SYSTEM} :Setup ${SERVICE_NAME} startup success!"
        else
            doprint "${SYSTEM}:设置 ${SERVICE_NAME} 服务器开机启动失败!" "${SYSTEM} :Setup ${SERVICE_NAME} startup failed!"
            exit 1
        fi
        ;;
    esac
}

service ${SERVICE_NAME} stop

do_pre_checkroot

do_pre_checkos

do_install_target

do_install_startup

systemctl daemon-reload

service ${SERVICE_NAME} start
