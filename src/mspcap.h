#ifndef MSPCAP_H
#define MSPCAP_H

#include <pcap.h>
#ifndef MSPCAP_C
extern ms_s08 
	mspacp_api_setopt_noactivated (pcap_t *handle);
extern pcap_t * 
	mspacp_api_open(ms_string pdev);
extern ms_bool 
	mspacp_api_isen10mb(pcap_t *handle);
extern ms_s08 
	mspacp_api_setfilter(pcap_t *handle,ms_string dev,ms_string filter_exp);
extern ms_void 
	mspacp_api_close(pcap_t *handle);
extern int 
	mspacp_api_send(pcap_t * p, ms_byte * buf, ms_s32 size);
extern pcap_dumper_t *
	mspacp_api_dumpopen(pcap_t * p, const char * fname, FILE *f);
extern ms_void 
	mspacp_api_dumpwrite(FILE *f, const struct pcap_pkthdr * h, const u_char * sp);
extern FILE * 
	 mspacp_api_dumpgetfilehd(pcap_dumper_t * p);
extern long 
	 mspacp_api_dumpftell(pcap_dumper_t * p);
extern ms_void 
	 mspacp_api_dumpclose(pcap_dumper_t * p);
#endif
#endif