#define MSNET_C
#include <stdio.h>
#include <unistd.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <arpa/inet.h> 
#include <netinet/ip.h> 
#include <netinet/udp.h> 
#include <netinet/in.h> 
#include <net/if_arp.h> 
#include <netdb.h> 
#include <sys/ioctl.h> 
#include <sys/stat.h> 
#include <linux/if_packet.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h> 
#include <strings.h>
#include <unistd.h> 
#include <netdb.h> 
#include <sys/ioctl.h> 
#include <fcntl.h> 
#include <linux/if_ether.h>
#include <net/ethernet.h>
#include <libmscommon/mscommon.h>
#include <libmscommon/mstime.h>
#include <libmscommon/msnetframe.h>
#include <libmscommon/msstring.h>
#include <libmscommon/mssignal.h>
#include <libmscommon/msthread.h>
#include <libmscommon/msnetwork.h>
#include <libmscommon/mscjson.h>
#include <libmscommon/msmd.h>
#include <libmstool/msweb.h>
#include <libmsprotocol/msprotocol.h>
#include <libmslog/mslog.h>
#include <net/ethernet.h>
#include "mspcap.h"

#define FLAG "msnet"

#define MAXNUM_IPNET_SEGMENT		100
#define MAXNUM_IPADDR_PER_SEGMENT	512	

#define MAXNUM_IPADDR_SCANPORT	100	
#define MAXNUM_PORT_PER_IPADDR	65535	

#define MAX_CHECKNUM_IP			10240
#define MAX_CHECKNUM_MAC			5120
#define MAX_CHECKNUM_BROADCAST	5120
#define NUM_STATINFO_FIFO			3


#define mswebt_statinfo		"STATINFO"
#define mswebt_checkip		"CHECKIP"
#define mswebt_checkport	"CHECKPORT"
#define mswebt_checkmac		"CHECKMAC"
#define mswebt_broadcast	"BROADCAST"
#define mswebt_meminfo	"MEMINFO"
#define mswebt_msnet		"MSNET"

#define msweb_statinfo		"数据构成分析"
#define msweb_checkip		"网络地址分析"
#define msweb_checkport		"网络端口扫描"
#define msweb_checkmac		"物理地址分析"
#define msweb_broadcast		"广播数据分析"
#define msweb_meminfo		"内存分配分析"
#define msweb_msnet		"智慧网络检测平台"

#define path_msdb_msnet  		"/etc/msnet/db/"
#define msdb_msnet  		"/etc/msnet/db/msdb_msnet.db"
typedef struct MSNETSTATContext{
	ms_u32 num_total;
	ms_u32 num_ipv4;
	ms_u32 num_ipv6;
	ms_u32 num_arp;
	ms_u32 num_others;

	ms_u32 num_ipv4_tcp;
	ms_u32 num_ipv4_udp;
	ms_u32 num_ipv4_icmp;
	ms_u32 num_ipv4_igmp;
	ms_u32 num_ipv4_ipv6;
	ms_u32 num_ipv4_others;


	ms_u64 len_total;
	ms_u64 len_ipv4;
	ms_u64 len_ipv6;
	ms_u64 len_arp;
	ms_u64 len_others;

	ms_u64 len_ipv4_tcp;
	ms_u64 len_ipv4_udp;
	ms_u64 len_ipv4_icmp;
	ms_u64 len_ipv4_igmp;
	ms_u64 len_ipv4_ipv6;
	ms_u64 len_ipv4_others;
}MSNETSTATContext;
typedef struct MSNETINPUTParam{
	ms_byte	program_name[128] ;
	ms_byte	devname[64] ;
	ms_bool	flag_watchdog;
	ms_bool	flag_daemon;
	ms_bool	flag_output_event ;
	ms_bool	flag_output_header ;
	ms_bool	flag_output_parse ;
	ms_bool	flag_stat;
	ms_bool	flag_checkip_all ;
	ms_byte	ipaddr_list_scanip[MAXNUM_IPNET_SEGMENT*32];//32==ms_buflen("192.168.000.001-192.168.000.255#)"
	ms_byte	port_list_scanport[MAXNUM_IPNET_SEGMENT*8];//32==ms_buflen("122#80)"
	ms_byte	pipaddr_list_scanport[MAXNUM_IPNET_SEGMENT*16];//32==ms_buflen("192.168.000.001#)"
	ms_byte	filter_string[512] ;
} MSNETINPUTParam;
typedef struct MSIPCONFLICTContent{
	ms_bool flag_used;
	ms_u16 type;	//ip  arp
	ms_u08 macaddr_source[18];
	ms_u08  arp_request_ipaddr[16];
	ms_u08  ipaddr_source[16];
	ms_u32  times;
	//arp used
	ms_u16 op;
	ms_u32  arp_request_mac_num[20];
	ms_u08 arp_request_mac[20][18];

	ms_u32 index_pmsarpingip;
	ms_u32 index_ipaddrs;
	ms_bool flag_gl_msarpingip;
}MSIPCONFLICTContent;

typedef struct MSMACCONFLICTContent{
	ms_bool flag_used;
	ms_u16 type;	//ip
	ms_u08 macaddr[18];
	ms_u08  ipaddr[20][16];
}MSMACCONFLICTContent;

typedef struct MSBCASTContent{
	ms_bool flag_used;
	ms_u16 type;	//ip
	ms_u08 macaddr[18];
	ms_u08  ipaddr[20][16];
}MSBCASTContent;
typedef struct MSSCANIPContext{
	ms_bool flag_ipaddrs;
	ms_bool flag_scanip[MAXNUM_IPADDR_PER_SEGMENT];
	ms_bool flag_gl_msipconflict[MAXNUM_IPADDR_PER_SEGMENT];
	ms_byte ipaddrs_start[16];
	ms_byte ipaddrs_end[16];
	ms_byte ipaddrs_list[MAXNUM_IPADDR_PER_SEGMENT][16];
	ms_byte status[MAXNUM_IPADDR_PER_SEGMENT][32];
	ms_u32 totalnum_ipaddr;
	ms_u32 index_msipconflict[MAXNUM_IPADDR_PER_SEGMENT];
}MSSCANIPContext;
typedef struct MSSCANPORTContext{
	ms_byte ipaddrs[128];
	ms_byte status[128];
	ms_byte port_info[MAXNUM_PORT_PER_IPADDR*5];
}MSSCANPORTContext;
typedef struct MSNETContext{
	ms_u08 index_in;
	ms_u08 index_out;
	MSNETSTATContext msnetstat_ctt[NUM_STATINFO_FIFO];
	ms_u32 pktindex;
	MSNETINPUTParam *pmsnetinput_param;
	ms_u32  totalnum_arp;
	ms_u32  totalnum_maclist;
	ms_u32  totalnum_broadcast;
	ms_s32 num_ipaddr_scanip;
	ms_s32 num_ipaddr_scanport;
	ms_u08 max_times_num;

	MSIPCONFLICTContent *plist_msipconflict_ctt;
	MSMACCONFLICTContent  *plist_msmacconflict_ctt;
	MSBCASTContent  *plist_msbcast_ctt;


	MSSCANIPContext *pmsscanip_ctt_list;
	MSSCANPORTContext *pmsscanport_ctt_list;
}MSNETContext;
typedef struct MSSCANIPPARAMContext{
	MSNETContext *pmsnet;
	ms_s32 index;
}MSSCANIPPARAMContext;
typedef struct MSSCANPORTPARAMContext{
	MSNETContext *pmsnet;
	ms_s32 index;
}MSSCANPORTPARAMContext;

typedef struct HRGATEWAYDATAContext {
	struct pcap_pkthdr header;
	 u_char packet[5120];
	MSNETContext *pmsnet_ctt;
} HRGATEWAYDATAContext;

static MSLOCKContext mslockctt_pkt;
static MSFIFOLISTContext * pfifolistctt_pkt=ms_null;

static ms_void msnet_abapi_savecfg(MSNETINPUTParam *pmsnetinput_param)
{
	ms_byte cmdbuf[1024]={0};
	ms_cmd(cmdbuf, "mkdir  -p %s",path_msdb_msnet);
	cJSON * js_root = cJSON_CreateObject();
	if(NULL == js_root){
		ms_errNoret("cJSON_CreateObject failed");
	}
	MSJSON_BUILD_STRING_DEFAULT(js_root, "devname", pmsnetinput_param->devname,"eth0");
	MSJSON_BUILD_INT( js_root, "flag_output_event", pmsnetinput_param->flag_output_event);
	MSJSON_BUILD_INT(js_root, "flag_output_header", pmsnetinput_param->flag_output_header);
	MSJSON_BUILD_INT(js_root, "flag_output_parse", pmsnetinput_param->flag_output_parse);
	MSJSON_BUILD_INT(js_root, "flag_stat", pmsnetinput_param->flag_stat);	
	MSJSON_BUILD_INT(js_root, "flag_checkip_all", pmsnetinput_param->flag_checkip_all);	
	MSJSON_BUILD_STRING_DEFAULT( js_root, "ipaddr_list_scanip", pmsnetinput_param->ipaddr_list_scanip,"none");
	MSJSON_BUILD_STRING_DEFAULT(js_root, "port_list_scanport", pmsnetinput_param->port_list_scanport,"none");	
	MSJSON_BUILD_STRING_DEFAULT(js_root, "ipaddr_list_scanport", pmsnetinput_param->pipaddr_list_scanport,"none");
	MSJSON_BUILD_STRING_DEFAULT(js_root, "filter_string", pmsnetinput_param->filter_string,"none");
	
	char * json_string = cJSON_Print(js_root);
	cJSON_Delete(js_root);
	if(NULL==json_string){
		ms_errNoret("cJSON_Print failed");
	}
	ms_s08 dbname[mscfg_maxlen_url]={0};
	ms_strcpy(dbname,msdb_msnet);
	if(msmd_api_writeProtect(dbname,json_string,strlen(json_string))<0){
		ms_error("Write %s failed",dbname);
	}
	ms_free(json_string);
}

static ms_s08 msnet_abapi_readcfg(MSNETINPUTParam *pmsnetinput_param)
{
	char json_buf[40960]={0};
	if(msmd_api_read2(msdb_msnet, json_buf, sizeof(json_buf))<0){
		msnet_abapi_savecfg(pmsnetinput_param);
		return -1;
	}
	cJSON *js_root = cJSON_Parse(json_buf);
	if(!js_root) {
		ms_errRet(-1,"Get root faild(%s)",msdb_msnet);
	}
	cJSON *js_item= ms_null;
	MSJSON_PARSE_STRING(js_item, js_root, "devname", pmsnetinput_param->devname);
	MSJSON_PARSE_INT_DEFALUT(js_item, js_root, "flag_output_event", pmsnetinput_param->flag_output_event,ms_false);
	MSJSON_PARSE_INT_DEFALUT(js_item, js_root, "flag_output_header", pmsnetinput_param->flag_output_header,ms_false);
	MSJSON_PARSE_INT_DEFALUT(js_item, js_root, "flag_output_parse", pmsnetinput_param->flag_output_parse,ms_true);
	MSJSON_PARSE_INT_DEFALUT(js_item,js_root, "flag_stat", pmsnetinput_param->flag_stat,ms_true);	
	MSJSON_PARSE_INT_DEFALUT(js_item,js_root, "flag_checkip_all", pmsnetinput_param->flag_checkip_all,ms_false);	
	MSJSON_PARSE_STRING(js_item, js_root, "ipaddr_list_scanip", pmsnetinput_param->ipaddr_list_scanip);
	MSJSON_PARSE_STRING(js_item, js_root, "port_list_scanport", pmsnetinput_param->port_list_scanport);
	MSJSON_PARSE_STRING(js_item, js_root, "ipaddr_list_scanport", pmsnetinput_param->pipaddr_list_scanport);
	MSJSON_PARSE_STRING(js_item, js_root, "filter_string", pmsnetinput_param->filter_string);
	msnet_abapi_savecfg(pmsnetinput_param);
	return 0;
}
ms_string mslog_api_version(ms_void)
{
	return "01.00.01";
}
static void msnet_innerapi_version()
{
	ms_info( "msnet v%s build %s %s" , mslog_api_version(), __DATE__ , __TIME__ );
	ms_info( "copyright by su.gao<sugao_cn@163.com> " );
	return;
}
static void msnet_innerapi_usage()
{
	ms_info( "USAGE : msnet [-v ] [ -l]   [ -i  network_interface] [ -f filter_string] [ -o [EHD] ]" );
	ms_info( "  -h: show help" );
	ms_info( "  -v: show version" );
	ms_info( "  -db:running in debug mode,will disable daemon and watchdog" );
	ms_info( "  -wd <n>: running with watchdog function(0-disable;1-enable),the default value is \"enable\"" );
	ms_info( "  -d     <n>: running in daemon mode(0-disable;1-enable),the default value is \"enable\"" );
	ms_info( "  pn : set programname" );
	ms_info( "  -l : list all find device" );
	ms_info( "  -i       <interface>: set input network interface" );
	ms_info( "  -f: set libpcap filter string" );
	ms_info( "  -oe : Output EVENT" );
	ms_info( "  -oh : Output HEADER INFO" );
	ms_info( "  -op: Output RESULT OF PARSE ARP" );
	ms_info( "  -ca : check all arp data" );
	ms_info( "  -ipaddr_list_scanip       <ipstart1-ipend1#ipstart2-ipend2>: send broadcast arp packects(like:192.168.0.1-192.168.0.255#192.168.200.1-192.168.200.255)" );
	ms_info( "  -port_list_scanport		<port1#port1>: (like:22#8080)" );
	ms_info( "  -ipaddr_list_scanport       <ip1#ip2>: (like:192.168.0.2#192.168.0.3)" );
	ms_info( "  -stat: stat packects" );
	
	ms_info( "NOTICE : See pcap-filter(7) for the syntax of filter" );
	return;
}
static ms_void msnet_innerapi_optdefault(MSNETINPUTParam *pmsnetinput_param)
{
	ms_pamcheck(pmsnetinput_param,"pmsnetinput_param");
	ms_stru0(pmsnetinput_param, MSNETINPUTParam);
	pmsnetinput_param->flag_output_event=ms_false;
	pmsnetinput_param->flag_output_header=ms_false;
	pmsnetinput_param->flag_output_parse=ms_true;
	pmsnetinput_param->flag_stat=ms_true;

	ms_strcpy(pmsnetinput_param->program_name,"msnet");
	#ifdef CONFIG_DEBUG
		pmsnetinput_param->flag_daemon=ms_false;
		pmsnetinput_param->flag_watchdog=ms_false;
		pmsnetinput_param->flag_checkip_all=ms_false;
		ms_strcpy(pmsnetinput_param->ipaddr_list_scanip,"192.168.0.1-192.168.0.255#192.168.200.1-192.168.200.245");
		ms_strcpy(pmsnetinput_param->port_list_scanport,"22#8090" );	
		ms_strcpy(pmsnetinput_param->pipaddr_list_scanport,"220.180.145.7#39.129.41.66#192.168.200.244#192.168.200.97" );
	#else
		pmsnetinput_param->flag_daemon=ms_true;
		pmsnetinput_param->flag_watchdog=ms_true;
		pmsnetinput_param->flag_checkip_all=ms_false;
		ms_strcpy(pmsnetinput_param->ipaddr_list_scanip,"none");
		ms_strcpy(pmsnetinput_param->port_list_scanport,"none");
		ms_strcpy(pmsnetinput_param->pipaddr_list_scanport,"none");
	#endif
	ms_strcpy(pmsnetinput_param->filter_string,"none");
}
static ms_void msnet_innerapi_optparse(MSNETINPUTParam *pmsnetinput_param,int argc, char **argv)
{
	ms_byte errbuf[PCAP_ERRBUF_SIZE]={0};
	ms_s32 argindex;
	for( argindex = 1 ; argindex < argc ; argindex++ )
	{
		if( ms_strncmp_saeq( argv[argindex], "-h" ) ){
			msnet_innerapi_usage();
			ms_exit(0);
		}else if( ms_strncmp_saeq( argv[argindex], "-v" ) ){
			msnet_innerapi_version();
			ms_exit(0);
		}else if( ms_strncmp_saeq( argv[argindex]  , "-db" ) ){
			pmsnetinput_param->flag_daemon=ms_false;
			pmsnetinput_param->flag_watchdog=ms_false;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-d" ) && argindex + 1 < argc ){
			pmsnetinput_param->flag_daemon=ms_atoi(argv[argindex+1] );
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-wd" ) && argindex + 1 < argc ){
			pmsnetinput_param->flag_watchdog=ms_atoi(argv[argindex+1] );
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-pn" ) && argindex + 1 < argc ){
			ms_strcpy(pmsnetinput_param->program_name, argv[argindex+1] );
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-i" ) && argindex + 1 < argc ){
			ms_strcpy(pmsnetinput_param->devname, argv[argindex+1] );
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-lo" ) && argindex + 1 < argc ){
			mslog_api_init( ms_atoi( argv[argindex+1]), ms_null,ms_null, ms_null);
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-l" ) ){
			pcap_if_t	*network_interface_list = NULL ;
			pcap_if_t	*network_interface = NULL ;
			ms_s32 nret = pcap_findalldevs( & network_interface_list ,errbuf ) ;
			if( nret == -1 ){
				ms_errExit(-1, "ERROR : pcap_findalldevs failed : %s" ,errbuf );
			}
			network_interface = network_interface_list ;
			while( network_interface ){
				ms_info( "%s %s" , network_interface->name , network_interface->description );
				network_interface = network_interface->next ;
			}
			pcap_freealldevs( network_interface_list );
			ms_exit(0);
		}else if( ms_strncmp_saeq( argv[argindex]  , "-f" ) && argindex + 1 < argc ){
			ms_strcpy(pmsnetinput_param->filter_string,argv[argindex+1]);
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-oe" ) ){
			pmsnetinput_param->flag_output_event = ms_true ;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-oh" ) ){
			pmsnetinput_param->flag_output_header = ms_true ;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-op" ) ){
			pmsnetinput_param->flag_output_parse = ms_true ;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-stat" ) )	{
			pmsnetinput_param->flag_stat = ms_true ;
		}else if( ms_strncmp_saeq( argv[argindex]  , "-ca" )  ){
			pmsnetinput_param->flag_checkip_all= ms_true ;
		}else if( ms_strncmp_saeq( argv[argindex] , "-ipaddr_list_scanip" ) && argindex + 1 < argc ){
			ms_strcpy(pmsnetinput_param->ipaddr_list_scanip, argv[argindex+1] );
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex] , "-port_list_scanport" ) && argindex + 1 < argc ){
			ms_strcpy(pmsnetinput_param->port_list_scanport, argv[argindex+1] );
			argindex++;
		}else if( ms_strncmp_saeq( argv[argindex] , "-ipaddr_list_scanport" ) && argindex + 1 < argc ){
			ms_strcpy(pmsnetinput_param->pipaddr_list_scanport, argv[argindex+1] );
			argindex++;
		}else{
			ms_error( "***ERROR : invalid command parameter '%s'" , argv[argindex] );
			msnet_innerapi_usage();
			ms_exit(1);
		}
	}
}

ms_void msnet_innerapi_showpacket(MSNETContext *pmsnet_ctt,ETHERContext *pether_ctt)
{
	FRAMEContext *pframe_ctt=&pether_ctt->frame_ctt;
	
	if((++pmsnet_ctt->pktindex)>4200000000){
		pmsnet_ctt->pktindex=1;
	}
	if( ETHERTYPE_IP==pether_ctt->frame_ctt.type ){
		IPHEADERContext *ipheader_ctt=&pether_ctt->ethertype_data.ipheader_ctt;
		ms_byte tbuf[2048]={0};
		ms_byte outbuf[2048]={0};

		ms_byte source_ip[32]={0};
		ms_byte dest_ip[32]={0};
		ms_sprintfs(source_ip, "%s", inet_ntoa( *(struct in_addr *)(&ipheader_ctt->source_ip)));
		ms_sprintfs(dest_ip, "%s", inet_ntoa( *(struct in_addr *)(&ipheader_ctt->dest_ip)) );
		ms_strcats(tbuf,outbuf,"%08d.%s  \t%s|%s  \t%s|%s",pmsnet_ctt->pktindex,ipheader_ctt->str_protocol,
			source_ip ,pframe_ctt->str_source_mac,
			dest_ip,pframe_ctt->str_dest_mac);

		if(IPPROTO_TCP==ipheader_ctt->protocol){
			TCPHEADERContext *ptcpheader_ctt=&pether_ctt->ipprotocol_data.tcpheader_ctt;
			ms_strcats(tbuf,outbuf,"\t%05d->%05d",ntohs( ptcpheader_ctt->source_port ) ,ntohs( ptcpheader_ctt->dest_port ));
			ms_strcats(tbuf,outbuf,"\tSeq=%u,Ack=%u,len=%u|%u",ptcpheader_ctt->seq_num,ptcpheader_ctt->ack_num,ipheader_ctt->total_len,pether_ctt->data_len);				
			ms_byte buf_flags[64]={0};
			if(ptcpheader_ctt->ack){
				ms_strcats(tbuf, buf_flags, "ACK");
			}
			if(ptcpheader_ctt->psh){
				if(ms_buflen(buf_flags)){
					ms_strcats(tbuf, buf_flags, ",");
				}
				ms_strcats(tbuf, buf_flags, "PUSH");
			}
			if(ptcpheader_ctt->rst){
				if(ms_buflen(buf_flags)){
					ms_strcats(tbuf, buf_flags, ",");
				}
				ms_strcats(tbuf, buf_flags, "REST");
			}
			if(ptcpheader_ctt->syn){
				if(ms_buflen(buf_flags)){
					ms_strcats(tbuf, buf_flags, ",");
				}
				ms_strcats(tbuf, buf_flags, "SYN");
			}
			if(ptcpheader_ctt->fin){
				if(ms_buflen(buf_flags)){
					ms_strcats(tbuf, buf_flags, ",");
				}
				ms_strcats(tbuf, buf_flags, "FIN");
			}
			ms_strcats(tbuf,outbuf,"\t[%s]",buf_flags);
		}else if(IPPROTO_UDP==ipheader_ctt->protocol){
			UDPHEADERContext *pudpheader_ctt=&pether_ctt->ipprotocol_data.udpheader_ctt;
			ms_strcats(tbuf,outbuf,"\t%u->%u \tlen=%u|%u",ntohs( pudpheader_ctt->source_port ) ,ntohs( pudpheader_ctt->dest_port ),ipheader_ctt->total_len,pether_ctt->data_len);	
		}
		ms_debug1("%s\r\n", outbuf);
	}else if( ETHERTYPE_ARP==pether_ctt->frame_ctt.type ){
		ARPHEADERContext *arpheader_ctt=&pether_ctt->ethertype_data.arpheader_ctt;
		ms_byte target_ipaddr[32]={0};
		ms_byte sender_ipaddr[32]={0};
		ms_sprintfs(target_ipaddr, "%s", inet_ntoa( *(struct in_addr *)(&arpheader_ctt->target_ipaddr )) );
		ms_sprintfs(sender_ipaddr, "%s", inet_ntoa( *(struct in_addr *)(&arpheader_ctt->sender_ipaddr )) );
		switch( arpheader_ctt->opcode ){
			case ARPOP_REQUEST:
				ms_debug1("%08d.%s  \t%s  \t%s \tWho has %s?tell %s\r\n",pmsnet_ctt->pktindex,pframe_ctt->str_type,
					pframe_ctt->str_source_mac,pframe_ctt->str_dest_mac,target_ipaddr ,sender_ipaddr);
				break;
			case ARPOP_REPLY:  
				ms_debug1("%08d.%s  \t%s  \t%s \tI has %s,reply %s, is at %s\r\n",pmsnet_ctt->pktindex,pframe_ctt->str_type,
					pframe_ctt->str_source_mac,pframe_ctt->str_dest_mac,sender_ipaddr ,target_ipaddr,pframe_ctt->str_source_mac);
				break;
			default:
				ms_debug1("opcode:unknow(%s->%s)\r\n",sender_ipaddr ,target_ipaddr);
			break;
		}
	}else{
		ms_debug1("%08d.%s  \t%s  \t%s\r\n",pmsnet_ctt->pktindex,pframe_ctt->str_type,pframe_ctt->str_source_mac,pframe_ctt->str_dest_mac);
	}
}
ms_void msnet_innerapi_statnum(MSNETContext *pmsnet_ctt,ms_u32 len,ETHERContext *pether_ctt)
{
	FRAMEContext *pframe_ctt=&pether_ctt->frame_ctt;
	MSNETSTATContext *pmsnetstat_ctt=&pmsnet_ctt->msnetstat_ctt[pmsnet_ctt->index_in];
	pmsnetstat_ctt->num_total+=1;
	pmsnetstat_ctt->len_total+=len;
	switch( pether_ctt->frame_ctt.type ){
		case ETHERTYPE_IP:
			pmsnetstat_ctt->num_ipv4+=1;
			pmsnetstat_ctt->len_ipv4+=len;
			IPHEADERContext *pipheader_ctt=&pether_ctt->ethertype_data.ipheader_ctt;
			switch( pipheader_ctt->protocol ){
				case IPPROTO_TCP:
					pmsnetstat_ctt->num_ipv4_tcp+=1;
					pmsnetstat_ctt->len_ipv4_tcp+=pether_ctt->data_len;
					break;
				case IPPROTO_UDP:
					pmsnetstat_ctt->num_ipv4_udp+=1;
					pmsnetstat_ctt->len_ipv4_udp+=pether_ctt->data_len;
					break;
				case IPPROTO_ICMP:
					pmsnetstat_ctt->num_ipv4_icmp+=1;
					pmsnetstat_ctt->len_ipv4_icmp+=pipheader_ctt->total_len;
					break;
				case IPPROTO_IGMP:
					pmsnetstat_ctt->num_ipv4_igmp+=1;
					pmsnetstat_ctt->len_ipv4_igmp+=pipheader_ctt->total_len;
					break;
				case IPPROTO_IPV6:
					pmsnetstat_ctt->num_ipv4_ipv6+=1;
					pmsnetstat_ctt->len_ipv4_ipv6+=pipheader_ctt->total_len;
					break;
				default:
					pmsnetstat_ctt->num_ipv4_others+=1;
					pmsnetstat_ctt->len_ipv4_others+=pipheader_ctt->total_len;
					break;
			}
			break;
		case ETHERTYPE_IPV6:
			pmsnetstat_ctt->num_ipv6+=1;
			pmsnetstat_ctt->len_ipv6+=len;
			break;
		case ETHERTYPE_ARP:
			pmsnetstat_ctt->num_arp+=1;
			pmsnetstat_ctt->len_arp+=len;
			break;
		default:
			pmsnetstat_ctt->num_others+=1;
			pmsnetstat_ctt->len_others+=len;
			break;
	}
}
ms_void msnet_innerapi_parse_checkip(MSNETContext *pmsnet_ctt,ETHERContext *pether_ctt)
{
	FRAMEContext *pframe_ctt=&pether_ctt->frame_ctt;
	
	if( ETHERTYPE_ARP==pether_ctt->frame_ctt.type ){
		ARPHEADERContext *arpheader_ctt=&pether_ctt->ethertype_data.arpheader_ctt;
		ms_byte target_ipaddr[32]={0};
		ms_byte sender_ipaddr[32]={0};
		ms_sprintfs(target_ipaddr, "%s", inet_ntoa( *(struct in_addr *)(&arpheader_ctt->target_ipaddr )) );
		ms_sprintfs(sender_ipaddr, "%s", inet_ntoa( *(struct in_addr *)(&arpheader_ctt->sender_ipaddr )) );

		ms_bool flag_find_scanip=ms_false;
		ms_s32 index=0;
		for(index=0;index<pmsnet_ctt->num_ipaddr_scanip;index++){
			MSSCANIPContext *pmsscanip_ctt=&pmsnet_ctt->pmsscanip_ctt_list[index];
			ms_u32 index_ipaddr;
			for(index_ipaddr=0;index_ipaddr<pmsscanip_ctt->totalnum_ipaddr;index_ipaddr++){
				switch( arpheader_ctt->opcode ){
					case ARPOP_REQUEST:
						if(ms_strncmp_saeq(target_ipaddr, pmsscanip_ctt->ipaddrs_list[index_ipaddr])){
							flag_find_scanip=ms_true;
							goto msfind_scanip_end;
						}
						break;
					case ARPOP_REPLY:  
						if(ms_strncmp_saeq(sender_ipaddr, pmsscanip_ctt->ipaddrs_list[index_ipaddr])){
							flag_find_scanip=ms_true;
							goto msfind_scanip_end;
						}
						break;
					default:
						break;
				}
			}	
		}	
msfind_scanip_end:
		if(ms_false==pmsnet_ctt->pmsnetinput_param->flag_checkip_all
			&&ms_false==flag_find_scanip){
			return;                
		}
		
		for(index=0;index<MAX_CHECKNUM_IP;index++){
			MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[index];
			if(ms_true==pmsipconflict_ctt->flag_used){
				if( ETHERTYPE_ARP==pmsipconflict_ctt->type){
					switch( arpheader_ctt->opcode ){
						case ARPOP_REQUEST:
							if(ms_strncmp_saeq(target_ipaddr, pmsipconflict_ctt->arp_request_ipaddr)){
								pmsipconflict_ctt->times++;
								return;
							}
							break;
						case ARPOP_REPLY:  
							if(ms_strncmp_saeq(sender_ipaddr, pmsipconflict_ctt->arp_request_ipaddr)){
								ms_u32 index_macaddr=0;
								for(index_macaddr=0;index_macaddr<20;index_macaddr++){
									if(ms_strncmp_saeq(pframe_ctt->str_source_mac, pmsipconflict_ctt->arp_request_mac[index_macaddr])){
										pmsipconflict_ctt->arp_request_mac_num[index_macaddr]+=1;
										return;
									}else{
										if(0!=index_macaddr){
											//ms_error1("%s has many mac %s,%s \r\n",sender_ipaddr,pframe_ctt->str_source_mac, pmsipconflict_ctt->arp_request_mac[index_macaddr]);
										}
										if(0==pmsipconflict_ctt->arp_request_mac_num[index_macaddr]){
											ms_strcpy(pmsipconflict_ctt->arp_request_mac[index_macaddr], pframe_ctt->str_source_mac);
											pmsipconflict_ctt->arp_request_mac_num[index_macaddr]+=1;
											return;
										}
									}
								}
								return;
							}
							break;
						default:
							break;
					}
				}
			}else{
				if(ARPOP_REQUEST==arpheader_ctt->opcode
					||ARPOP_REPLY==arpheader_ctt->opcode){
					pmsnet_ctt->totalnum_arp+=1;
					pmsipconflict_ctt->times+=1;
					pmsipconflict_ctt->type=ETHERTYPE_ARP;
					pmsipconflict_ctt->flag_used=ms_true;
				}
				switch( arpheader_ctt->opcode ){
					case ARPOP_REQUEST:
						ms_strcpy(pmsipconflict_ctt->arp_request_ipaddr, target_ipaddr);
						break;
					case ARPOP_REPLY:  
						ms_strcpy(pmsipconflict_ctt->arp_request_ipaddr, sender_ipaddr);
						ms_strcpy(pmsipconflict_ctt->arp_request_mac[0], pframe_ctt->str_source_mac);
						pmsipconflict_ctt->arp_request_mac_num[0]+=1;
						break;
					default:
						break;
				}
				return;
			}
		}
	}
}

ms_void msnet_innerapi_parse_checkmac(MSNETContext *pmsnet_ctt,ETHERContext *pether_ctt)
{
	FRAMEContext *pframe_ctt=&pether_ctt->frame_ctt;
	if( ETHERTYPE_IP==pether_ctt->frame_ctt.type ){
		IPHEADERContext *ipheader_ctt=&pether_ctt->ethertype_data.ipheader_ctt;
		ms_u08  source_ip[16]={0};
		ms_strcpy(source_ip, inet_ntoa( *(struct in_addr *)(&ipheader_ctt->source_ip)));
		ms_u32 index=0;
		for(index=0;index<MAX_CHECKNUM_MAC;index++){
			MSMACCONFLICTContent *pmsmacconflict_ctt=(MSMACCONFLICTContent  *)&pmsnet_ctt->plist_msmacconflict_ctt[index];
			if(ms_true==pmsmacconflict_ctt->flag_used){
				if( ETHERTYPE_IP==pmsmacconflict_ctt->type){
					if(ms_strncmp_saeq(pmsmacconflict_ctt->macaddr, pframe_ctt->str_source_mac)){
						ms_u32 index_ipaddr=0;
						for(index_ipaddr=0;index_ipaddr<20;index_ipaddr++){
							if(ms_strncmp_saeq(pmsmacconflict_ctt->ipaddr[index_ipaddr], source_ip)){
								return;
							}else{
								if(0==ms_buflen(pmsmacconflict_ctt->ipaddr[index_ipaddr])){
									ms_strcpy(pmsmacconflict_ctt->ipaddr[index_ipaddr], source_ip);
									return;
								}
							}
						}
					}
				}
			}else{
				pmsmacconflict_ctt->flag_used=ms_true;
				pmsmacconflict_ctt->type=ETHERTYPE_IP;
				ms_strcpy(pmsmacconflict_ctt->macaddr, pframe_ctt->str_source_mac);
				ms_strcpy(pmsmacconflict_ctt->ipaddr[0], source_ip);
				pmsnet_ctt->totalnum_maclist+=1;
				return;
			}
		}
	}
}

ms_void msnet_innerapi_parse_broadcast(MSNETContext *pmsnet_ctt,ETHERContext *pether_ctt)
{
	FRAMEContext *pframe_ctt=&pether_ctt->frame_ctt;
	if(ms_strncmp_saeq("ff:ff:ff:ff:ff:ff", pframe_ctt->str_dest_mac)){
		ms_byte source_ip[32]={0};
		if( ETHERTYPE_IP==pether_ctt->frame_ctt.type ){
			IPHEADERContext *ipheader_ctt=&pether_ctt->ethertype_data.ipheader_ctt;
			ms_sprintfs(source_ip, "%s", inet_ntoa( *(struct in_addr *)(&ipheader_ctt->source_ip)));
		}
		ms_u32 index=0;
		for(index=0;index<MAX_CHECKNUM_BROADCAST;index++){
			MSBCASTContent *pmsbcast_ctt=(MSBCASTContent *)&pmsnet_ctt->plist_msbcast_ctt[index];
			if(ms_true==pmsbcast_ctt->flag_used){
				if(ms_strncmp_saeq(pmsbcast_ctt->macaddr, pframe_ctt->str_source_mac)){
					ms_u32 index_ipaddr=0;
					for(index_ipaddr=0;index_ipaddr<20;index_ipaddr++){
						if(ms_strncmp_saeq(pmsbcast_ctt->ipaddr[index_ipaddr], source_ip)){
							return;
						}else{
							if(0==ms_buflen(pmsbcast_ctt->ipaddr[index_ipaddr])){
								ms_strcpy(pmsbcast_ctt->ipaddr[index_ipaddr], source_ip);
								return;
							}
						}
					}
				}
			}else{
				pmsbcast_ctt->flag_used=ms_true;
				pmsbcast_ctt->type=pether_ctt->frame_ctt.type;
				ms_strcpy(pmsbcast_ctt->macaddr, pframe_ctt->str_source_mac);
				ms_strcpy(pmsbcast_ctt->ipaddr[0], source_ip);
				pmsnet_ctt->totalnum_broadcast+=1;
				return;
			}
		}
	}
}

ms_void msnet_innerapi_recvpacket(u_char *args, const struct pcap_pkthdr *header,const u_char *packet)
{
	msthread_api_setName("recvpacket");	
	MSNETContext *pmsnet_ctt=(MSNETContext *)args;
	
	MSFIFOLISTContext *pmsfifolist_ctt=msfifo_api_listMalloc(sizeof(HRGATEWAYDATAContext),"hrgw_fifolist");
	if(ms_null!=pmsfifolist_ctt){
		HRGATEWAYDATAContext * phrgwdata_ctt=(HRGATEWAYDATAContext *)pmsfifolist_ctt->ptr;
		ms_memcpy(&phrgwdata_ctt->header, header, ms_bufsize(struct pcap_pkthdr));
		if( ms_noLess( ms_bufsize(phrgwdata_ctt->packet),header->len)
			&&ms_noLess( ms_bufsize(phrgwdata_ctt->packet),header->caplen)){
			ms_memcpy(&phrgwdata_ctt->packet, packet, header->caplen);
		}else{
			msfifo_api_listDemalloc(&pmsfifolist_ctt);
			return;
		}
		phrgwdata_ctt->pmsnet_ctt=pmsnet_ctt;
									//push data to fifo
		ms_u32 totalnum=0;
		mslock_api_do(&mslockctt_pkt);
		msfifo_api_listIn(&pfifolistctt_pkt,pmsfifolist_ctt);
		totalnum=msfifo_api_listTotalNum(&pfifolistctt_pkt);
		mslock_api_undo(&mslockctt_pkt);
	}else{
		ms_error("msfifo_api_listMalloc failed,drop clent request");
	}
}
void msnet_innerapi_checkip_matchscan(MSNETContext *pmsnet_ctt,ms_u32 index_msipconflict)
{
	ms_u32 index=0;
	MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[index_msipconflict];
	for(index=0;index<pmsnet_ctt->num_ipaddr_scanip;index++){
		MSSCANIPContext *pmsscanip_ctt=&pmsnet_ctt->pmsscanip_ctt_list[index];
		ms_u32 index_ipaddr;
		for(index_ipaddr=0;index_ipaddr<pmsscanip_ctt->totalnum_ipaddr;index_ipaddr++){
			if(ms_true==pmsscanip_ctt->flag_gl_msipconflict[index_ipaddr]
				||(ms_false==pmsscanip_ctt->flag_scanip[index_ipaddr])	){
				continue;
			}
			if(ms_strncmp_saeq(pmsipconflict_ctt->arp_request_ipaddr, pmsscanip_ctt->ipaddrs_list[index_ipaddr])){
				pmsscanip_ctt->flag_gl_msipconflict[index_ipaddr]=ms_true;
				pmsscanip_ctt->index_msipconflict[index_ipaddr]=index_msipconflict;
				pmsipconflict_ctt->flag_gl_msarpingip=ms_true;
				pmsipconflict_ctt->index_pmsarpingip=index;
				pmsipconflict_ctt->index_ipaddrs=index_ipaddr;
				return;
			}
		}		
	}
}
void msnet_innerapi_checkip_glscan(MSNETContext *pmsnet_ctt)
{
	ms_u32 index_msipconflict=0;
	for(index_msipconflict=0;index_msipconflict<MAX_CHECKNUM_IP;index_msipconflict++){
		MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[index_msipconflict];
		if(ms_true==pmsipconflict_ctt->flag_used){
			if(ms_true==pmsipconflict_ctt->flag_gl_msarpingip){
				continue;
			}
			ms_u32 index_macaddr=0;
			if( ETHERTYPE_ARP==pmsipconflict_ctt->type){
				MSSCANIPContext *pmsscanip_ctt=&pmsnet_ctt->pmsscanip_ctt_list[pmsipconflict_ctt->index_pmsarpingip];
				if(ms_strncmp_snaeq(pmsipconflict_ctt->arp_request_ipaddr, pmsscanip_ctt->ipaddrs_list[pmsipconflict_ctt->index_ipaddrs])){
					msnet_innerapi_checkip_matchscan(pmsnet_ctt, index_msipconflict);
				}
			}
		}
	}
}

void msnet_innerapi_checkipscan(char *outbuf,ms_pbyte tempbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	ms_u32 index_ipaddr_out=0;
	for(index=0;index<pmsnet_ctt->num_ipaddr_scanip;index++){
		MSSCANIPContext *pmsscanip_ctt=&pmsnet_ctt->pmsscanip_ctt_list[index];
		ms_u32 index_ipaddr;
		for(index_ipaddr=0;index_ipaddr<pmsscanip_ctt->totalnum_ipaddr;index_ipaddr++){
			ms_byte tbuf[2048]={0};
			ms_u32 index_macaddr=0;
			ms_byte arp_request_mac[2048]={0};
			if(ms_true==pmsscanip_ctt->flag_gl_msipconflict[index_ipaddr]){
				MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[pmsscanip_ctt->index_msipconflict[index_ipaddr]];
				for(index_macaddr=0;index_macaddr<20;index_macaddr++){
					if(ms_buflen(pmsipconflict_ctt->arp_request_mac[index_macaddr])>0){
						if(0!=index_macaddr){
							ms_strcats(tbuf,arp_request_mac,",");
						}
						ms_strcats(tbuf,arp_request_mac,"%s(%d)", pmsipconflict_ctt->arp_request_mac[index_macaddr],pmsipconflict_ctt->arp_request_mac_num[index_macaddr]);
					}else{
						break;
					}
				}
			}
			index_ipaddr_out+=1;
			ms_strcats(tempbuf,outbuf,	
				"<tr>"
				"<th align=\"left\">"
					"%04d</th>""<th align=\"left\"></th>"
				"<th align=\"left\">"
					"%s(%d|%d)</th>""<th align=\"left\"></th>"
				"<th align=\"left\">"
					"%s</th>""<th align=\"left\"></th>"
				"<th align=\"left\">"
					"%s</th>""<th align=\"left\"></th>"
				"</tr>",
				index_ipaddr_out,
				pmsscanip_ctt->status[index_ipaddr],pmsscanip_ctt->flag_gl_msipconflict[index_ipaddr],pmsscanip_ctt->index_msipconflict[index_ipaddr],
				pmsscanip_ctt->ipaddrs_list[index_ipaddr],
				arp_request_mac);
		}		
	}
}
void msnet_innerapi_checkipconflict(char *outbuf,ms_pbyte tempbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	for(index=0;index<MAX_CHECKNUM_IP;index++){
		MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[index];
		if(ms_true==pmsipconflict_ctt->flag_used){
			ms_u32 index_macaddr=0;
			if( ETHERTYPE_ARP==pmsipconflict_ctt->type){
				ms_byte tbuf[2048]={0};
				ms_byte arp_request_ipaddr[2048]={0};
				ms_byte arp_request_mac[2048]={0};
				ms_strcats(tbuf,arp_request_ipaddr,"%s(%d) ", pmsipconflict_ctt->arp_request_ipaddr,pmsipconflict_ctt->times);
	
				for(index_macaddr=0;index_macaddr<20;index_macaddr++){
					if(ms_buflen(pmsipconflict_ctt->arp_request_mac[index_macaddr])>0){
						if(0!=index_macaddr){
							ms_strcats(tbuf,arp_request_mac,",");
						}
						ms_strcats(tbuf,arp_request_mac,"%s(%d)", pmsipconflict_ctt->arp_request_mac[index_macaddr],pmsipconflict_ctt->arp_request_mac_num[index_macaddr]);
					}else{
						break;
					}
				}
//CHECK IP conflict		
				if(index_macaddr>1){
					ms_strcats(tempbuf,outbuf,	
						"<tr>"
							"<th align=\"left\">"
								"%04d</th>""<th align=\"left\"></th>"
							"<th align=\"left\">"
								"IP conflict|IP冲突</th>""<th align=\"left\"></th>"
							"<th align=\"left\">"
								"%s</th>""<th align=\"left\"></th>"
							"<th align=\"left\">"
								"%s</th>""<th align=\"left\"></th>"
							"</tr>",
								(index+1),
								arp_request_ipaddr,
								arp_request_mac);
					}
				}
			}else{
						break;
		}
	}
}
void msnet_innerapi_checkipmaxnum(char *outbuf,ms_pbyte tempbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	ms_u32 max_times_index[100];
	for(index=0;index<pmsnet_ctt->max_times_num;index++){
		max_times_index[index]=-1;
	}
	for(index=0;index<MAX_CHECKNUM_IP;index++){
		MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[index];
		if(ms_true==pmsipconflict_ctt->flag_used){
			ms_u32 index_macaddr=0;
			if( ETHERTYPE_ARP==pmsipconflict_ctt->type){
				ms_byte tbuf[2048]={0};
				ms_byte arp_request_ipaddr[2048]={0};
				ms_byte arp_request_mac[2048]={0};
				ms_strcats(tbuf,arp_request_ipaddr,"%s(%d) ", pmsipconflict_ctt->arp_request_ipaddr,pmsipconflict_ctt->times);
//CHECK max num	
				ms_u32 index_cp=0;						
				for(index_cp=0;index_cp<pmsnet_ctt->max_times_num;index_cp++){
					//ms_debug("max_times_index[%d]:%d",index_cp,max_times_index[index_cp]);
					MSIPCONFLICTContent *pmsipconflict_ctt_max=ms_null;
					if(-1!=max_times_index[index_cp]){
						pmsipconflict_ctt_max=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[max_times_index[index_cp]];
					}
					if((ms_null==pmsipconflict_ctt_max)||(ms_moreThan(pmsipconflict_ctt->times, pmsipconflict_ctt_max->times))){
						ms_bufOffset((ms_pbyte)(&max_times_index[index_cp]), -(1*4),	(pmsnet_ctt->max_times_num-index_cp-1)*4);
						max_times_index[index_cp]=index;
						break;
					}
					if((ms_null!=pmsipconflict_ctt_max)&&(ms_strncmp_saeq(pmsipconflict_ctt->arp_request_ipaddr, pmsipconflict_ctt_max->arp_request_ipaddr))){
						break;
					}
				}	
			}
		}else{
			break;
		}
	}

	ms_u32 index_cp=0;
	for(index_cp=0;index_cp<pmsnet_ctt->max_times_num;index_cp++){
		MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[max_times_index[index_cp]];
			ms_u32 index_macaddr=0;
			ms_byte tbuf[2048]={0};
			ms_byte arp_request_ipaddr[2048]={0};
			ms_byte arp_request_mac[2048]={0};
			ms_strcats(tbuf,arp_request_ipaddr,"%s(%d) ", pmsipconflict_ctt->arp_request_ipaddr,pmsipconflict_ctt->times);
			for(index_macaddr=0;index_macaddr<20;index_macaddr++){
				if(ms_buflen(pmsipconflict_ctt->arp_request_mac[index_macaddr])>0){
					if(0!=index_macaddr){
						ms_strcats(tbuf,arp_request_mac,",");
					}
					ms_strcats(tbuf,arp_request_mac,"%s(%d)", pmsipconflict_ctt->arp_request_mac[index_macaddr],pmsipconflict_ctt->arp_request_mac_num[index_macaddr]);
				}else{
					break;
				}
			}
			ms_strcats(tempbuf,outbuf,	
				"<tr>"
				"<th align=\"left\">"
					"%04d</th>""<th align=\"left\"></th>"
				"<th align=\"left\">"
					"最多数量排行%03d</th>""<th align=\"left\"></th>"
				"<th align=\"left\">"
					"%s</th>""<th align=\"left\"></th>"
				"<th align=\"left\">"
					"%s</th>""<th align=\"left\"></th>"
				"</tr>",
				max_times_index[index_cp],(index_cp+1),arp_request_ipaddr,arp_request_mac);
	}
}
void msnet_innerapi_checkipall(char *outbuf,ms_pbyte tempbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	for(index=0;index<MAX_CHECKNUM_IP;index++){
		MSIPCONFLICTContent *pmsipconflict_ctt=(MSIPCONFLICTContent *)&pmsnet_ctt->plist_msipconflict_ctt[index];
		if(ms_true==pmsipconflict_ctt->flag_used){
			ms_u32 index_macaddr=0;
			if( ETHERTYPE_ARP==pmsipconflict_ctt->type){
				ms_byte tbuf[2048]={0};
				ms_byte arp_request_ipaddr[2048]={0};
				ms_byte arp_request_mac[2048]={0};
				ms_strcats(tbuf,arp_request_ipaddr,"%s(%d) ", pmsipconflict_ctt->arp_request_ipaddr,pmsipconflict_ctt->times);
				for(index_macaddr=0;index_macaddr<20;index_macaddr++){
					if(ms_buflen(pmsipconflict_ctt->arp_request_mac[index_macaddr])>0){
						if(0!=index_macaddr){
							ms_strcats(tbuf,arp_request_mac,",");
						}
						ms_strcats(tbuf,arp_request_mac,"%s(%d)", pmsipconflict_ctt->arp_request_mac[index_macaddr],pmsipconflict_ctt->arp_request_mac_num[index_macaddr]);
					}else{
						break;
					}
				}

				ms_strcats(tempbuf,outbuf,	
				"<tr>"
					"<th align=\"left\">"
						"%04d</th>""<th align=\"left\"></th>"
					"<th align=\"left\">"
						"无</th>""<th align=\"left\"></th>"
					"<th align=\"left\">"
						"%s</th>""<th align=\"left\"></th>"
					"<th align=\"left\">"
						"%s</th>""<th align=\"left\"></th>"
				"</tr>",
					(index+1),
					arp_request_ipaddr,
					arp_request_mac);
				}
			}else{
				break;
		}
	}
}
void msnet_innerapi_checkportscan(char *outbuf,ms_pbyte tempbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	ms_u32 index_ipaddr_out=0;
	for(index=0;index<pmsnet_ctt->num_ipaddr_scanport;index++){
		MSSCANPORTContext *pmsscanport_ctt=&pmsnet_ctt->pmsscanport_ctt_list[index];
		index_ipaddr_out+=1;
		ms_strcats(tempbuf,outbuf,"%s","<tr>");
		ms_strcats(tempbuf,outbuf,"<th align=\"left\">""%04d</th>""<th align=\"left\"></th>",index_ipaddr_out);
		ms_strcats(tempbuf,outbuf,"<th align=\"left\">""%s</th>""<th align=\"left\"></th>",pmsscanport_ctt->status);
		ms_strcats(tempbuf,outbuf,"<th align=\"left\">""%s</th>""<th align=\"left\"></th>",pmsscanport_ctt->ipaddrs);
		ms_strcats(tempbuf,outbuf,"<th align=\"left\">""%s</th>""<th align=\"left\"></th>",pmsscanport_ctt->port_info);
		ms_strcats(tempbuf,outbuf,"%s","</tr>");
	}	
}
void msnet_innerapi_checkmacconflict(char *outbuf,ms_pbyte tempbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	for(index=0;index<MAX_CHECKNUM_MAC;index++){
		MSMACCONFLICTContent *pmsmacconflict_ctt=(MSMACCONFLICTContent  *)&pmsnet_ctt->plist_msmacconflict_ctt[index];
		if(ms_true==pmsmacconflict_ctt->flag_used){
			ms_u32 index_ipaddr=0;
			if( ETHERTYPE_IP==pmsmacconflict_ctt->type){
				ms_byte tbuf[2048]={0};
				ms_byte ipaddr_list[1024]={0};
				for(index_ipaddr=0;index_ipaddr<20;index_ipaddr++){
					if(ms_buflen(pmsmacconflict_ctt->ipaddr[index_ipaddr])>0){
						if(0!=index_ipaddr){
							ms_strcats(tbuf,ipaddr_list,",");
						}
						ms_strcats(tbuf,ipaddr_list,"%s", pmsmacconflict_ctt->ipaddr[index_ipaddr]);
					}else{
						break;
					}
				}
				if(index_ipaddr>1){
					ms_strcats(tempbuf,outbuf,	
					"<tr>"
						"<th align=\"left\">"
							"%04d</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
							"多IP地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
							"%s</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
							"%s</th>""<th align=\"left\"></th>"
						"</tr>",
						(index+1),
						pmsmacconflict_ctt->macaddr,
						ipaddr_list);
				}
			}
		}else{
			break;
		}
	}
}
void msnet_innerapi_page_common(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	MSNETINPUTParam *pmsnetinput_param=pmsnet_ctt->pmsnetinput_param;
	char tempbuf[2048]={0};

	mslock_api_do(&mslockctt_pkt);
	ms_u32 totalnum=msfifo_api_listTotalNum(&pfifolistctt_pkt);
	mslock_api_undo(&mslockctt_pkt);	
//Available Connect Infomation	
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">001.%s</h2>","通用配置信息");
	ms_strcats(tempbuf,outbuf,"%s","<ul "MSWEB_FONT_STYLE">");
	ms_strcats(tempbuf,outbuf,"<li>监控网卡:%s</li>" , pmsnetinput_param->devname);
	ms_strcats(tempbuf,outbuf,"<li>过滤规则:%s</li>" , 
		 ms_strncmp_snaeq("none" ,pmsnetinput_param->filter_string)? pmsnetinput_param->filter_string : "无");
	ms_strcats(tempbuf,outbuf,"<li>网络地址检测扫描地址范围:%s</li>" , ms_strncmp_snaeq("none" ,pmsnetinput_param->ipaddr_list_scanip)? pmsnetinput_param->ipaddr_list_scanip : "无");
	ms_strcats(tempbuf,outbuf,"<li>网络服务检测扫描服务端口:%s</li>" ,  ms_strncmp_snaeq("none" ,pmsnetinput_param->port_list_scanport)? pmsnetinput_param->port_list_scanport : "无");
	ms_strcats(tempbuf,outbuf,"<li>网络服务检测扫描地址范围:%s</li>" ,  ms_strncmp_snaeq("none" ,pmsnetinput_param->pipaddr_list_scanport)? pmsnetinput_param->pipaddr_list_scanport : "无");
	ms_strcats(tempbuf,outbuf,"<li>包时间显示功能:%s</li>" , pmsnetinput_param->flag_output_event 	? "开启" : "关闭");
	ms_strcats(tempbuf,outbuf,"<li>包信息显示功能:%s</li>" , pmsnetinput_param->flag_output_header? "开启" : "关闭");
	ms_strcats(tempbuf,outbuf,"<li>包分析功能:%s</li>" , pmsnetinput_param->flag_output_parse 	? "开启" : "关闭");
	ms_strcats(tempbuf,outbuf,"<li>包统计功能:%s</li>" , pmsnetinput_param->flag_stat 		? "开启" : "关闭");
	ms_strcats(tempbuf,outbuf,"<li>待处理的数据帧总数:%d</li>" , totalnum);
	
	ms_strcats(tempbuf,outbuf,"%s","</ul>");

	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">002.%s</h2>","功能页面列表");
	ms_u08 listindex=0;
	#define pagelist_msnet(item) \
		ms_strcats(tempbuf,outbuf,"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;<a  "MSWEB_FONT_STYLE"    href=\"http://%s/%s\"><b>%03d.%s</b></a>", \
			webhost_info.server_host,mswebt_##item,++listindex,msweb_##item); 
	pagelist_msnet(statinfo);	
	pagelist_msnet(checkip);
	pagelist_msnet(checkport);
	pagelist_msnet(checkmac);
	pagelist_msnet(broadcast);
	pagelist_msnet(meminfo);
}
void msnet_innerapi_page_statinfo(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	ms_u32 index=0;
	#define msnetstat_bytesframe_pageshow(item_str,item);	\
		ms_strcats(tempbuf,outbuf, \
					"<tr>" \
					"<th align=\"left\">" \
						"%04d</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"帧数据</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%s</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%d</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%s</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%.03f</th>""<th align=\"left\"></th>" \
					"</tr>",(++index),item_str,pmsnetstat_ctt->num_##item,ms_BYTE64AUTO_unit(pmsnetstat_ctt->len_##item), \
			(ms_double)((ms_double)pmsnetstat_ctt->len_##item/(ms_double)pmsnetstat_ctt->len_total));
	#define msnetstat_bytesipv4_pageshow(item_str,item); \
		ms_strcats(tempbuf,outbuf, \
					"<tr>" \
					"<th align=\"left\">" \
						"%04d</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"IPV4</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%s</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%d</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%s</th>""<th align=\"left\"></th>" \
					"<th align=\"left\">" \
						"%.03f</th>""<th align=\"left\"></th>" \
					"</tr>",(++index),item_str,pmsnetstat_ctt->num_ipv4_##item,ms_BYTE64AUTO_unit(pmsnetstat_ctt->len_ipv4_##item), \
			(ms_double)((ms_double)pmsnetstat_ctt->len_ipv4_##item/(ms_double)pmsnetstat_ctt->len_total));

	MSNETSTATContext *pmsnetstat_ctt=&pmsnet_ctt->msnetstat_ctt[pmsnet_ctt->index_out];
	char *tempbuf=ms_null;
	ms_malloc_noRet(tempbuf,3062,"availablestreams_buf");	
	msnet_innerapi_page_common( outbuf, pmsnet_ctt, webhost_info);
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">003.%s</h2>","数据构成分析");

	ms_strcats(tempbuf,outbuf,
		"<table cellpadding=\"4\" cellspacing=\"0\" "MSWEB_FONT_STYLE">"
		);
		ms_strcats(tempbuf,outbuf,
			"<tbody>"
			);
			
			ms_strcats(tempbuf,outbuf,
				"<tr>"
						"<th align=\"left\">"
					"序号</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"类型</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"	
					"名称</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"		
					"包个数</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"包字节</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"占比</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
				"</tr>");	
				msnetstat_bytesframe_pageshow("TOTAL", total);
				msnetstat_bytesframe_pageshow("IPV4", ipv4);
				msnetstat_bytesframe_pageshow("IPV6", ipv6);
				msnetstat_bytesframe_pageshow("ARP", arp);
				msnetstat_bytesframe_pageshow("ORTHERS", others);
				msnetstat_bytesipv4_pageshow("TCP", tcp);
				msnetstat_bytesipv4_pageshow("UDP", udp);
				msnetstat_bytesipv4_pageshow("ICMP", icmp);
				msnetstat_bytesipv4_pageshow("IGMP", igmp);
				msnetstat_bytesipv4_pageshow("IPV6", ipv6);
				msnetstat_bytesipv4_pageshow("ORTHERS", others);

			ms_strcats(tempbuf,outbuf,	
				"</tbody>"
				);	
		ms_strcats(tempbuf,outbuf,	
			"</table>"
			);
	ms_deMalloc(tempbuf);
}
void msnet_innerapi_page_checkip(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	char *tempbuf=ms_null;
	ms_u32 bufsize=(0==pmsnet_ctt->totalnum_arp)  ? 1024:(pmsnet_ctt->totalnum_arp*128);
	ms_malloc_noRet(tempbuf,bufsize,"availablestreams_buf");				
//Available Connect Infomation	
	msnet_innerapi_page_common( outbuf, pmsnet_ctt, webhost_info);
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">003.%s</h2>","网络地址分析");

	ms_strcats(tempbuf,outbuf,"<li>total_num: %d </li>",pmsnet_ctt->totalnum_arp);

	ms_strcats(tempbuf,outbuf,
		"<table cellpadding=\"4\" cellspacing=\"0\" "MSWEB_FONT_STYLE">"
		);
		ms_strcats(tempbuf,outbuf,
			"<tbody>"
			);
			
			ms_strcats(tempbuf,outbuf,
				"<tr>"
						"<th align=\"left\">"
					"序号</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"描述信息</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"	
					"目标地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"		
					"回应目标地址的物理地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
				"</tr>");	
				msnet_innerapi_checkipconflict(outbuf,  tempbuf,  pmsnet_ctt,  webhost_info);
				msnet_innerapi_checkipscan(outbuf,  tempbuf,  pmsnet_ctt,  webhost_info);
			#if 0	
				if(ms_true==pmsnet_ctt->pmsnetinput_param->flag_checkip_all){
					msnet_innerapi_checkipmaxnum(outbuf,  tempbuf,  pmsnet_ctt,  webhost_info);
				}
				msnet_innerapi_checkipall(outbuf,  tempbuf,  pmsnet_ctt,  webhost_info);
			#endif
			ms_strcats(tempbuf,outbuf,	
				"</tbody>"
				);	
		ms_strcats(tempbuf,outbuf,	
			"</table>"
			);
	ms_deMalloc(tempbuf);
}
void msnet_innerapi_page_checkport(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	char *tempbuf=ms_null;
	ms_u32 bufsize=(10>pmsnet_ctt->num_ipaddr_scanport)  ? 65536:(pmsnet_ctt->num_ipaddr_scanport*256);
	ms_malloc_noRet(tempbuf,bufsize,"availablestreams_buf");				
//Available Connect Infomation	
	msnet_innerapi_page_common( outbuf, pmsnet_ctt, webhost_info);
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">003.%s</h2>","网络地址分析");
	ms_strcats(tempbuf,outbuf,"<li>total_num: %d </li>",pmsnet_ctt->num_ipaddr_scanport);
	ms_strcats(tempbuf,outbuf,
		"<table cellpadding=\"4\" cellspacing=\"0\" "MSWEB_FONT_STYLE">"
		);
		ms_strcats(tempbuf,outbuf,
			"<tbody>"
			);
			
			ms_strcats(tempbuf,outbuf,
				"<tr>"
						"<th align=\"left\">"
					"序号</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"描述信息</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"	
					"目标地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"		
					"服务端口</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
				"</tr>");	
				msnet_innerapi_checkportscan(outbuf,  tempbuf,  pmsnet_ctt,  webhost_info);
			ms_strcats(tempbuf,outbuf,	
				"</tbody>"
				);	
		ms_strcats(tempbuf,outbuf,	
			"</table>"
			);
	ms_deMalloc(tempbuf);
}
void msnet_innerapi_page_checkmac(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	char *tempbuf=ms_null;
	ms_u32 bufsize=(0==pmsnet_ctt->totalnum_maclist)  ? 1024: (pmsnet_ctt->totalnum_maclist*256);
	ms_malloc_noRet(tempbuf,bufsize,"availablestreams_buf");				
//Available Connect Infomation	
	msnet_innerapi_page_common( outbuf, pmsnet_ctt, webhost_info);
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">003.%s</h2>","物理地址分析");

	ms_strcats(tempbuf,outbuf,"<li>total_num: %d </li>",pmsnet_ctt->totalnum_maclist);

	ms_strcats(tempbuf,outbuf,
		"<table cellpadding=\"4\" cellspacing=\"0\" "MSWEB_FONT_STYLE">"
		);
		ms_strcats(tempbuf,outbuf,
			"<tbody>"
			);
			
			ms_strcats(tempbuf,outbuf,
				"<tr>"
						"<th align=\"left\">"
					"序号</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"描述信息</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"	
					"物理地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"		
					"网络地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
				"</tr>");	

				msnet_innerapi_checkmacconflict(outbuf,  tempbuf,  pmsnet_ctt,  webhost_info);
				ms_u32 index=0;
				for(index=0;index<MAX_CHECKNUM_MAC;index++){
					MSMACCONFLICTContent *pmsmacconflict_ctt=(MSMACCONFLICTContent  *)&pmsnet_ctt->plist_msmacconflict_ctt[index];
					if(ms_true==pmsmacconflict_ctt->flag_used){
						ms_u32 index_ipaddr=0;
						if( ETHERTYPE_IP==pmsmacconflict_ctt->type){
							ms_byte tbuf[2048]={0};
							ms_byte ipaddr_list[1024]={0};
							for(index_ipaddr=0;index_ipaddr<20;index_ipaddr++){
								if(ms_buflen(pmsmacconflict_ctt->ipaddr[index_ipaddr])>0){
									if(0!=index_ipaddr){
										ms_strcats(tbuf,ipaddr_list,",");
									}
									ms_strcats(tbuf,ipaddr_list,"%s", pmsmacconflict_ctt->ipaddr[index_ipaddr]);
								}else{
									break;
								}
							}

							ms_strcats(tempbuf,outbuf,	
							"<tr>"
								"<th align=\"left\">"
									"%04d</th>""<th align=\"left\"></th>"
								"<th align=\"left\">"
									"无</th>""<th align=\"left\"></th>"
								"<th align=\"left\">"
									"%s</th>""<th align=\"left\"></th>"
								"<th align=\"left\">"
									"%s</th>""<th align=\"left\"></th>"
							"</tr>",
								(index+1),
								pmsmacconflict_ctt->macaddr,
								ipaddr_list);
						}
					}else{
						break;
					}
				}
			ms_strcats(tempbuf,outbuf,	
				"</tbody>"
				);	
		ms_strcats(tempbuf,outbuf,	
			"</table>"
			);
	ms_deMalloc(tempbuf);
}
void msnet_innerapi_page_broadcast(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	char *tempbuf=ms_null;
	ms_u32 bufsize=(0==pmsnet_ctt->totalnum_broadcast)  ? 1024: (pmsnet_ctt->totalnum_broadcast*256);
	ms_malloc_noRet(tempbuf,bufsize,"availablestreams_buf");				
//Available Connect Infomation	
	msnet_innerapi_page_common( outbuf, pmsnet_ctt, webhost_info);
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">003.%s</h2>","广播数据分析");

	ms_strcats(tempbuf,outbuf,"<li>total_num: %d </li>",pmsnet_ctt->totalnum_broadcast);

	ms_strcats(tempbuf,outbuf,
		"<table cellpadding=\"4\" cellspacing=\"0\" "MSWEB_FONT_STYLE">"
		);
		ms_strcats(tempbuf,outbuf,
			"<tbody>"
			);
			
			ms_strcats(tempbuf,outbuf,
				"<tr>"
						"<th align=\"left\">"
					"序号</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
					"描述信息</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"	
					"物理地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"		
					"网络地址</th>""<th align=\"left\"></th>"
						"<th align=\"left\">"
				"</tr>");	
				ms_u32 index=0;
				for(index=0;index<MAX_CHECKNUM_BROADCAST;index++){
					MSBCASTContent *pmsbcast_ctt=(MSBCASTContent *)&pmsnet_ctt->plist_msbcast_ctt[index];
					if(ms_true==pmsbcast_ctt->flag_used){
						ms_u32 index_ipaddr=0;
						ms_byte tbuf[2048]={0};
						ms_byte ipaddr_list[1024]={0};
						for(index_ipaddr=0;index_ipaddr<20;index_ipaddr++){
							if(ms_buflen(pmsbcast_ctt->ipaddr[index_ipaddr])>0){
								if(0!=index_ipaddr){
									ms_strcats(tbuf,ipaddr_list,",");
								}
								ms_strcats(tbuf,ipaddr_list,"%s", pmsbcast_ctt->ipaddr[index_ipaddr]);
							}else{
								break;
							}
						}
						ms_strcats(tempbuf,outbuf,	
							"<tr>"
								"<th align=\"left\">"
									"%04d</th>""<th align=\"left\"></th>"
								"<th align=\"left\">"
									"无</th>""<th align=\"left\"></th>"
								"<th align=\"left\">"
									"%s</th>""<th align=\"left\"></th>"
								"<th align=\"left\">"
									"%s</th>""<th align=\"left\"></th>"
							"</tr>",
							(index+1),
							pmsbcast_ctt->macaddr,
							ipaddr_list);
					}else{
						break;
					}
				}
			ms_strcats(tempbuf,outbuf,	
				"</tbody>"
				);	
		ms_strcats(tempbuf,outbuf,	
			"</table>"
			);
	ms_deMalloc(tempbuf);
}
void msnet_innerapi_page_meminfo(char *outbuf,MSNETContext *pmsnet_ctt, WEBHOSTInfo webhost_info)
{
	msnet_innerapi_page_common( outbuf, pmsnet_ctt, webhost_info);
	char tempbuf[256]={0};
	ms_strcats(tempbuf,outbuf,"<h2 "MSWEB_H2_STYLE">003.%s</h2>","内存分配分析");
	msmem_api_info(outbuf,"disable");
}
MSEVConnectRet msnet_innerapi_newconnect(void *callback_args,MSEVClientContext * pEvClient_ctt)
{
	MSNETContext *pmsnet_ctt=(MSNETContext *)callback_args;
	WEBREQUESTContext request_ctt;
	ms_byte databuf[mscfg_maxlen_recv+1]={0};
	int recvLen =msev_api_recv(pEvClient_ctt,&request_ctt,databuf,sizeof(databuf));
	if(recvLen<0){
		if(-1==recvLen){
			ms_error("msev_api_recv failed.ipaddr:%s",pEvClient_ctt->ipaddr);
		}
		return msev_connect_ret_err;
	}
	if(msstr_api_isCasestr(databuf, "HTTP") !=0){
		char *sendbuf=NULL;
		ms_malloc_goto(ms_faild,sendbuf, (sizeof(char)*1024*ms_max3(MAX_CHECKNUM_IP,MAX_CHECKNUM_MAC, MAX_CHECKNUM_BROADCAST)), "stbuf_ reply")	;

#define page_list(item) \
	if(ms_strncmp_seq((const char *)&request_ctt.url[1], mswebt_##item)){ \
			msweb_api_ok(sendbuf); \
			msweb_api_cmHeadTitle(sendbuf,msweb_msnet,msweb_msnet, request_ctt.extern_data.http_rd.webhost_info);   \
			msnet_innerapi_page_##item(sendbuf,pmsnet_ctt, request_ctt.extern_data.http_rd.webhost_info);  \
			msweb_api_tailTitle(sendbuf, request_ctt.extern_data.http_rd.webhost_info,mslog_api_version(),"msavskit@163.com");  \
		}
		page_list(statinfo)
		else page_list(checkip)
		else page_list(checkport)
		else page_list(checkmac)
		else page_list(broadcast)
		else page_list(meminfo)
		else{
			msweb_api_ok(sendbuf); 
			msweb_api_cmHeadTitle(sendbuf,msweb_msnet,msweb_msnet, request_ctt.extern_data.http_rd.webhost_info); 
			msnet_innerapi_page_common(sendbuf,pmsnet_ctt, request_ctt.extern_data.http_rd.webhost_info); 
			msweb_api_tailTitle(sendbuf, request_ctt.extern_data.http_rd.webhost_info,mslog_api_version(),"msavskit@163.com");
		}	
			
		int datalen=ms_buflen(sendbuf);
		if(datalen>0){
			int len=msnet_api_epollSend3(pEvClient_ctt->fd,sendbuf,datalen); 
			if(!(len>0)){
				ms_error("msnet_innerapi_epollsend2 failed");
			}
		}
		DEMALLOC_VAL(sendbuf);
	}
	close(pEvClient_ctt->fd);
	return msev_connect_ret_close;
ms_faild:
	close(pEvClient_ctt->fd);
	return msev_connect_ret_err;
}
ms_void msnet_innerapi_parsescanip(MSNETContext *pmsnet_ctt)
{
	if(ms_strncmp_saeq(pmsnet_ctt->pmsnetinput_param->ipaddr_list_scanip, "none")){
		pmsnet_ctt->num_ipaddr_scanip=0;
		ms_debug("Total----%d", pmsnet_ctt->num_ipaddr_scanip);
	}else{
		ms_byte temp_ipaddr[MAXNUM_IPNET_SEGMENT*32]={0};
		ms_strcpy(temp_ipaddr,pmsnet_ctt->pmsnetinput_param->ipaddr_list_scanip);
		
		ms_string str_array_item[MAXNUM_IPNET_SEGMENT];
		pmsnet_ctt->num_ipaddr_scanip=msstr_api_split(temp_ipaddr, "#", str_array_item);	//192.168.000.001-192.168.000.255#192.168.1.1-192.168.1.255
		if(pmsnet_ctt->num_ipaddr_scanip>MAXNUM_IPNET_SEGMENT){
			ms_waring("War:ipaddrs num(%d)  is  out of range(0~%d)",pmsnet_ctt->num_ipaddr_scanip,MAXNUM_IPNET_SEGMENT);
			pmsnet_ctt->num_ipaddr_scanip=MAXNUM_IPNET_SEGMENT;
		}else{
			ms_debug("Total----%d(%s)", pmsnet_ctt->num_ipaddr_scanip,pmsnet_ctt->pmsnetinput_param->ipaddr_list_scanip);
		}
			
		ms_u32 index;
		ms_string str_array_var[64];
		for(index=0;index<pmsnet_ctt->num_ipaddr_scanip;index++){
			MSSCANIPContext *pmsscanip_ctt=&pmsnet_ctt->pmsscanip_ctt_list[index];
			ms_s32 num=msstr_api_split(str_array_item[index], "-", str_array_var);
			if(num>0){
				pmsscanip_ctt->flag_ipaddrs=ms_true;
				ms_strcpy(pmsscanip_ctt->ipaddrs_start,str_array_var[0]);
				if(num>1){
					ms_strcpy(pmsscanip_ctt->ipaddrs_end,str_array_var[1]);
				}
				pmsscanip_ctt->totalnum_ipaddr=msnet_api_getIpList(pmsscanip_ctt->ipaddrs_start, pmsscanip_ctt->ipaddrs_end, 
					(ms_string)pmsscanip_ctt->ipaddrs_list, 16);
				ms_debug("%03d.totalnum_ipaddr:%d[%s -%s ]",(index+1),pmsscanip_ctt->totalnum_ipaddr,
					pmsscanip_ctt->ipaddrs_start,pmsscanip_ctt->ipaddrs_end);
			}			
		}
	}
}
ms_void msnet_innerapi_parsescanport(MSNETContext *pmsnet_ctt)
{
	if(ms_strncmp_saeq(pmsnet_ctt->pmsnetinput_param->pipaddr_list_scanport, "none")){
		pmsnet_ctt->num_ipaddr_scanport=0;
		ms_debug("Total----%d", pmsnet_ctt->num_ipaddr_scanport);
	}else{
		ms_byte temp_ipaddr[MAXNUM_IPNET_SEGMENT*16]={0};
		ms_strcpy(temp_ipaddr,pmsnet_ctt->pmsnetinput_param->pipaddr_list_scanport);
		ms_string str_array_item[MAXNUM_IPADDR_SCANPORT];
		pmsnet_ctt->num_ipaddr_scanport=msstr_api_split(temp_ipaddr, "#", str_array_item);	
		if(pmsnet_ctt->num_ipaddr_scanport>MAXNUM_IPADDR_SCANPORT){
			ms_waring("War:ipaddrs num(%d)  is  out of range(0~%d)",pmsnet_ctt->num_ipaddr_scanport,MAXNUM_IPADDR_SCANPORT);
			pmsnet_ctt->num_ipaddr_scanport=MAXNUM_IPADDR_SCANPORT;
		}else{
			ms_debug("Total----%d(%s)", pmsnet_ctt->num_ipaddr_scanport,pmsnet_ctt->pmsnetinput_param->pipaddr_list_scanport);
		}
		ms_u32 index;
		for(index=0;index<pmsnet_ctt->num_ipaddr_scanport;index++){
			MSSCANPORTContext *pmsscanport_ctt=&pmsnet_ctt->pmsscanport_ctt_list[index];
			ms_strcpy(pmsscanport_ctt->ipaddrs,str_array_item[index]);
		}	
	}
}
static ms_pvoid msnet_monitorapi_page(ms_void * ms_in arglist)
{
	MSEVContext pserver_msnet_page;
	pserver_msnet_page.name="msnet_page";
	pserver_msnet_page.port=8040;
	pserver_msnet_page.tdCpNum=4;
	pserver_msnet_page.tdCpType=msev_tdcp_maxnum1;
	pserver_msnet_page.ictAccessTime_us=ms_usmseconds(200);
	pserver_msnet_page.cbArgs=(ms_void *) arglist;
	pserver_msnet_page.onConnect=msnet_innerapi_newconnect;
	pserver_msnet_page.flag_ictAccess=ms_false;
	pserver_msnet_page.flag_ictResAddr=ms_false ;
	pserver_msnet_page.dcpSize=100;
	pserver_msnet_page.pIct_ctt=ms_null;
	pserver_msnet_page.pflag_appInit=ms_null;
	pserver_msnet_page.pflag_disable=ms_null;
	pserver_msnet_page.purlval=ms_null;
	pserver_msnet_page.method="GET";
	pserver_msnet_page.route=ms_null;
	pserver_msnet_page.evAuthent_ctt.flag_reqAuthent=ms_false;
	pserver_msnet_page.evAuthent_ctt.flag_web=ms_false;
	pserver_msnet_page.evAuthent_ctt.pStrPrivatekey=ms_null;
	pserver_msnet_page.evAuthent_ctt.timeoutSec=TIMEOUT_TOKEN_LIVE;
	pserver_msnet_page.evAuthent_ctt.ignorPath=ms_null;
	msev_api_createServer(&pserver_msnet_page);
	
	//msev_api_createServer("msnet_page",8040,0,-1,(ms_void *) arglist,msnet_innerapi_newconnect,ms_false,ms_false,ms_true);
}
ms_void msnet_monitorapi_parsepacket(u_char *args)
{
	msthread_api_setName("parsepacket");	
	MSFIFOLISTContext *msfifolist_ctt=ms_null;
	HRGATEWAYDATAContext *phrgwdata_ctt=ms_null;
	while(1){
		//get request_list num
		mslock_api_do(&mslockctt_pkt);
		ms_u32 totalnum=msfifo_api_listTotalNum(&pfifolistctt_pkt);
		if(totalnum>0){
			//get one request
			msfifolist_ctt=msfifo_api_listOut(&pfifolistctt_pkt);
		}
		mslock_api_undo(&mslockctt_pkt);
		if(totalnum>0){
			//do open
			if(ms_null!=msfifolist_ctt){
				phrgwdata_ctt=msfifolist_ctt->ptr;
				MSNETContext *pmsnet_ctt=phrgwdata_ctt->pmsnet_ctt;
				ETHERContext ether_ctt;
				ms_stru0(&ether_ctt, ETHERContext);
				ether_ctt.debug=pmsnet_ctt->pmsnetinput_param->flag_output_header ;
				msframe_api_doFrame(&ether_ctt, (ms_pbyte ms_in) phrgwdata_ctt->packet,ms_true);
				if(pmsnet_ctt->pmsnetinput_param->flag_output_event){
					msnet_innerapi_showpacket(pmsnet_ctt,&ether_ctt);
				}
				if(pmsnet_ctt->pmsnetinput_param->flag_output_parse){
					msnet_innerapi_parse_checkip(pmsnet_ctt,&ether_ctt);
					msnet_innerapi_parse_checkmac(pmsnet_ctt,&ether_ctt);
					msnet_innerapi_parse_broadcast(pmsnet_ctt,&ether_ctt);
				}
				if(pmsnet_ctt->pmsnetinput_param->flag_stat){
					msnet_innerapi_statnum(pmsnet_ctt,phrgwdata_ctt->header.len,&ether_ctt);
				}
				msfifo_api_listDemalloc(&msfifolist_ctt);
			}else{
				ms_error("msfifolist_ctt is null");
			}
			ms_usleep(50);
		}else{
			ms_usleep(500);
		}
	}
}
ms_void msnet_monitorapi_scanip(ms_void *args)
{
	msthread_api_setName("scanip");	
	MSSCANIPPARAMContext *pmsscanipparam_ctt=(MSSCANIPPARAMContext *)args;	
	MSNETContext *pmsnet_ctt=pmsscanipparam_ctt->pmsnet;	
	ETHERContext ether_ctt_arp;
	ether_ctt_arp.debug=ms_true;
	ms_byte  outbuf_arp[2048]={0};
	ms_s32 len_arp=0;
	URLContext rtpraw_urlctt;
	URLProtocol *pffurl_raw=(URLProtocol *)msptc_api_matchByPtcindex(msptc_raw);
	rtpraw_urlctt.ifcOut=pmsnet_ctt->pmsnetinput_param->devname;
	rtpraw_urlctt.flags=FLAG_WRITE;
	pffurl_raw->url_open(&rtpraw_urlctt);
	ms_u08 send_times=0;
	while(1){
		MSSCANIPContext *pmsscanip_ctt=&pmsnet_ctt->pmsscanip_ctt_list[pmsscanipparam_ctt->index];
		ms_u32 index_ipaddr;
		for(index_ipaddr=0;index_ipaddr<pmsscanip_ctt->totalnum_ipaddr;index_ipaddr++){
			ms_strcpy(pmsscanip_ctt->status[index_ipaddr], "scan ipaddr....");
			ms_stru0(&ether_ctt_arp, ETHERContext);
			len_arp=msframe_api_buildArp(&ether_ctt_arp, "00:1f:c6:9c:63:24", "10.10.80.40", pmsscanip_ctt->ipaddrs_list[index_ipaddr], outbuf_arp);
			pffurl_raw->url_write(&rtpraw_urlctt,outbuf_arp,len_arp);
			pmsscanip_ctt->flag_scanip[index_ipaddr]=ms_true;
			msnet_innerapi_checkip_glscan(pmsnet_ctt);
			ms_msleep(100);
			ms_buf0(pmsscanip_ctt->status[index_ipaddr]);
		}	
		ms_sleep(60);
	}
	pffurl_raw->url_close(&rtpraw_urlctt);
}

ms_void msnet_monitorapi_scanport(ms_void *args)
{
	msthread_api_setName("scanport");	
	MSSCANPORTPARAMContext *pmsscanportparam_ctt=(MSSCANPORTPARAMContext *)args;	
	MSNETContext *pmsnet_ctt=pmsscanportparam_ctt->pmsnet;	
	URLProtocol *pffurl_tcp=(URLProtocol *)msptc_api_matchByPtcindex(msptc_tcp);
	URLContext tcp_urlctt;
	ms_byte tbuf[16]={0};
	ms_bool flag_frist=ms_false;	
	while(1){
		MSSCANPORTContext *pmsscanport_ctt=&pmsnet_ctt->pmsscanport_ctt_list[pmsscanportparam_ctt->index];
		ms_strcpy(pmsscanport_ctt->status, "scan port....");
		ms_buf0(pmsscanport_ctt->port_info);
		ms_u32 index_port=1;
		for(index_port=1;index_port<MAXNUM_PORT_PER_IPADDR;index_port++){
			ms_stru0(&tcp_urlctt, URLContext);
			ms_sprintfs(pmsscanport_ctt->status, "scan port %d....",index_port);
			ms_sprintfs(tcp_urlctt.url, "tcp://%s:%d", pmsscanport_ctt->ipaddrs,index_port);
			tcp_urlctt.flags=FLAG_WRITE;
			tcp_urlctt.opt.tcp.flag_scanport=ms_true;
			tcp_urlctt.opt.tcp.open_timeout=300;
			tcp_urlctt.opt.tcp.flag_noprint=ms_true;
			if(!(pffurl_tcp->url_open(&tcp_urlctt)<0)){
				if(ms_true==flag_frist){
					ms_strcats(tbuf, pmsscanport_ctt->port_info, ",", index_port);
				}else{
					flag_frist=ms_true;
				}
				ms_strcats(tbuf, pmsscanport_ctt->port_info, "%d", index_port);
			}
			pffurl_tcp->url_close(&tcp_urlctt);
		}
		ms_strcpy(pmsscanport_ctt->status, "scan finish");
		ms_sleep(3600);
	}
}
ms_void msnet_monitorapi_stat(ms_void *args)
{
	msthread_api_setName("stat");	
	MSNETContext *pmsnet_ctt=(MSNETContext *)args;
	MSNETSTATContext *pmsnetstat_ctt=ms_null;
	ms_u64 timep=mstime_api_upUs();
	pmsnet_ctt->index_out=0;
	pmsnet_ctt->index_in=pmsnet_ctt->index_out+1;
	while(1){
		if(mstime_api_counterAsyncUs_Up(&timep,ms_usmseconds(999))){
			if(NUM_STATINFO_FIFO==(++pmsnet_ctt->index_in)){
				pmsnet_ctt->index_in=0;
			}
			pmsnetstat_ctt=&pmsnet_ctt->msnetstat_ctt[pmsnet_ctt->index_out];
			ms_stru0(pmsnetstat_ctt, MSNETSTATContext);
			if(NUM_STATINFO_FIFO==(++pmsnet_ctt->index_out)){
				pmsnet_ctt->index_out=0;
			}
		}
		ms_usleep(100);
	}
}
ms_void msnet_monitorapi_attackPort(ms_void )
{
	msthread_api_setName("attackPort");	
	URLProtocol *pffurl_raw=(URLProtocol *)msptc_api_matchByPtcindex(msptc_raw);
	URLContext rtpraw_urlctt;
	rtpraw_urlctt.ifcOut="enp3s0";
	rtpraw_urlctt.flags=FLAG_WRITE;
	pffurl_raw->url_open(&rtpraw_urlctt);

	ETHERContext ether_ctt;
	ms_memset0(&ether_ctt, ms_bufsize(ETHERContext));
	MSBULDTCPContext msbuildtcp_ctt;
	ms_byte tcpPkt[2048]={0};
	msbuildtcp_ctt.str_dest_mac="00:e0:4c:3f:3d:b8";
	msbuildtcp_ctt.str_source_mac="00:e0:4c:3f:3d:b8";
	msbuildtcp_ctt.source_ip="192.168.0.83";
	msbuildtcp_ctt.dest_ip="192.168.0.83";
	msbuildtcp_ctt.source_port=65241;
	msbuildtcp_ctt.dest_port=8010 ;
	msbuildtcp_ctt.seq_num=286;
	msbuildtcp_ctt.ack_num=108;
	msbuildtcp_ctt.urg=0;
	msbuildtcp_ctt.ack=1;
	msbuildtcp_ctt.psh=0;
	msbuildtcp_ctt.rst=1;
	msbuildtcp_ctt.syn=0;
	msbuildtcp_ctt.fin=0;
	TCPHEADEROPTIONContext opt_ctt;	
	ether_ctt.debug=ms_true;
	ether_ctt.data_len=40;
	ms_u32 len=msframe_api_buildTcp(&ether_ctt, &msbuildtcp_ctt, tcpPkt);
	ms_debug("len=====%d");
	while(1){
		pffurl_raw->url_write(&rtpraw_urlctt,tcpPkt,len);
		ms_msleep(100);
	}
	pffurl_raw->url_close(&rtpraw_urlctt);
}

ms_s08 msnet_innerapi_enter(MSNETINPUTParam *pmsnetinput_param)
{
//init
	msptc_api_init();
	msthread_api_envInit();
	msptc_api_register();
	msmem_api_init(ms_false);
ms_funcLine;	
	//msnet_monitorapi_attackPort();
ms_funcLine;	
	mslock_api_init(&mslockctt_pkt,"hrrequest_lock",lockType_mutex);
ms_funcLine;	
	ms_byte errbuf[PCAP_ERRBUF_SIZE]={0};
	pcap_t *handle=mspacp_api_open(pmsnetinput_param->devname);
	 if (handle == ms_null) {
		 ms_errGoto(ms_end,  "mspacp_api_open  failed ");
	 }
	//pcap_file(pcap_t * p)
	//pcap_is_swapped
	if (ms_true!=mspacp_api_isen10mb(handle) ) {
		 ms_errGoto(ms_failed,  "Device %s doesn't provide Ethernet headers - not supported", pmsnetinput_param->devname);
	}
ms_funcLine;
	if(mspacp_api_setfilter(handle,pmsnetinput_param->devname,pmsnetinput_param->filter_string)<0){
		ms_errGoto(ms_failed, "mspacp_api_setfilter failed");
	}
ms_funcLine;
	mssignal_api_catch("msnet");

	MSNETContext msnet_ctt;
	ms_stru0(&msnet_ctt, MSNETContext);
	msnet_ctt.max_times_num=30;	
	msnet_ctt.pmsnetinput_param=pmsnetinput_param;
ms_funcLine;
	ms_malloc_retErr(-1,msnet_ctt.plist_msipconflict_ctt, sizeof(MSIPCONFLICTContent)*MAX_CHECKNUM_IP, "plist_msipconflict_ctt");
	ms_malloc_retErr(-1,msnet_ctt.plist_msmacconflict_ctt, sizeof(MSMACCONFLICTContent)*MAX_CHECKNUM_MAC, "plist_msmacconflict_ctt");
	ms_malloc_retErr(-1,msnet_ctt.plist_msbcast_ctt, sizeof(MSBCASTContent)*MAX_CHECKNUM_BROADCAST, "plist_msbcast_ctt");
	ms_malloc_retErr(-1,msnet_ctt.pmsscanip_ctt_list, (ms_bufsize(MSSCANIPContext)*MAXNUM_IPNET_SEGMENT), "pmsscanip_ctt_list");
	ms_malloc_retErr(-1,msnet_ctt.pmsscanport_ctt_list, (ms_bufsize(MSSCANPORTContext)*MAXNUM_IPADDR_SCANPORT), "pmsscanport_ctt_list");
ms_funcLine;
	msnet_innerapi_parsescanip(&msnet_ctt);
ms_funcLine;	
	msnet_innerapi_parsescanport(&msnet_ctt);
ms_funcLine;	
	MSTHREADContext msthread_ctt_page;
	ms_debug("Create msnet_page");
	msthread_api_create(&msthread_ctt_page, "msnet_page",msnet_monitorapi_page,  &msnet_ctt);
	ms_debug("Create msnet_parsepacket");
	MSTHREADContext msthread_ctt_parsepacket;
	msthread_api_create(&msthread_ctt_parsepacket, "msnet_parsepacket",msnet_monitorapi_parsepacket,  &msnet_ctt);
ms_funcLine;
	ms_u32 index;
	MSTHREADContext msthread_ctt_scanip[MAXNUM_IPNET_SEGMENT]={0};
	MSSCANIPPARAMContext msscanipparam_ctt[MAXNUM_IPNET_SEGMENT];
	if( ms_strncmp_snaeq("none" ,pmsnetinput_param->ipaddr_list_scanip)){
		ms_debug("start scan ip");
		for(index=0;index<msnet_ctt.num_ipaddr_scanip;index++){
			msscanipparam_ctt[index].pmsnet=&msnet_ctt;
			msscanipparam_ctt[index].index=index;
			msthread_api_create(&msthread_ctt_scanip[index], "msnet_scanip",msnet_monitorapi_scanip, &msscanipparam_ctt[index]);
		}
	}
	
	MSTHREADContext msthread_ctt_scanport[MAXNUM_IPADDR_SCANPORT];
	MSSCANPORTPARAMContext msscanportparam_ctt[MAXNUM_IPNET_SEGMENT];
	if(ms_strncmp_snaeq("none" ,pmsnetinput_param->pipaddr_list_scanport)){
		ms_debug("start scan port");
		for(index=0;index<msnet_ctt.num_ipaddr_scanport;index++){
			msscanportparam_ctt[index].pmsnet=&msnet_ctt;
			msscanportparam_ctt[index].index=index;
			msthread_api_create(&msthread_ctt_scanport[index], "msnet_scanport",msnet_monitorapi_scanport, &msscanportparam_ctt[index]);
		}
	}
	
	MSTHREADContext msthread_ctt_stat;
	msthread_api_create(&msthread_ctt_stat, "msnet_stat",msnet_monitorapi_stat,  &msnet_ctt);
//one time
#if 0
	struct pcap_pkthdr header;	
	const ms_u08 *packet = pcap_next(handle, &header);
	ms_debug("Jacked a packet with length of [%d]", header.len);
	msnet_innerapi_recvpacket(ms_null, &header, packet);
#else	
//more time
	pcap_loop(handle, -1, msnet_innerapi_recvpacket, (u_char *)&msnet_ctt);
	//pcap_dispatch(pcap_t * p, int cnt, pcap_handler callback, u_char * user)
	//pcap_stats(pcap_t * p, struct pcap_stat * ps)
#endif

ms_failed:	
	mspacp_api_close(handle);
ms_end:	
	ms_deMalloc(msnet_ctt.pmsscanport_ctt_list);
	ms_deMalloc(msnet_ctt.pmsscanip_ctt_list);
	ms_deMalloc(msnet_ctt.plist_msipconflict_ctt);
	ms_deMalloc(msnet_ctt.plist_msmacconflict_ctt);
	ms_deMalloc(msnet_ctt.plist_msbcast_ctt);
	
	mslock_api_deinit(&mslockctt_pkt);
	msptc_api_deinit();
	msmem_api_deinit();
	msthread_api_envDeinit();
	mslog_api_deinit();
}
ms_s08 main(int argc, char **argv)
{
	mslog_api_init( 52, ms_null,ms_null, ms_null);
	MSNETINPUTParam msnetinput_param;
	msnet_innerapi_optdefault(&msnetinput_param);
	msnet_abapi_readcfg(&msnetinput_param);
	msnet_innerapi_optparse(&msnetinput_param,  argc, argv);

	ms_debug("--set program name to %s",msnetinput_param.program_name);
	msthread_api_setName(msnetinput_param.program_name);
	
	ms_debug("--set program running in %s mode",((msnetinput_param.flag_daemon) ?"daemon"  :"interactive"));
	if(ms_true==msnetinput_param.flag_daemon){
		msmd_api_startDaemon("/");
	}else{
		ms_waring("Interactive mode only can be running in debug mode,please enable daemon");
	}
	ms_debug("set program running %s watchdog",((msnetinput_param.flag_watchdog) ? "with" :"without"));
	if(ms_true==msnetinput_param.flag_watchdog){
		msmd_api_watchdog((int (*)(ms_void *))msnet_innerapi_enter,&msnetinput_param);
	}else{
		ms_waring("This mode only can be running in debug mode,please enable watchdog");
		msnet_innerapi_enter(&msnetinput_param);
	}
	mslog_api_deinit();
}
#undef MSNET_C

